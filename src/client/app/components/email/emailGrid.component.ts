// core
import { Component, OnInit} from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

// interfaces
import { Email, IEmailBody } from '../../interfaces/email';
import { User } from '../../interfaces/user';

// UI
import {MenuItem, Message } from 'primeng/primeng';

// routes
import { RouterExtensions, Config } from '../../modules/core/index';

// Service
import {ConfirmationService} from 'primeng/primeng';

var saveAs = require('file-saver/FileSaver');

@Component({
  moduleId: module.id,
  selector: 'sd-email',
  templateUrl: 'emailGrid.component.html',
  providers: [ConfirmationService]
})

export class EmailGridComponent implements OnInit {

  msgs: Message[] = []; msg: Message[] = [];

  items: MenuItem[];

  title;
  data: any[];

  cols: any[];

  user: User;

  account;
  email;
  password = '';
  type;
  seen = ''

  body : IEmailBody;

  constructor(public routerext: RouterExtensions,
              private http: Http,
              private confirmationService: ConfirmationService) {
  }

  async ngOnInit() {

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.account = this.user.email;

    this.items = [
      {
        label: 'Inbox', icon: 'fa-user-plus', command: () => {
        if (this.password === '') {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Enter Password First',
            detail: 'Enter Password'
          });
        } else {
          this.seen = '';
          this.type = 'INBOX';
          this.getMails(this.type, this.seen);
        }
      }
      },
      {
        label: 'Unread Inbox Mails', icon: 'fa-user-plus', command: () => {
        if (this.password === '') {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Enter Password First',
            detail: 'Enter Password'
          });
        } else {
          this.seen = 'UNSEEN';
          this.type = 'INBOX';
          this.getMails(this.type, this.seen);
        }
      }
      },
      {
        label: 'Read Inbox Mails', icon: 'fa-user-plus', command: () => {
        if (this.password === '') {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Enter Password First',
            detail: 'Enter Password'
          });
        } else {
          this.seen = 'SEEN';
          this.type = 'INBOX';
          this.getMails(this.type, this.seen);
        }
      }
      },
      {
        label: 'Sent Mail', icon: 'fa-user-plus', command: () => {
        if (this.password === '') {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Enter Password First',
            detail: 'Enter Password'
          });
        } else {
          this.seen = '';
          this.type = '[Gmail]/Sent Mail';
          this.getMails(this.type, this.seen);
        }
      }
      },
      {
        label: 'Draft', icon: 'fa-user-plus', command: () => {
        if (this.password === '') {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Enter Password First',
            detail: 'Enter Password'
          });
        } else {
          this.seen = '';
          this.type = '[Gmail]/Drafts';
          this.getMails(this.type, this.seen);
        }
      }
      },
      {
        label: 'Starred', icon: 'fa-user-plus', command: () => {
        if (this.password === '') {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Enter Password First',
            detail: 'Enter Password'
          });
        } else {
          this.seen = '';
          this.type = '[Gmail]/Starred';
          this.getMails(this.type, this.seen);
        }
      }
      }
    ];

    this.title = 'Email Grid';

  }

  getMails(mailtype: string, seen: string) {

    var types = this.user.email;
    var result = types.split('@');
    var ext = result.pop();               //Removes last value and grap the last value

    if (ext.toString() === 'gmail.com') {
      this.email = new Email(this.user.email,
        this.password, 'imap.gmail.com', mailtype, '', seen, '');

      this.http.post('http://localhost:3000/email/inbox', JSON.stringify(
        this.email
      )).subscribe((res) => {
          console.log('res', res.json());
          this.data = res.json();
          console.log('this.data', this.data);
        },
        (err) => {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Authentication Error',
            detail: 'Enter Password Correct'
          });
        });
      this.cols = [
        {field: 'UID', header: 'UID'},
        {field: 'from.address', header: 'Mail'},
        {field: 'messageId', header: 'MessageId'},
        {field: 'path', header: 'Main from'},
        {field: 'date', header: 'Date & Time'},
        {field: 'bodystructure', header: 'Body'},
        {field: 'title', header: 'Title'},
        {field: 'flags', header: 'Flag'}
      ];
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Not Allowed',
        detail: 'We Only Support Gmail'
      });
    }

  }

  open(item) {
    console.log('open', item.UID);

    ////------------------ without calling body api-----------//

//     console.log('open', item.bodystructure);
// let face = item.bodystructure;
//     for(let key in face) {
//       // console.log('key', key);
//       // console.log('face', face[key]);
//       let sub = face[key];
//       for(let ckey in sub) {
//         // console.log('ckey', ckey);
//         // console.log('sub', sub[ckey]);
//         if(ckey === 'disposition') {
//           console.log('face', face[key]);
//           let ssub = sub[ckey][0];
//           // console.log('ssub', ssub);
//           for(let cckey in ssub) {
//             // console.log('cckey', cckey);
//             if(cckey === 'filename') {
//               // console.log('filename',ssub[cckey]);
//               this.confirmationService.confirm({
//                           message: 'Do You Download Attachment?',
//                           header: 'Confirmation',
//                           icon: 'fa fa-question-circle',
//                           accept: () => {
//                             var blob = new Blob([sub[ckey]]);
//                             saveAs(blob, ssub[cckey]);
//                           }
//                         });
//             }
//           }
//         }
//         // let ssub = sub[ckey];
//         // for(let cckey in ssub) {
//         //   // console.log('ckey', cckey);
//         //   // console.log('sub', ssub[cckey]);
//         // }
//       }
//     }

    ////------------------ with calling body api---------------------//

    this.http.post('http://localhost:3000/email/body', JSON.stringify(
      new Email(this.user.email, this.password, 'imap.gmail.com', this.type, '', '', item.UID.toLocaleString())
    )).subscribe((res) => {

   ////------------------ without mail parser-------------------//

      //   // let data = JSON.stringify(res);
      //   // console.log('data.struct' ,typeof data.struct);
      //   console.log('--------------------------------------');
      //  // var abc = JSON.stringify(data);
      //  //  console.log('abc', abc);
      //  //  console.log(abc['struct']);
      //   let data = res.json();
      //   // console.log('data', data);
      // let a = JSON.parse(res['_body']);
      //   console.log('res', typeof a);
      //   // console.log('data', JSON.parse(data));
      //   // for(let key in  data) {
      //   //
      //   //   console.log('---------------key-----------------');
      //   //   console.log(key);
      //   //
      //   //   console.log('----------------data--------------------');
      //   //   console.log(data[key]);
      //   // }
      //
      // // var blob = new Blob([res.arrayBuffer()]);
      // //         saveAs(blob, 'abc');


    ////------------------ with mail parser----------------------//

        // if(item.flags.indexOf('\\Seen') === -1)
        //   item.flags.push('\\Seen');
        //
        // let data = res.json();
        // let contentType = ''; let filename = '';
        // for(var key in data) {
        //   console.log('key: ' , key , ' value: ' + data[key]);
        //   if(key === 'filename') {
        //     filename = data[key];
        //   } else if(key === 'contentType') {
        //     contentType = data[key];
        //   } else if(key === 'text') {
        //     if(data[key] === '') {
        //       data[key] = 'Empty Mail';
        //     }
        //     this.msg = [];
        //     this.msg.push({severity:'success', summary:'',
        //       detail: data[key]});
        //     // break;
        //   }
        //
        //   if(filename !== '' && contentType !== '') {
        //     this.confirmationService.confirm({
        //       message: 'File Name : ' + filename + '     File Type : ' + contentType + 'Do You Download Attachment?',
        //       header: 'Confirmation',
        //       icon: 'fa fa-question-circle',
        //       accept: () => {
        //         var blob = new Blob([res.arrayBuffer()]);
        //         saveAs(blob, filename);
        //       }
        //     });
        //     this.msg = [];
        //     this.msg.push({severity:'success', summary:'',
        //       detail: 'File Name : ' + filename + '     File Type : ' + contentType });
        //     // break;
        //   }
        // }


      console.log('res', res.json());
      let data = res.json();
      for(let key in data) {
        console.log('key', key);
        console.log('data[key]', data[key]);
        let sub = data[key];
        for(let ckey in sub) {
          console.log('ckey', ckey);
          console.log('sub[ckey]', sub[ckey]);
          if(ckey === 'path') {
            this.confirmationService.confirm({
                          message: 'Do You Download Attachment?',
                          header: 'Confirmation',
                          icon: 'fa fa-question-circle',
                          accept: () => {
                            var blob = new Blob([JSON.stringify(sub)]);
                            saveAs(blob, sub[ckey]);
                          }
                        });
          }
        }
      }
      },
      (err) => {
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Authentication Error OR ' + err,
          detail: err});
      });
  }

  edit(item) {
    console.log('edit', item);
    if (item.path === this.type.toUpperCase().trim()) {
      var seen = 'unseen';
      if (item.flags.length > 0) {
        console.log(item.flags.indexOf('\\Seen'));
        seen = item.flags.indexOf('\\Seen') === -1 ? 'unseen' : 'seen';
      }

      this.http.post('http://localhost:3000/email/read', JSON.stringify(
        new Email(this.user.email,
          this.password, 'imap.gmail.com', 'INBOX', '0', seen, item.UID)
      )).subscribe((res) => {
          console.log('res', res.json());
          let flag = (res.json() === true) ? [] : res.json();
          item.flags = flag;
        },
        (err) => {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Authentication Error OR Server Error',
            detail: 'Enter Password Correct OR Server Not Responed'
          });
        });
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Not Any Action Perform',
        detail: 'Read OR Unread Working For Only Inbox'
      });
    }
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error', err);
  }
}
