import { EmailGridComponent } from './emailGrid.component';

export const EmailRoutes: Array<any> = [
  {
    path: 'email',
    component: EmailGridComponent
  }
];
