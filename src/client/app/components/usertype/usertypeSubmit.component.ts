// core libs
import { Component, OnInit } from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { UserType } from '../../interfaces/usertype';
import { User } from '../../interfaces/user';

// UI libs
import {Message} from 'primeng/primeng';

// services
import { UserTypeService } from '../../services/usertype.service';
import { UserService } from '../../services/user.service';

@Component({
  moduleId: module.id,
  selector: 'ss-usertypesubmit',
  templateUrl: 'usertypeSubmit.component.html',
  providers: [UserTypeService, UserService]
})

export class UserTypeSubmitComponent implements OnInit {

  msgs: Message[] = [];

  title; isedited: boolean;

  btnedit: boolean;
  btnsubmit: boolean;

  id; name; isActive; isDelete;

  usertype: UserType; loginuser: User;

  constructor(private userTypeService: UserTypeService
    , private userService: UserService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });

      this.title = 'UserType Submit Page';

      this.btnsubmit = true;
      this.btnedit = false;

      if (localStorage.getItem('editUserType')) {
        this.isedited = true;
        console.log('edit Initialize');
        this.usertype = JSON.parse(localStorage.getItem('editUserType'));

        this.btnedit = true;
        this.btnsubmit = false;

        // this.id = this.id;
        this.name = this.usertype.name;
        this.isActive = this.usertype.isActive;
        this.isDelete = this.usertype.isDelete;
      }
  }

  submit() {
    if (localStorage.getItem('currentUser')) {

      this.userTypeService.insert(
        this.name.toLowerCase().trim(),
        this.isActive = true,
        this.isDelete = false).subscribe(data => {
        console.log(data);
          this.routeTo('/admin/usertype');
      },
      error => {
        this.msgs = [];
        this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'User Type Already Exist'});

      });
    }
  }

  editUserType() {
    if (localStorage.getItem('editUserType')) {
      console.log('edit Update');

      if(this.usertype) {
        this.usertype.name = this.name !== undefined ? this.name.toLowerCase().trim()
          : this.name;
        this.usertype.isActive = this.isActive;
        this.usertype.isDelete = this.isDelete;

        this.userTypeService.updateBy(this.usertype.name, this.usertype)
          .subscribe(data => { console.log(data); } ,
            error => {
              this.msgs = [];
              this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Cannot Edit'});

            });

        this.routeTo('/admin/usertype');
      }
    }
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToGrid() {
    this.routeTo('/admin/usertype');
  }

}
