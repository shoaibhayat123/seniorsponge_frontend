// core
import { Component, OnInit , ViewEncapsulation} from '@angular/core';

// services
import { UserTypeService } from '../../services/usertype.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message } from 'primeng/primeng';

// interfaces
import { UserType } from '../../interfaces/usertype';
import { User } from '../../interfaces/user';

// routes
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-usertypegrid',
  templateUrl: 'usertypeGrid.component.html',
  providers: [UserTypeService, UserService]
})

export class UserTypeGridComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  title;

  cols: any[];

  usertypes: UserType[]; loginuser: User;

  constructor(private userTypeService: UserTypeService,
              private userService: UserService,
              public routerext: RouterExtensions) {
    this.userTypeService.getAll().subscribe(usertypes => { this.usertypes = usertypes;
        console.log(this.usertypes);
      }, error => this.logError(error)
    );

    this.cols = [
      // {field: 'id' , header: 'User Type Id'},
      {field: 'name' , header: 'Name'},
      {field: 'isActive' , header: 'IsActive'},
      {field: 'isDelete' , header: 'IsDelete'}
    ];

  }

  ngOnInit() {

    localStorage.removeItem('editUserType');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });

    this.items = [
      {
        label: 'UserType Submit', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/usertype/submit');
      }
      },
      {
        label: 'Delete All UserTypes', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];

    this.title = 'User Type Grid';

  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.userTypeService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.usertypes.length);
    this.usertypes.splice(0, this.usertypes.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All UserType Succesfully'});
  }

  edit(usertype: UserType) {
    localStorage.setItem('editUserType', JSON.stringify(usertype));
    this.routeTo('/admin/usertype/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + usertype.name});
  }

  delete(usertype: UserType) {
    this.msgs = [];
    this.userTypeService.deleteBy(usertype.id)
      .subscribe(usertype => { console.log(usertype);},
        error => {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Delete'});
        });

    let indexToDelete: number = 0;
    indexToDelete = this.usertypes.findIndex((item) => item.id === usertype.id);
    this.usertypes.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + usertype.name});
  }

}
