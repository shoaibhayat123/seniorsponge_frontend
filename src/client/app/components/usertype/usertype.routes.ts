import { UserTypeGridComponent } from './usertypeGrid.component';
import { UserTypeSubmitComponent } from './usertypeSubmit.component';

export const UserTypeRoutes: Array<any> = [
  {
    path: 'admin/usertype',
    component: UserTypeGridComponent
  },
  {
    path: 'admin/usertype/submit',
    component: UserTypeSubmitComponent
  }
];
