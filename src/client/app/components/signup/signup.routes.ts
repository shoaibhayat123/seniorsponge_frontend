import { SignUpComponent } from './signup.component';

export const SignUpRoutes: Array<any> = [
  {
    path: 'signup',
    component: SignUpComponent
  }
];
