// core libs
import {Component, OnInit} from '@angular/core';

// routes libs
import {ActivatedRoute} from '@angular/router';
import {RouterExtensions, Config} from '../../modules/core/index';

// interfaces
import { User } from '../../interfaces/user';
import { UserType } from '../../interfaces/usertype';

// services
import { UserService } from '../../services/user.service';
import { UserTypeService } from '../../services/usertype.service';

//UI libs
import {Message, SelectItem } from 'primeng/primeng';

// password hash
import { Endcrypt } from 'endcrypt/endcrypt';

@Component({
  moduleId: module.id,
  selector: 'ss-signup',
  templateUrl: 'signup.component.html',
  providers: [UserService, UserTypeService]
})

export class SignUpComponent implements OnInit {

  title;
  selectedUserType: string;
  btnedit: boolean;
  btnsignup: boolean;

  firstName;
  lastName;
  createdDate;
  createdBy;
  modifiedDate;
  lat;
  long;
  isActive;
  isDelete;
  userName;
  password;
  repassword;
  email;
  age;

  loginuser: User;
  user: User;
  usertypes: UserType[] = [];

  msgs: Message[] = [];
  _usertypeid: SelectItem[];

  constructor(private userService: UserService
              ,private userTypeService: UserTypeService
              ,private route: ActivatedRoute
              ,public routerext: RouterExtensions) {}

  ngOnInit() {
    this.title = 'SignUp Page';

    this.btnsignup = true;
    this.btnedit = false;

    if (localStorage.getItem('editUser')) {
      console.log('edit Initialize');
      this.user = JSON.parse(localStorage.getItem('editUser'));

      this.btnedit = true;
      this.btnsignup = false;

      if(this.user) {
        this.email = this.user.email;
        this.firstName = this.user.firstName;
        this.lastName = this.user.lastName;
        this.userName = this.user.userName;
        this.password = this.decryptPassword(JSON.parse(this.user.password));
        this.repassword = this.decryptPassword(JSON.parse(this.user.password));
        this.age = this.user.age;
        this.lat = this.user.lat;
        this.long = this.user.long;
        this.createdDate = this.user.createdDate;
        this.createdBy = this.user.createdBy;
        this.modifiedDate = this.user.modifiedDate;
        this.isActive = this.user.isActive;
        this.isDelete = this.user.isDelete;
      }
    }

    this.userTypeService.getAll().subscribe(usertypes => {
        this.usertypes = usertypes;
        for (let i = 0; i < this.usertypes.length; i++) {
          console.log('user types', this.usertypes);
          if (!this.usertypes[i]['isDelete']) {
            this._usertypeid.push({label: this.usertypes[i]['name'], value: this.usertypes[i]['id']});
          }
        }
        if (this.user) {
          this.user.id_userType = this.user.id_userType === undefined ? ''
            : this.user.id_userType;
          this.selectedUserType = this.user.id_userType === '' ? this._usertypeid.length > 0 ?
            String(this._usertypeid[0].value) : '' : this.user.id_userType;
        }
      }
      , error => this.logError(error)
    );
  }

  encryptPassword(userPassword): string {
    return new Endcrypt().encryptWithKey(userPassword, '16bytesecretkeys');
  }

  decryptPassword(encrypt): string {
    return new Endcrypt().decryptWithKey(encrypt, '16bytesecretkeys');
  }

  logError(err: any) {
    console.log('error' , err);
  }

  signup() {
    if (localStorage.getItem('currentUser')) {
      this.loginuser = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.loginuser.userName);
      this.createdBy = this.loginuser.userName;
    }
    console.log(this.createdBy);
    if(this.selectedUserType) {
      console.log('selectedUserType',this.selectedUserType)
      if (this.password === this.repassword) {
        this.userService.signUp(
          this.firstName,
          this.lastName,
          this.createdDate = new Date().toDateString(),
          this.createdBy = this.loginuser.userName || 'admin',
          this.modifiedDate = new Date().toDateString(),
          this.lat,
          this.long,
          this.isActive = true,
          this.isDelete = false,
          this.userName.toLowerCase().trim(),
          JSON.stringify(this.encryptPassword(this.password)),
          this.email.toLowerCase().trim(),
          this.age,
          this.selectedUserType).subscribe(
          (data) => {
            this.routeToLogin();
          },
          (err) => {
            this.msgs = [];
            this.msgs.push({
              severity: 'error',
              summary: 'Error Message',
              detail: 'User Already Exist OR Remove Space from Start and End'
            });
          });
      } else {
        this.msgs = [];
        this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Password Not Matching'});
      }
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Missing Selection', detail: 'Select User Type'
      });
    }
  }

  editUser() {
    if (localStorage.getItem('editUser')) {
      console.log('edit Update');
      if (this.selectedUserType) {
        console.log('selectedUserType',this.selectedUserType)
        if (this.password === this.repassword) {
          if (this.user) {
            this.user.email = this.email !== undefined ? this.email.toLowerCase().trim()
              : this.email;
            this.user.firstName = this.firstName;
            this.user.lastName = this.lastName;
            this.user.userName = this.userName !== undefined ? this.userName.toLowerCase().trim()
              : this.userName;
            this.user.password = JSON.stringify(this.encryptPassword(this.password));
            this.user.age = this.age;
            this.user.lat = this.lat;
            this.user.long = this.long;
            this.user.createdDate = this.createdDate;
            this.user.createdBy = this.createdBy;
            this.user.modifiedDate = new Date().toDateString();
            this.user.isActive = this.isActive;
            this.user.isDelete = this.isDelete;
            this.user.id_userType = this.selectedUserType;

            this.userService.updateUser(this.user.email, this.user)
              .subscribe(user => { console.log(user); },
                error => {
                  this.msgs = [];
                  this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Update'});
                });

            localStorage.removeItem('currentUser');
            localStorage.setItem('currentUser', JSON.stringify(this.user[0]));
          }
        } else {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Password Not Matching'});
        }
      } else {
        this.msgs = [];
        this.msgs.push({
          severity: 'error', summary: 'Missing Selection', detail: 'Select User Type'
        });
      }
    } else {
      this.msgs = [];
      this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'User Not Found'});
    }
  }

  routeToLogin() {
    this.routerext.navigate(['/login'], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
