import { DashboardComponent } from './dashboard.component';
import { GalleryComponent } from '../gallery/gallery.component';

export const DashboardRoutes: Array<any> = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'gallery',
    component: GalleryComponent
  }
];
