// core
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
// services
import {UserService} from '../../services/user.service';
// interfaces
import {User} from '../../interfaces/user';
// routes
import {RouterExtensions, Config} from '../../modules/core/index';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'sd-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],
  providers: [UserService]
})

export class DashboardComponent implements OnInit {

  title;

  loginuser: User;

  constructor(private userServices: UserService,
              public routerext: RouterExtensions) {
  }

  ngOnInit() {
    console.log('hello to dashboard');
    $(function () {
      var options = {
        alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
      };
      $('.grid-stack').gridstack(options);
    });

    localStorage.removeItem('editUser');

    if (!localStorage.getItem('currentUser')) {
      this.routeToLogin();
    }

  }

  logout() {
    localStorage.removeItem('isAdmin');
    console.log(typeof localStorage.getItem('currentUser'));
    this.loginuser = JSON.parse(localStorage.getItem('currentUser'));
    this.loginuser.isActive = false;
    console.log(this.loginuser.email);

    this.userServices.updateUser(this.loginuser.email, this.loginuser)
      .subscribe(user => console.log(user));
    localStorage.removeItem('currentUser');
    this.routeToLogin();
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToLogin() {
    this.routerext.navigate(['/login'], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  go_to(route) {
    this.routerext.navigate(['/' + route], {
      transition: {
        duration: 1000
      }
    });
  }

}
