// core libs
import { Component, OnInit } from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { CommunityLocation } from '../../interfaces/communityLocation';
import { Community } from '../../interfaces/community';
import { User } from '../../interfaces/user';

// services
import { CommunityLocationService } from '../../services/communityLocation.service';
import { CommunityService } from '../../services/community.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-submitlocation',
  templateUrl: 'communityLocationSubmit.component.html',
  providers: [CommunityLocationService, CommunityService, UserService]
})

export class CommunityLocationSubmitComponent implements OnInit {

  title; edited;

  msgs: Message[] = [];

  _communityid: SelectItem[];

  communities : Community[] = [];

  selectedCommunityId: string;

  btnedit: boolean;
  btnsubmit: boolean;

  name; address; lat; long; isActive; isDelete;

  communityLocation: CommunityLocation; loginuser: User;

  constructor(private communityLocationService: CommunityLocationService
    ,private communityService: CommunityService
    , private userService: UserService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  logError(err: any) {
    console.log('error' , err);
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.title = 'Community Location Submit Page';

      if (!localStorage.getItem('isAdmin')) {
        this.routeTo('/admin/login');
      }

      this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

      this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
        console.log('finddata', finddata);
        if (finddata) {
          if (!finddata.isDelete) {
            let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
              : '';
            if (isAdmin.toLowerCase().trim() !== 'admin') {
              localStorage.removeItem('isAdmin');
              this.routeTo('/admin/login');
            }
          }
        }
      });

      this.btnsubmit = true;
      this.btnedit = false;

      if (localStorage.getItem('editCommunityLocation')) {
        console.log('edit Initialize');
        this.communityLocation = JSON.parse(localStorage.getItem('editCommunityLocation'));
        if (this.communityLocation) {
          console.log('submit CommunityLocation', this.communityLocation.id_community);
          this.btnedit = true;
          this.btnsubmit = false;
          this.edited = true;

          this.name = this.communityLocation.name;
          this.address = this.communityLocation.address;
          this.lat = this.communityLocation.lat;
          this.long = this.communityLocation.long;
          this.isActive = this.communityLocation.isActive;
          this.isDelete = this.communityLocation.isDelete;
        }
      }

      this._communityid = [];


      // Community Dropdown
      this.communityService.getAll().subscribe(communities => {
          this.communities = communities;
          this._communityid.push({label: '', value: ''});
          for (let j = 0; j < this.communities.length; j++) {
            console.log('communities ', this.communities);
            if (!this.communities[j]['isDelete']) {
              this._communityid.push({label: this.communities[j]['name'], value: this.communities[j]['id']});
            }
          }
          if (this.communityLocation) {
            console.log('this.communityLocation.id_community', this.communityLocation.id_community);
            this.communityLocation.id_community = this.communityLocation.id_community === undefined ? ''
              : this.communityLocation.id_community;
            this.selectedCommunityId = this.communityLocation.id_community === '' ? this._communityid.length > 0 ?
              String(this._communityid[0].value) : '' : this.communityLocation.id_community;
          }
        }
        , error => this.logError(error)
      );
    } else {
      this.routeTo('/login');
    }
  }

  submit() {

    if(this.selectedCommunityId) {
      console.log('enter', this.selectedCommunityId);

      this.communityLocationService.insert(
        this.name.toLowerCase().trim(),
        this.address,
        this.lat,
        this.long,
        this.isActive = true,
        this.isDelete = false,
        this.selectedCommunityId).subscribe(data => {
          console.log(data);
          if (data) {
            this.title = 'Submit Success';
            this.routeToGrid();
          }},
        err => {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Error Message', detail: 'Community Location Already exist' +
            ' OR Bad Request ' + err
          });
        });
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Missing Selection', detail: ' Community Name'
      });
    }
  }

  editCommunityLocation() {
    if (localStorage.getItem('editCommunityLocation')) {
      console.log('edit Update');
      if (this.communityLocation) {
        this.communityLocation.name = this.name !== undefined ? this.name.toLowerCase().trim()
          : this.name;
        this.communityLocation.address = this.address;
        this.communityLocation.lat = this.lat;
        this.communityLocation.long = this.long;
        this.communityLocation.isActive = this.isActive;
        this.communityLocation.isDelete = this.isDelete;
        this.communityLocation.id_community = this.selectedCommunityId;

        console.log('this.communityLocation.id', this.communityLocation.id);

        this.communityLocationService.updateBy(this.communityLocation.id, this.communityLocation)
          .subscribe(data => { console.log(data); },
            err => {
              this.msgs = [];
              this.msgs.push({
                severity: 'error', summary: 'Error Message', detail: 'Cannot Update Something Invalid'
              });
            });

        this.routeToGrid();
      }
    }
  }

  routeToGrid() {
    this.routeTo('/admin/community/location');
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
