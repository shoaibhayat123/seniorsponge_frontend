// core
import { Component, OnInit} from '@angular/core';

// services
import { CommunityLocationService } from '../../services/communityLocation.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

// interfaces
import { CommunityLocation } from '../../interfaces/communityLocation';
import { User } from '../../interfaces/user';

// routes
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-locationgrid',
  templateUrl: 'communityLocation.component.html',
  providers: [CommunityLocationService, UserService]
})

export class CommunityLocationComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  title;

  cols: any[];

  communityLocationgrids : any[] = [];

  communityLocation: CommunityLocation[]; loginuser: User;

  constructor(private communityLocationService: CommunityLocationService,
              private userService: UserService,
              public routerext: RouterExtensions) {
    this.communityLocationService.getAll().subscribe(communityLocation => { this.communityLocation = communityLocation;
        console.log('Grid Community Location', this.communityLocation);
        for(let j = 0; j < this.communityLocation.length ; j++) {
          let obj = {
            id: this.communityLocation[j]['id'],
            name : this.communityLocation[j]['name'],
            address : this.communityLocation[j]['address'],
            lat : this.communityLocation[j]['lat'],
            long : this.communityLocation[j]['long'],
            isActive : this.communityLocation[j]['isActive'],
            isDelete : this.communityLocation[j]['isDelete'],
            community_id : this.communityLocation[j]['id_community'].length > 0 ? this.communityLocation[j]['id_community'][0]['id'] : '',
            community_name: this.communityLocation[j]['id_community'].length > 0 ? this.communityLocation[j]['id_community'][0]['name'] :''
          };
          this.communityLocationgrids.push(obj);
        }
      }, error => this.logError(error)
    );

    this.cols = [
      {field: 'name' , header: 'Name'},
      {field: 'address' , header: 'Address'},
      {field: 'lat' , header: 'Latitude'},
      {field: 'long', header: 'Longitude'},
      {field: 'isDelete', header: 'IsDelete'},
      {field: 'isActive', header: 'IsActive'},
      {field: 'community_name', header: 'Community Name'}
    ];

  }

  ngOnInit() {

    localStorage.removeItem('editCommunityLocation');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });

    this.title = 'Community Location Grid';

    this.items = [
      {
        label: 'Add Community Location', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/community/location/submit');
      }
      },
      {
        label: 'Delete All Community Location', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];
  }

  // ngAfterViewInit() {
  //
  //   this.communityLocationService.getAll().subscribe(communityLocation => { this.communityLocation = communityLocation;
  //       console.log('Grid Community Location', this.communityLocation);
  //       for(let j = 0; j < this.communityLocation.length ; j++) {
  //         let obj = {
  //           id: this.communityLocation[j]['id'],
  //           name : this.communityLocation[j]['name'],
  //           address : this.communityLocation[j]['address'],
  //           lat : this.communityLocation[j]['lat'],
  //           long : this.communityLocation[j]['long'],
  //           isActive : this.communityLocation[j]['isActive'],
  //           isDelete : this.communityLocation[j]['isDelete'],
  //           community_id : this.communityLocation[j]['id_community'].length > 0 ? this.communityLocation[j]['id_community'][0]['id'] : '',
  //           community_name: this.communityLocation[j]['id_community'].length > 0 ? this.communityLocation[j]['id_community'][0]['name'] :''
  //         };
  //         this.communityLocationgrids.push(obj);
  //       }
  //     }, error => this.logError(error)
  //   );
  //
  //   this.cols = [
  //     {field: 'name' , header: 'Name'},
  //     {field: 'address' , header: 'Address'},
  //     {field: 'lat' , header: 'Latitude'},
  //     {field: 'long', header: 'Longitude'},
  //     {field: 'isDelete', header: 'IsDelete'},
  //     {field: 'isActive', header: 'IsActive'},
  //     {field: 'community_name', header: 'Community Name'}
  //   ];
  // }

  routeTo(pageroute: string)  {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.communityLocationService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.communityLocationgrids.length);
    this.communityLocationgrids.splice(0, this.communityLocationgrids.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All Community Locations Succesfully'});
  }

  edit(communityLocation: CommunityLocation) {
    localStorage.setItem('editCommunityLocation', JSON.stringify(communityLocation));
    this.routeTo('/admin/community/location/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + communityLocation.id});
  }

  delete(communityLocation: CommunityLocation) {
    this.msgs = [];
    this.communityLocationService.deleteBy(communityLocation.id)
      .subscribe(communityLocation => console.log(communityLocation));

    let indexToDelete: number = 0;
    indexToDelete = this.communityLocationgrids.findIndex((item) => item.id === communityLocation.id);
    this.communityLocationgrids.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + communityLocation.id});
  }

}
