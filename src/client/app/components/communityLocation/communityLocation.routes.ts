import { CommunityLocationComponent } from './communityLocation.component';
import { CommunityLocationSubmitComponent } from './communityLocationSubmit.component';

export const CommunityLocationRoutes: Array<any> = [
  {
    path: 'admin/community/location',
    component: CommunityLocationComponent
  },
  {
    path: 'admin/community/location/submit',
    component: CommunityLocationSubmitComponent
  }
];
