// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { User } from '../../interfaces/user';
import { UserType } from '../../interfaces/usertype';
import { Community } from '../../interfaces/community';

// services
import { UserService } from '../../services/user.service';
import { UserTypeService } from '../../services/usertype.service';
import { CommunityService } from '../../services/community.service';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

// password Hash lib
import { Endcrypt } from 'endcrypt/endcrypt';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  providers: [UserService, UserTypeService, CommunityService]
})

export class LoginComponent implements OnInit {

  title; lemail; lpassword;
  loginuser: User; user: User;
  usertypes: UserType[] = [];
  firstName; lastName; createdDate; createdBy; modifiedDate; lat; long; isActive;
  isDelete; userName; password; repassword; email; age; selectedUserType: string;

  communities : Community[] = [];
  msgs: Message[] = [];
  _usertypeid: SelectItem[];
  _communityid: SelectItem[];

  selectedCommunityId: string;

  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('closeLoginBtn') closeLoginBtn: ElementRef;

  constructor(private userService: UserService
              ,private userTypeService: UserTypeService
              ,private communityService: CommunityService
              ,private route: ActivatedRoute
              ,public routerext: RouterExtensions) {}

  ngOnInit() {

    this.title = 'LoginPage';
    if (localStorage.getItem('currentUser')) {
      this.routeTo('/dashboard');
    }

    this._usertypeid = [];

    // UserType Dropdown
    this.userTypeService.getAll().subscribe(usertypes => {
        this.usertypes = usertypes;
        for (let i = 0; i < this.usertypes.length; i++) {
          console.log('user types', this.usertypes);
          if (!this.usertypes[i]['isDelete']) {
            this._usertypeid.push({label: this.usertypes[i]['name'], value: this.usertypes[i]['id']});
          }
        }
      }
      , error => this.logError(error)
    );

    this._communityid = [];

    // Community Dropdown
    this.communityService.getAll().subscribe(communities => {
        this.communities = communities;
        for (let j = 0; j < this.communities.length; j++) {
          console.log('communities ', this.communities);
          if (!this.communities[j]['isDelete']) {
            this._communityid.push({label: this.communities[j]['name'], value: this.communities[j]['id']});
          }
        }
      }
      , error => this.logError(error)
    );
  }

  logError(err: any) {
    console.log('error' , err);
  }

  login() {
    console.log('email', this.lemail);
    console.log('password', this.lpassword);
    this.userService.getBy(this.lemail.toLowerCase().trim()).subscribe(finddata => {
      if (finddata) {
        let confirmPassword = this.decryptPassword(JSON.parse(finddata.password));
        if (confirmPassword === this.lpassword) {
          if (!finddata.isDelete) {
            finddata.id_userType = finddata.id_userType.length > 0 ?
              finddata.id_userType[0]['id'] : finddata.id_userType;
            finddata.community_id = finddata.community_id.length > 0 ?
              finddata.community_id[0]['id'] : finddata.community_id;
            finddata.isActive = true;
            localStorage.setItem('currentUser', JSON.stringify(finddata));
            this.userService.updateUser(this.lemail, finddata)
              .subscribe(data => console.log(data));
            this.closeLoginBtn.nativeElement.click();
            this.cleraAllfield();
            this.routeTo('/dashboard');
          } else {
            this.msgs = [];
            this.msgs.push({
              severity: 'info',
              summary: 'Info Message',
              detail: 'User Have Been Deleted By Admin'
            });
          }
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: 'warn',
            summary: 'Warn Message',
            detail: 'Enter Valid password'
          });
        }
      } else {
        this.msgs = [];
        this.msgs.push({severity: 'warn', summary: 'Warn Message', detail: 'No User Found'});
      }
    });
  }

  newUser() {
    if (localStorage.getItem('currentUser')) {
      this.loginuser = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.loginuser.userName);
      this.createdBy = this.loginuser.userName;
    }
    console.log(this.createdBy);
    console.log('this.selectedUserType', this.selectedUserType);
    console.log('this.selectedCommunityId', this.selectedCommunityId);
    if(this.password === this.repassword && this.selectedUserType && this.selectedCommunityId) {
      this.userService.signUp(
        this.firstName,
        this.lastName,
        this.createdDate = new Date().toDateString(),
        this.createdBy = 'admin',
        this.modifiedDate = new Date().toDateString(),
        this.lat,
        this.long,
        this.isActive = true,
        this.isDelete = false,
        this.userName.toLowerCase().trim(),
        JSON.stringify(this.encryptPassword(this.password)),
        this.email.toLowerCase().trim(),
        this.age,
        this.selectedUserType,
        this.selectedCommunityId).subscribe(
          (data) => {
            this.cleraAllfield();
            this.closeBtn.nativeElement.click(); },
        (err) => { this.msgs = [];
          this.msgs.push({severity:'error', summary:'Error Message', detail:'User Already Exist OR Remove Space from Start and End'}); });
    } else {
      console.log('this.selectedUserType error');
      this.msgs = [];
      this.msgs.push({severity:'error', summary:'Error Message', detail:'Password Not Matching OR Select User Type OR Community'});
    }

  }

  encryptPassword(userPassword): string {
    return new Endcrypt().encryptWithKey(userPassword, '16bytesecretkeys');
  }

  decryptPassword(encrypt): string {
    return new Endcrypt().decryptWithKey(encrypt, '16bytesecretkeys');
  }

  cleraAllfield() {
    this.firstName = this.password = this.lastName = this.createdBy = this.lat
    = this.long = this.userName = this.email = this.age = this.lemail = this.lpassword = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}

