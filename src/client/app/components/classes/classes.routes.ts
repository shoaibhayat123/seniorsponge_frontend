import { ClassesGridComponent } from './classesGrid.component';
import { ClassesSubmitComponent } from './classesSubmit.component';

export const ClassesRoutes: Array<any> = [
  {
    path: 'admin/classes',
    component: ClassesGridComponent
  },
  {
    path: 'admin/classes/submit',
    component: ClassesSubmitComponent
  }
];
