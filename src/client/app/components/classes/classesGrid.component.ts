// core
import { Component, OnInit} from '@angular/core';
// services
import { ClassesService } from '../../services/classes.service';
import { UserService } from '../../services/user.service';
// UI
import {MenuItem, Message } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
// interfaces
import { Classes } from '../../interfaces/classes';
import { User } from '../../interfaces/user';
// routes
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-classesgrid',
  templateUrl: 'classesGrid.component.html',
  providers: [ClassesService, UserService]
})

export class ClassesGridComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  title;

  cols: any[];

  classesgrids : any[] = [];

  classes: Classes[]; loginuser: User;

  constructor(private classesService: ClassesService,
              private userService: UserService,
              public routerext: RouterExtensions) {
    this.classesService.getAll().subscribe(classes => { this.classes = classes;
        console.log('grid classes', this.classes);
        for(let j = 0; j < this.classes.length ; j++) {
          let obj = {
            id: this.classes[j]['id'],
            name : this.classes[j]['name'],
            dateTime : this.classes[j]['dateTime'],
            startDate : this.classes[j]['startDate'],
            endDate : this.classes[j]['endDate'],
            days : this.classes[j]['days'],
            id_user : this.classes[j]['id_user'].length > 0 ? this.classes[j]['id_user'][0]['id'] : '',
            user_email: this.classes[j]['id_user'].length > 0 ? this.classes[j]['id_user'][0]['email'] : '',
            isPublic : this.classes[j]['isPublic'],
            community_id : this.classes[j]['community_id'].length > 0 ? this.classes[j]['community_id'][0]['id'] : '',
            community_name: this.classes[j]['community_id'].length > 0 ? this.classes[j]['community_id'][0]['name'] :''
          };
          this.classesgrids.push(obj);
        }
      }, error => this.logError(error)
    );

    this.cols = [
      {field: 'name' , header: 'Name'},
      {field: 'dateTime' , header: 'DateTime'},
      {field: 'startDate' , header: 'Start Date'},
      {field: 'endDate', header: 'End Date'},
      {field: 'days', header: 'Days'},
      {field: 'user_email', header: 'User Email'},
      {field: 'isPublic', header: 'IsPublic'},
      {field: 'community_name', header: 'Community Name'}
    ];
  }

  ngOnInit() {

    localStorage.removeItem('editClasses');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });

    this.title = 'Classes Grid';

    this.items = [
      {
        label: 'Add Classes', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/classes/submit');
      }
      },
      {
        label: 'Delete All Classes', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];
  }

  // ngAfterViewInit() {
  //
    // this.classesService.getAll().subscribe(classes => { this.classes = classes;
    //     console.log('grid classes', this.classes);
    //   for(let j = 0; j < this.classes.length ; j++) {
    //     let obj = {
    //       id: this.classes[j]['id'],
    //       name : this.classes[j]['name'],
    //       dateTime : this.classes[j]['dateTime'],
    //       startDate : this.classes[j]['startDate'],
    //       endDate : this.classes[j]['endDate'],
    //       days : this.classes[j]['days'],
    //       id_user : this.classes[j]['id_user'].length > 0 ? this.classes[j]['id_user'][0]['id'] : '',
    //       user_email: this.classes[j]['id_user'].length > 0 ? this.classes[j]['id_user'][0]['email'] : '',
    //       isPublic : this.classes[j]['isPublic'],
    //       community_id : this.classes[j]['community_id'].length > 0 ? this.classes[j]['community_id'][0]['id'] : '',
    //       community_name: this.classes[j]['community_id'].length > 0 ? this.classes[j]['community_id'][0]['name'] :''
    //     };
    //     this.classesgrids.push(obj);
    //   }
    //   }, error => this.logError(error)
    // );
    //
    // this.cols = [
    //   {field: 'name' , header: 'Name'},
    //   {field: 'dateTime' , header: 'DateTime'},
    //   {field: 'startDate' , header: 'Start Date'},
    //   {field: 'endDate', header: 'End Date'},
    //   {field: 'days', header: 'Days'},
    //   {field: 'user_email', header: 'User Email'},
    //   {field: 'isPublic', header: 'IsPublic'},
    //   {field: 'community_name', header: 'Community Name'}
    // ];
  // }

  routeTo(pageroute: string)  {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.classesService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.classesgrids.length);
    this.classesgrids.splice(0, this.classesgrids.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All Classes Succesfully'});
  }

  edit(classed: Classes) {
    localStorage.setItem('editClasses', JSON.stringify(classed));
    this.routeTo('/admin/classes/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + classed.name});
  }

  delete(classed: Classes) {
    this.msgs = [];
    this.classesService.deleteBy(classed.id)
      .subscribe(classed => { console.log(classed); },
        error => {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Delete'});
        });

    let indexToDelete: number = 0;
    indexToDelete = this.classesgrids.findIndex((item) => item.id === classed.id);
    this.classesgrids.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + classed.name});
  }

}
