// core libs
import { Component, OnInit } from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Event } from '../../interfaces/event';
import { User } from '../../interfaces/user';
import { Community } from '../../interfaces/community';

// services
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';
import { CommunityService } from '../../services/community.service';

// UI
import {MenuItem, Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-submitevent',
  templateUrl: 'eventSubmit.component.html',
  providers: [EventService, UserService, CommunityService]
})

export class EventSubmitComponent implements OnInit {

  title; edited;

  msgs: Message[] = [];

  _days: SelectItem[];
  _userid: SelectItem[];
  _communityid: SelectItem[];

  users : User[] = [];   communities : Community[] = [];

  selectedDays: string[];  selectedUserId: string;  selectedCommunityId: string;
  startdate; enddate;

  btnedit: boolean;
  btnsubmit: boolean;
  communitydropdown: boolean;

  name; dateTime; startDate; endDate; days; isPublic;

  event: Event; loginuser: User;

  constructor(private eventService: EventService
    , private userService: UserService
    ,private communityService: CommunityService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {

    // Days Multiselector
    this._days = [];
    this._days.push({label:'Monday', value:'Monday'});
    this._days.push({label:'Tuesday', value:'Tuesday'});
    this._days.push({label:'Wednesday', value:'Wednesday'});
    this._days.push({label:'Thursday', value:'Thursday'});
    this._days.push({label:'Friday', value:'Friday'});
    this._days.push({label:'Saturday', value:'Saturday'});
    this._days.push({label:'Sunday', value:'Sunday'});


  }

  logError(err: any) {
    console.log('error' , err);
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.title = 'Event Submit Page';

      if (!localStorage.getItem('isAdmin')) {
        this.routeTo('/admin/login');
      }

      this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

      this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
        console.log('finddata', finddata);
        if (finddata) {
          if (!finddata.isDelete) {
            let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
              : '';
            if (isAdmin.toLowerCase().trim() !== 'admin') {
              localStorage.removeItem('isAdmin');
              this.routeTo('/admin/login');
            }
          }
        }
      });

      this.btnsubmit = true;
      this.btnedit = false;
      this.communitydropdown = true;

      if (localStorage.getItem('editEvent')) {
        console.log('edit Initialize');
        this.event = JSON.parse(localStorage.getItem('editEvent'));
        if (this.event) {
          this.communitydropdown = false;
          this.btnedit = true;
          this.btnsubmit = false;
          this.edited = true;

          console.log('new Date(this.event.startDate).toLocaleString()', Date.parse(new Date(this.event.startDate).toLocaleString()));
          this.name = this.event.name;
          this.startdate = isNaN(Date.parse(new Date(this.event.startDate).toLocaleString())) ? new Date() : new Date(this.event.startDate);
          this.startDate = this.startdate.toLocaleString();
          this.enddate = isNaN(Date.parse(new Date(this.event.endDate).toLocaleString())) ? this.startdate : new Date(this.event.endDate);
          this.endDate = this.enddate.toLocaleString();
          this.selectedDays = this.days = this.removeDaysInvalidItem(this.event.days);
        }
      }

      this._userid = [];
      this._communityid = [];

      // User Dropdown
      this.userService.getUsers().subscribe(users => {
          this.users = users;
          this._userid.push({label: '', value: ''});
          for (let i = 0; i < this.users.length; i++) {
            if (!this.users[i]['isDelete']) {
              this._userid.push({label: this.users[i]['email'], value: this.users[i]['id']});
            }
          }
          if (this.event) {
            this.event.id_user = this.event.id_user === undefined ? ''
              : this.event.id_user;
            this.selectedUserId = this.event.id_user === '' ? this._userid.length > 0 ?
              String(this._userid[0].value) : '' : this.event.id_user;
          }
        }
        , error => this.logError(error)
      );

      // Community Dropdown
      this.communityService.getAll().subscribe(communities => {
          this.communities = communities;
          this._communityid.push({label: '', value: ''});
          for (let j = 0; j < this.communities.length; j++) {
            if (!this.communities[j]['isDelete']) {
              this._communityid.push({label: this.communities[j]['name'], value: this.communities[j]['id']});
            }
          }
          if (this.event) {
            this.event.community_id = this.event.community_id === undefined ? ''
              : this.event.community_id;
            this.selectedCommunityId = this.event.community_id === '' ? this._communityid.length > 0 ?
              String(this._communityid[0].value) : '' : this.event.community_id;
          }
        }
        , error => this.logError(error)
      );
    } else {
      this.routeTo('/login');
    }
  }

  removeDaysInvalidItem(daysarr1 : any[]) : any[] {
      let i: number = 0;
      let temparr : any[] = [];
      while (i < daysarr1.length) {
        for (let toRem of this._days) {
          if (toRem.value === daysarr1[i]) {
            temparr.push(daysarr1[i]);
            continue;
          }
        }
        ++i;
      }
      return temparr ;
  }

  getDaysinto(date1: string, date2: string): any[] {
    let startTime = new Date(date1).setHours(0);
    let timeDiff = Math.abs(new Date(date2).getTime() - new Date(startTime).getTime());
    let diff = Math.ceil(timeDiff / (1000 * 3600 * 24));
    console.log('diff', diff);
    let _days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let days : any[] = []; let temparr : any[] = [];
    for (let i = 0; i < diff; i++) {
      if ((new Date(date1).getDay() + i) >= _days.length) {
        let pushDay = (new Date(date1).getDay() + i) - ( _days.length);
        if(_days[pushDay] !== undefined) {
          if (days.indexOf(_days[pushDay]) === -1) {
            if (this.selectedDays.indexOf(_days[pushDay]) > -1) {
              days.push(_days[pushDay]);
            }
          }
          if(temparr.indexOf(_days[pushDay]) === -1) {
            temparr.push(_days[pushDay]);
          }
        }

      } else {
        let getDay = _days[new Date(date1).getDay() + i];
        if (getDay) {
          if(_days[new Date(date1).getDay() + i] !== undefined) {
            if (days.indexOf(_days[new Date(date1).getDay() + i]) === -1) {
              if (this.selectedDays.indexOf(_days[new Date(date1).getDay() + i]) > -1) {
                days.push(_days[new Date(date1).getDay() + i]);
              }
            }
            if (temparr.indexOf(_days[new Date(date1).getDay() + i]) === -1) {
              temparr.push(_days[new Date(date1).getDay() + i]);
            }
          }
        }
      }
    }
    console.log('temparr', temparr);
    if(days.length < 1) {
      console.log('iner days', days);
      days = temparr;
    }
    console.log('days', days);
    return days;
  }

  onChange() {
    if(this.selectedUserId) {
      this.userService.getById(this.selectedUserId).subscribe(finddata => {
        if (finddata) {
          this.selectedCommunityId = finddata.community_id[0]['id'];
          this.communitydropdown = false;
        }
      });
    }
  }

  submit() {

    if(this.selectedDays.length > 0 && this.selectedUserId
      && this.selectedCommunityId) {
      console.log('enter', this.selectedUserId, this.selectedCommunityId);

      this.eventService.insert(
        this.name.toLowerCase().trim(),
        this.startDate = this.startdate ? Date.parse(this.startdate.toLocaleString()) > Date.parse(new Date().toLocaleString()) ?
          this.startdate.toLocaleString() : new Date().toLocaleString() : new Date().toLocaleString(),
        this.endDate = this.enddate ? Date.parse(this.enddate.toLocaleString()) > Date.parse(this.startDate.toLocaleString()) ? this.enddate.toLocaleString()
          : this.startDate.toLocaleString() : this.startDate.toLocaleString(),
        this.selectedUserId,
        this.days = this.selectedDays = this.getDaysinto(this.startDate , this.endDate),
        this.selectedCommunityId).subscribe(data => {
          console.log(data);
          if (data) {
            this.title = 'Submit Success';
            this.routeToGrid();
          }},
        err => {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Error Message', detail: 'Class Already exist' +
            ' OR Bad Request ' + err
          });
        });
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Missing Selection', detail: 'Select User Email OR' +
        ' Community Name OR Days'
      });
    }
  }

  editEvent() {
    if (localStorage.getItem('editEvent')) {
      console.log('edit Update');
      if (this.event) {
        if (this.selectedUserId
          && this.selectedCommunityId) {
          this.event.name = this.name !== undefined ? this.name.toLowerCase().trim()
            : this.name;
          this.event.startDate = !isNaN(Date.parse(this.startDate)) ? Date.parse(this.startDate) < Date.parse(this.startdate.toLocaleString()) ?
            Date.parse(new Date().toLocaleString()) < Date.parse(this.startdate.toLocaleString()) ? this.startdate.toLocaleString()
              : new Date().toLocaleString()
            : new Date(this.startDate).toLocaleString()
            : new Date().toLocaleString();
          this.event.endDate = Date.parse(this.event.startDate) <= Date.parse(this.enddate.toLocaleString()) ? this.enddate.toLocaleString() : this.event.startDate;
          this.event.id_user = this.selectedUserId;
          this.event.days = this.days = this.selectedDays = this.getDaysinto(this.event.startDate , this.event.endDate);
          this.event.community_id = this.selectedCommunityId;

          console.log('this.event', this.event);

          this.eventService.updateBy(this.event.id, this.event)
            .subscribe(data => {
                console.log(data);
              },
              err => {
                this.msgs = [];
                this.msgs.push({
                  severity: 'error', summary: 'Error Message', detail: 'Cannot Update Something Invalid'
                });
              });

          this.routeToGrid();
        } else {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Missing Selection', detail: 'Select User Email OR' +
            ' Community Name OR Days'
          });
        }
      }
    }
  }

  routeToGrid() {
    this.routeTo('/admin/event');
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
