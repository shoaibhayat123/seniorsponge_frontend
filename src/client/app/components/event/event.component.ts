// core
import { Component, OnInit} from '@angular/core';
// services
import { EventService } from '../../services/event.service';
// UI
import {MenuItem, Message } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
// interfaces
import { Event } from '../../interfaces/event';
// routes
import { RouterExtensions, Config } from '../../modules/core/index';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'sd-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.css'],
  providers: [EventService]
})

export class EventComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  title;

  cols: any[];

  eventgrids : any[] = [];

  events: Event[];

  constructor(private eventService: EventService,
              public routerext: RouterExtensions) {}

  ngOnInit() {

    localStorage.removeItem('editEvent');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    $(function () {
      var options = {
        alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
      };
      $('.grid-stack').gridstack(options);
    });

    this.title = 'Event Grid';

    this.items = [
      {
        label: 'Add Event', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/event/submit');
      }
      },
      {
        label: 'Delete All Event', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];
  }

  ngAfterViewInit() {

    this.eventService.getAll().subscribe(events => { this.events = events;
        console.log('grid events', this.events);

        for(let j = 0; j < this.events.length ; j++) {
          console.log('this.events[j][id_user].length > 0 ' , this.events[j]['id_user'].length > 0 );
          let obj = {
            id: this.events[j]['id'],
            name : this.events[j]['name'],
            startDate : this.events[j]['startDate'],
            endDate : this.events[j]['endDate'],
            days : this.events[j]['days'],
            id_user : this.events[j]['id_user'].length > 0 ? this.events[j]['id_user'][0]['id'] : '',
            user_email: this.events[j]['id_user'].length > 0 ? this.events[j]['id_user'][0]['email'] : '',
            community_id : this.events[j]['community_id'].length > 0 ? this.events[j]['community_id'][0]['id'] : '',
            community_name: this.events[j]['community_id'].length > 0 ? this.events[j]['community_id'][0]['name'] :''
          };
          this.eventgrids.push(obj);
        }
      }, error => this.logError(error)
    );

    this.cols = [
      {field: 'name' , header: 'Name'},
      {field: 'startDate' , header: 'Start Date'},
      {field: 'endDate', header: 'End Date'},
      {field: 'days', header: 'Days'},
      {field: 'user_email', header: 'User Email'},
      {field: 'community_name', header: 'Community Name'}
    ];
  }

  routeTo(pageroute: string)  {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.eventService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.eventgrids.length);
    this.eventgrids.splice(0, this.eventgrids.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All Event Succesfully'});
  }

  edit(event: Event) {
    localStorage.setItem('editEvent', JSON.stringify(event));
    this.routeTo('/admin/event/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + event.name});
  }

  delete(event: Event) {
    this.msgs = [];
    this.eventService.deleteBy(event.id)
      .subscribe(classed => console.log(classed));

    let indexToDelete: number = 0;
    indexToDelete = this.eventgrids.findIndex((item) => item.id === event.id);
    this.eventgrids.splice(indexToDelete, 1);

    this.msgs.push({
      severity: 'info', summary: 'Success',
      detail: 'Data Deleted ' + event.name
    });
  }

}
