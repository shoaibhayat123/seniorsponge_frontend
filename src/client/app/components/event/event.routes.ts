import { EventGridComponent } from './eventGrid.component';
import { EventSubmitComponent } from './eventSubmit.component';
import { EventComponent } from './event.component';

export const EventRoutes: Array<any> = [
  {
    path: 'admin/event',
    component: EventGridComponent
  },
  {
    path: 'admin/event/submit',
    component: EventSubmitComponent
  },
  {
    path: 'admin/event/dashbaord',
    component: EventComponent
  }
];
