// app
import { HomeRoutes } from './home/home.routes';
import { AboutRoutes } from './about/about.routes';
import { LoginRoutes } from './login/login.routes';
import { SignUpRoutes } from './signup/signup.routes';
import { DashboardRoutes } from './dashboard/dashboard.routes';
import { CommunityRoutes } from './community/community.routes';
import { UserTypeRoutes } from './usertype/usertype.routes';
import { UserRoutes } from './user/user.routes';
import { CommunityMetaRoutes } from './communityMeta/communityMeta.routes';
import { ClassesRoutes } from './classes/classes.routes';
import { FileUploadRoutes } from './fileupload/fileupload.routes';
import { CommunityTypeRoutes } from './communityType/communityType.routes';
import { CommunityLocationRoutes } from './communityLocation/communityLocation.routes';
import { EventRoutes } from './event/event.routes';
import { AdminRoutes } from './admin/admin.routes';
import { GalleryRoutes } from './gallery/gallery.routes';
import { ImagesRoutes } from './gallery/images/images.routes';
import { AlbumRoutes } from './gallery/images/album.routes';
import { VideosRoutes } from './gallery/videos/videos.routes';
import { AudiosRoutes } from './gallery/audios/audios.routes';
import { DocumentsRoutes } from './gallery/documents/documents.routes';
import { TasksRoutes } from './todo/tasks.routes';
import { RSSFeedRoutes } from './rssFeed/rssFeed.routes';
import { EmailRoutes } from './email/email.routes';
import { GamesRoutes } from './games/games.routes';

export const routes: Array<any> = [
  ...HomeRoutes,
  ...AboutRoutes,
  ...LoginRoutes,
  ...SignUpRoutes,
  ...DashboardRoutes,
  ...CommunityRoutes,
  ...UserTypeRoutes,
  ...UserRoutes,
  ...ClassesRoutes,
  ...FileUploadRoutes,
  ...CommunityTypeRoutes,
  ...CommunityMetaRoutes,
  ...CommunityLocationRoutes,
  ...EventRoutes,
  ...AdminRoutes,
  ...GalleryRoutes,
  ...ImagesRoutes,
  ...AlbumRoutes,
  ...VideosRoutes,
  ...AudiosRoutes,
  ...DocumentsRoutes,
  ...TasksRoutes,
  ...RSSFeedRoutes,
  ...EmailRoutes,
  ...GamesRoutes
];
