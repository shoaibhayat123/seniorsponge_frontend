import { TasksComponent } from './task/task.component';
// import { TaskFormComponent } from './task-form/task-form.component';
// import { TaskListComponent } from './task-list/task-list.component';
// import { TaskItemComponent } from './task-item/task-item.component';

export const TasksRoutes: Array<any> = [
  {
    path: 'tasks',
    component: TasksComponent
  },
  // {
  //   path: 'tasks/form',
  //   component: TaskFormComponent
  // },
  // {
  //   path: 'tasks/list',
  //   component: TaskListComponent
  // },
  // {
  //   path: 'tasks/item',
  //   component: TaskItemComponent
  // },
];
