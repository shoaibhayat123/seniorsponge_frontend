// core
import { Component, OnInit } from '@angular/core';

// routes
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions, Config } from '../../../modules/core/index';

// Service
import { TasksService } from '../../../services/task.service';

// Interface & Model
import { ITask, Task } from '../../../interfaces/task';
import { User } from '../../../interfaces/user';

// UI
import {MenuItem, Message, ConfirmationService, SelectItem } from 'primeng/primeng';


@Component({
  moduleId: module.id,
  selector: 'app-tasks',
  templateUrl: 'task.component.html',
  providers: [TasksService, ConfirmationService]
})
export class TasksComponent implements OnInit {
  tasks: ITask[];
  user: User;
  title = ''; index;
  msgs: Message[] = [];
  incomplete : any[];
  edits : boolean = false;
  editTask : ITask;

  constructor(
    public route: ActivatedRoute,
    public tasksService: TasksService,
    public routerext: RouterExtensions,
    private confirmationService: ConfirmationService
  ) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }
  }

  routeTo(pageroute: string)  {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  clear(): void {
    this.title = '';
  }

  all(): void {
    this.tasksService.getByUserId(this.user.id).subscribe(tasks => {
            this.tasks = tasks;
            console.log(this.tasks);
          }, error => this.logError(error)
        );

    this.incomplete = [
      {field: 'id', header: 'ID'},
      {field: 'title', header: 'Title'},
      {field: 'id_user', header: 'User ID'},
      {field: 'createdAt', header: 'Created At'},
      {field: 'complete', header: 'Complete'}
    ];
  }

  completes(): void {
    this.tasksService.getByUserTaskComplete(this.user.id, true).subscribe(tasks => {
            this.tasks = tasks;
            console.log(this.tasks);
          }, error => this.logError(error)
        );

    this.incomplete = [
      {field: 'id', header: 'ID'},
      {field: 'title', header: 'Title'},
      {field: 'id_user', header: 'User ID'},
      {field: 'createdAt', header: 'Created At'},
      {field: 'complete', header: 'Complete'}
    ];
  }

  inCompletes(): void {
    this.tasksService.getByUserTaskComplete(this.user.id, false).subscribe(tasks => {
            this.tasks = tasks;
            console.log(this.tasks);
          }, error => this.logError(error)
        );

    this.incomplete = [
      {field: 'id', header: 'ID'},
      {field: 'title', header: 'Title'},
      {field: 'id_user', header: 'User ID'},
      {field: 'createdAt', header: 'Created At'},
      {field: 'complete', header: 'Complete'}
    ];
  }

  submit(): void {
    const title: string = this.title.trim();
    if (title.length) {
      if(this.edits && this.editTask) {
        this.editTask.title = title;
        this.tasksService.updateTask(this.editTask).subscribe(task => { console.log(task); },
          error => {
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Update'});
          });
        this.edits = false;
      } else {
        this.tasksService.createTask(title, this.user.id).subscribe(task => console.log(task));
      }
    }
    this.clear();
  }

  edit(task: ITask) {

    this.title = task.title;
    task.id_user = this.user.id;
    this.edits = true;
    this.editTask = task;
  }

  complete(task: ITask) {

    task.complete = true;
    task.id_user = this.user.id;
    this.tasksService.updateTask(task).subscribe(task => {
        console.log(task);
      },
      error => {
        this.msgs = [];
        this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Update'});
      });
  }

  delete(task: ITask) {
    this.msgs = [];
    this.tasksService.removeTask(task.id)
      .subscribe(task => { console.log(task); } ,
        error => {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Delete'});
        });

    let indexToDelete: number = 0;
    indexToDelete = this.tasks.findIndex((item) => item.id === task.id);
    this.tasks.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + task.id});
  }
}
