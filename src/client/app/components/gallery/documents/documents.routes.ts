import {DocumentsComponent} from './documents.component';

export const DocumentsRoutes: Array<any> = [
  {
    path: 'gallery/documents',
    component: DocumentsComponent
  }
];
