import {VideosComponent} from './videos.component';

export const VideosRoutes: Array<any> = [
  {
    path: 'gallery/videos',
    component: VideosComponent
  }
];
