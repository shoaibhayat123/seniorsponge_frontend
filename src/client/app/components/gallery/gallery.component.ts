// core
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
// services
import {UserService} from '../../services/user.service';
// interfaces
import {User} from '../../interfaces/user';
// routes
import {RouterExtensions, Config} from '../../modules/core/index';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'sd-gallery',
  templateUrl: 'gallery.component.html',
  styleUrls: ['gallery.component.css'],
  providers: [UserService]
})

export class GalleryComponent implements OnInit {

  title;

  user: User;

  constructor(private userServices: UserService,
              public routerext: RouterExtensions) {
  }

  ngOnInit() {
    console.log('hello to Gallery');
    $(function () {
      var options = {
        alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
      };
      $('.grid-stack').gridstack(options);
    });

    if (!localStorage.getItem('currentUser')) {
      this.routeToLogin();
    }
  }

  logout() {
    localStorage.removeItem('isAdmin');
    console.log(typeof localStorage.getItem('currentUser'));
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.user.isActive = false;
    console.log(this.user.email);

    this.userServices.updateUser(this.user.email, this.user)
      .subscribe(user => console.log(user));
    localStorage.removeItem('currentUser');
    this.routeToLogin();
  }

  routeToLogin() {
    this.routerext.navigate(['/login'], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  go_to(route) {
    this.routerext.navigate(['/' + route], {
      transition: {
        duration: 1000
      }
    });
  }
}
