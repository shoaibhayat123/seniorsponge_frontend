import {AudiosComponent} from './audios.component';

export const AudiosRoutes: Array<any> = [
  {
    path: 'gallery/images',
    component: AudiosComponent
  }
];
