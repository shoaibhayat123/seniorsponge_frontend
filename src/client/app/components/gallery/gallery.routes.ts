import {GalleryComponent} from './gallery.component';
import {AlbumComponent} from './images/album.component';
import {VideosComponent} from './videos/videos.component';
import {DocumentsComponent} from './documents/documents.component';
import {AudiosComponent} from './audios/audios.component';

export const GalleryRoutes: Array<any> = [
  {
    path: 'gallery',
    component: GalleryComponent
  },
  {
    path: 'user/album',
    component: AlbumComponent
  },
  {
    path: 'user/videos',
    component: VideosComponent
  },
  {
    path: 'user/documents',
    component: DocumentsComponent
  },
  {
    path: 'user/audios',
    component: AudiosComponent
  }

];
