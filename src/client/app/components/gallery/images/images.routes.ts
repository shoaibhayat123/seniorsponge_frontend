import {ImagesComponent} from './images.component';

export const ImagesRoutes: Array<any> = [
  {
    path: 'gallery/images',
    component: ImagesComponent
  }
];
