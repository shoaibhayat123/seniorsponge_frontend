// core
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
// services
import {UserService} from '../../../services/user.service';
import {GalleryService} from '../../../services/gallery.service';
// interfaces
import {User, Album} from '../../../interfaces/user';
// routes
import {RouterExtensions, Config} from '../../../modules/core/index';
import {Http, Headers, Response} from '@angular/http';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'sd-images',
  templateUrl: '../../../../app/components/gallery/images/album.component.html',
  styleUrls: ['images.component.css'],
  providers: [UserService, GalleryService]
})

export class AlbumComponent implements OnInit {

  title;

  user: User;
  load: any[];
  public albums: Album;
  albumName;
  albumThumbnail;
  createdDate;
  createdBy;
  modifiedDate;
  isActive;
  isDelete;

  constructor(private http: Http, private userServices: UserService, private galleryServices: GalleryService,
              public routerext: RouterExtensions) {
  }

  ngOnInit() {
    console.log('hello to Images Gallery');
    if (!localStorage.getItem('currentUser')) {
      this.routeToLogin();
    }
    this.getAlbums();
  }

  logout() {
    localStorage.removeItem('isAdmin');
    console.log(typeof localStorage.getItem('currentUser'));
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.user.isActive = false;
    console.log(this.user.email);

    this.userServices.updateUser(this.user.email, this.user)
      .subscribe(user => console.log(user));
    localStorage.removeItem('currentUser');
    this.routeToLogin();
  }

  createAlbum() {
    console.log('create', this.albumThumbnail);
    this.galleryServices.createAlbum(this.albumName,
      this.albumThumbnail,
      this.createdDate = new Date().toDateString(),
      this.createdBy = this.user['id'],
      this.modifiedDate = new Date().toDateString(),
      this.isActive = true,
      this.isDelete = false).subscribe(data => {
      console.log(data);
      if (data) {
        this.title = 'Album Created';
        this.routeToAlbum();
      } else {
        this.title = 'Something is missing or bad data';
      }
    });
  }

  go_to(route) {
    this.routerext.navigate(['/' + route], {
      transition: {
        duration: 1000
      }
    });
  }

  routeToLogin() {
    this.routerext.navigate(['/login'], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToAlbum() {
    this.routerext.navigate(['/gallery/album'], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  getAlbums() {
    this.galleryServices.getAlbums().subscribe(
      value => this.albums = value
    );
  }

  fileChanged(e: Event) {
    var target: HTMLInputElement = e.target as HTMLInputElement;
    this.load = [];

    for (var i = 0; i < target.files.length; i++) {
      console.log('target.files.length', target.files.length);
      console.log('target.files.length', target.files[i]);
      this.loadFile(target.files[i]);
    }
  }

  loadFile(img: File) {
    console.log('img', img);
    var formData: FormData = new FormData();
    formData.append('album_Thumbnail', img, img.name);
    console.log(formData);
    this.http.post('http://localhost:3000/user/upload', formData).subscribe(response => this.load.push(response.json()));
    // this.form.nativeElement.reset();
  }
}
