// core
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
// services
import {UserService} from '../../../services/user.service';
import {GalleryService} from '../../../services/gallery.service';
// interfaces
import {User, Image} from '../../../interfaces/user';
// routes
import {Http, Headers, Response} from '@angular/http';
import {RouterExtensions, Config} from '../../../modules/core/index';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'sd-images',
  templateUrl: '../../../../app/components/gallery/images/images.component.html',
  styleUrls: ['images.component.css'],
  providers: [UserService, GalleryService]
})

export class ImagesComponent implements OnInit {

  title;

  public albumId;
  public images: Image;
  user: User;
  load: any[];
  Image;
  createdDate;
  createdBy;
  modifiedDate;
  isActive;
  isDelete;

  constructor(private http: Http, private userServices: UserService, private galleryServices: GalleryService,
              private activatedRoute: ActivatedRoute, public routerext: RouterExtensions) {
  }

  ngOnInit() {
    console.log('hello to Images Gallery');

    if (!localStorage.getItem('currentUser')) {
      this.routeToLogin();
    }

    this.activatedRoute.params.subscribe((params: Params) => {
      this.albumId = params['albumId'];
    });
    this.getAlbumImages(this.albumId);
  }

  logout() {
    localStorage.removeItem('isAdmin');
    console.log(typeof localStorage.getItem('currentUser'));
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.user.isActive = false;
    console.log(this.user.email);

    this.userServices.updateUser(this.user.email, this.user)
      .subscribe(user => console.log(user));
    localStorage.removeItem('currentUser');
    this.routeToLogin();
  }

  createImage() {
    console.log('createImage', this.Image);
    this.galleryServices.createImage(this.albumId, this.Image, this.createdDate,
      this.createdBy, this.modifiedDate, this.isActive,
      this.isDelete).subscribe(data => {
      console.log(data);
      if (data) {
        this.title = 'Image Created';
      } else {
        this.title = 'Something is missing or bad data';
      }
    });
  }

  routeToLogin() {
    this.routerext.navigate(['/login'], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  go_to(route) {
    this.routerext.navigate(['/' + route], {
      transition: {
        duration: 1000
      }
    });
  }

  getAlbumImages(albumId) {
    this.galleryServices.getImagesByAlbum(albumId).subscribe(
      value => this.images = value);
  }

  fileChanged(e: Event) {
    var target: HTMLInputElement = e.target as HTMLInputElement;
    this.load = [];

    for (var i = 0; i < target.files.length; i++) {
      console.log('target.files.length', target.files.length);
      console.log('target.files.length', target.files[i]);
      this.loadFile(target.files[i]);
    }
  }

  loadFile(img: File) {
    console.log('img', img);
    var formData: FormData = new FormData();
    formData.append('Image', img, img.name);
    console.log(formData);
    // this.http.post('http://localhost:3000/user/upload/image', formData).subscribe(response => this.load.push(response.json()));
    // this.form.nativeElement.reset();
  }
}
