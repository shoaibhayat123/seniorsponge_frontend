import {AlbumComponent} from './album.component';
import {ImagesComponent} from './images.component';

export const AlbumRoutes: Array<any> = [
  {
    path: 'user/album',
    component: AlbumComponent
  },
  {
    path:'user/album/:albumId',
    component:ImagesComponent
  }
];
