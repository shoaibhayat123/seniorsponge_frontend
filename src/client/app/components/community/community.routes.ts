import { CommunityGridComponent } from './communityGrid.component';
import { AddEditCommunityComponent } from './addEditCommunity.component';

export const CommunityRoutes: Array<any> = [
  {
    path: 'admin/community',
    component: CommunityGridComponent
  },
  {
    path: 'admin/community/submit',
    component: AddEditCommunityComponent
  }
];
