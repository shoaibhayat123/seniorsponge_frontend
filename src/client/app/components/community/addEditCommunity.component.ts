// core libs
import { Component, OnInit } from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Community } from '../../interfaces/community';
import { User } from '../../interfaces/user';

// services
import { CommunityService } from '../../services/community.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-submitcommuntiy',
  templateUrl: 'addEditCommunity.component.html',
  providers: [CommunityService, UserService]
})

export class AddEditCommunityComponent implements OnInit {

  title; edited;

  msgs: Message[] = [];

  btnedit: boolean;
  btnsubmit: boolean;

  name; profileName; lat; long; isActive;
  isDelete; address;

  community: Community; loginuser: User;

  constructor(private communityService: CommunityService
    , private userService: UserService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.title = 'Community Submit Page';

      if (!localStorage.getItem('isAdmin')) {
        this.routeTo('/admin/login');
      }

      this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

      this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
        console.log('finddata', finddata);
        if (finddata) {
          if (!finddata.isDelete) {
            let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
              : '';
            if (isAdmin.toLowerCase().trim() !== 'admin') {
              localStorage.removeItem('isAdmin');
              this.routeTo('/admin/login');
            }
          }
        }
      });

      this.btnsubmit = true;
      this.btnedit = false;

      if (localStorage.getItem('editCommunity')) {
        console.log('edit Initialize');
        this.community = JSON.parse(localStorage.getItem('editCommunity'));
        console.log(this.community);
        this.btnedit = true;
        this.btnsubmit = false;
        this.edited = true;

        this.name = this.community.name;
        this.address = this.community.address;
        this.lat = this.community.lat;
        this.long = this.community.long;
        this.isActive = this.community.isActive;
        this.isDelete = this.community.isDelete;
        this.profileName = this.community.profileName;
      }
    } else {
      this.routeTo('/login');
    }
  }

  submit() {
    console.log('enter');
    this.communityService.insert(
      this.name.toLowerCase().trim(),
      this.address,
      this.lat,
      this.long,
      this.isActive = true,
      this.isDelete = false,
      this.profileName).subscribe(data => {
      console.log(data);
        this.routeToGrid();
    },
    error => {
      this.msgs = [];
      this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Community Already Exist'});
    });
  }

    editCommunity() {
      if (localStorage.getItem('editCommunity')) {
        console.log('edit Update');

        if (this.community) {
          this.community.name = this.name !== undefined ? this.name.toLowerCase().trim()
            : this.name;
          this.community.address = this.address;
          this.community.lat = this.lat;
          this.community.long = this.long;
          this.community.isActive = this.isActive;
          this.community.isDelete = this.isDelete;
          this.community.profileName = this.profileName;

          this.communityService.updateBy(this.community.name, this.community)
            .subscribe(data => { console.log(data); } ,
              error => {
                this.msgs = [];
                this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Update'});
              });

          this.routeToGrid();
        }
      }
    }

    routeToGrid() {
      this.routeTo('/admin/community');
    }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
