// core
import { Component, OnInit , ViewEncapsulation} from '@angular/core';

// interfaces
import { Community } from '../../interfaces/community';
import { User } from '../../interfaces/user';

// services
import { CommunityService } from '../../services/community.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message } from 'primeng/primeng';

// routes
import { RouterExtensions, Config } from '../../modules/core/index';


@Component({
  moduleId: module.id,
  selector: 'sd-communitygrid',
  templateUrl: 'communityGrid.component.html',
  providers: [CommunityService, UserService]
})

export class CommunityGridComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  title;

  cols: any[];

  communities: Community[]; loginuser: User;

  constructor(private communityService: CommunityService,
              private userService: UserService,
              public routerext: RouterExtensions) {
    this.communityService.getAll().subscribe(communities => { this.communities = communities;
        console.log(this.communities);
      }, error => this.logError(error)
    );

    this.cols = [
      {field: 'name' , header: 'Name'},
      {field: 'profileName' , header: 'ProfileName'},
      {field: 'address', header: 'Address'},
      {field: 'lat', header: 'Latitude'},
      {field: 'long', header: 'Longitude'},
      {field: 'isActive', header: 'IsActive'},
      {field: 'isDelete', header: 'IsDelete'}
    ];
  }

  async ngOnInit() {

    localStorage.removeItem('editCommunity');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });

    this.items = [
      {
        label: 'Add Community', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/community/submit');
      }
      },
      {
        label: 'Delete All Communities', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];

    this.title = 'Community Grid';

  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.communityService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.communities.length);
    this.communities.splice(0, this.communities.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All Communities Succesfully'});
  }

  edit(community: Community) {
    localStorage.setItem('editCommunity', JSON.stringify(community));
    this.routeTo('/admin/community/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + community.name});
  }

  delete(community: Community) {
    this.msgs = [];
    this.communityService.deleteBy(community.name.toLowerCase())
      .subscribe(community => { console.log(community); } ,
        error => {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Delete'});
        });

    let indexToDelete: number = 0;
    indexToDelete = this.communities.findIndex((item) => item.name.toLowerCase() === community.name.toLowerCase());
    this.communities.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + community.name});
  }

}
