// core
import { Component, OnInit } from '@angular/core';
// services
import { UserService } from '../../services/user.service';
// interfaces
import { User } from '../../interfaces/user';
// UI
import {Message } from 'primeng/primeng';
// routes
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-admindashboard',
  templateUrl: 'admindashboard.component.html',
  providers: [UserService]
})
export class AdminDashboardComponent implements OnInit {

  title;
  email;
  username;
  password;
  user: User[];
  msgs: Message[] = [];
  loginuser: User;

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              public routerext: RouterExtensions) {
  }

  ngOnInit() {
    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToCommunity() {
    this.routeTo('/admin/community');
  }

  routeToCommunityType() {
    this.routeTo('/admin/community/type');
  }

  routeToCommunityMeta() {
     this.routeTo('/admin/community/meta');
    }

  routeToCommunityLocation() {
     this.routeTo('/admin/community/location');
    }

  routeToUser() {
    this.routeTo('/admin/user');
  }

  routeToEvent() {
     this.routeTo('/admin/event');
    }

  routeToEventUI() {
    this.routeTo('/admin/event/dashbaord');
  }

  routeToUserType() {
    this.routeTo('/admin/usertype');
  }

  routeToClasses() {
    this.routeTo('/admin/classes');
  }

  routeToFileUpload() {
    this.routeTo('/admin/upload');
  }

  routeToNewsFeed() {
    this.routeTo('/admin/feed');
  }

  routeToDashboard() {
    this.routeTo('/dashboard');
  }

  logout() {
    localStorage.removeItem('isAdmin');
    this.routeToDashboard();
  }

}
