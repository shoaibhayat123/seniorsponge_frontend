import { AdminDashboardComponent } from './admindashboard.component';
import { AdminLoginComponent } from './adminlogin.component';

export const AdminRoutes: Array<any> = [
  {
    path: 'admin/dashboard',
    component: AdminDashboardComponent
  },
  {
    path: 'admin/login',
    component: AdminLoginComponent
  }
];
