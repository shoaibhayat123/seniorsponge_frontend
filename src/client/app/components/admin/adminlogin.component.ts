// core
import { Component, OnInit } from '@angular/core';
// services
import { UserService } from '../../services/user.service';
// interfaces
import { User } from '../../interfaces/user';
// UI
import {Message } from 'primeng/primeng';
// password Hash lib
import { Endcrypt } from 'endcrypt/endcrypt';
// routes
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-adminlogin',
  templateUrl: 'adminlogin.component.html',
  // templateUrl: '../../../assets/index.html',
  providers: [UserService]
})
export class AdminLoginComponent implements OnInit {

  title;
  email;
  username;
  password;
  user: User[];
  msgs: Message[] = [];

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              public routerext: RouterExtensions) {
  }

  ngOnInit() {
    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }
  }

  login() {

    this.userService.getBy(this.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        let confirmPassword = this.decryptPassword(JSON.parse(finddata.password));
        if (confirmPassword === this.password) {
          if (!finddata.isDelete) {
            let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
              : '';
            if (isAdmin.toLowerCase().trim() === 'admin') {
              localStorage.setItem('isAdmin', JSON.stringify(finddata));
              this.routeTo('/admin/dashboard');
            } else {
              this.msgs = [];
              this.msgs.push({severity: 'warn', summary: 'Warn Message', detail: 'Enter valid Admin Account'});
            }
          } else {
            this.msgs = [];
            this.msgs.push({severity: 'Error', summary: 'Error Message', detail: 'Account Have Been Deleted By Admin'});
          }
        } else {
          this.msgs = [];
          this.msgs.push({severity: 'warn', summary: 'Warn Message', detail: 'Enter Valid password'});
        }
      } else {
        this.msgs = [];
        this.msgs.push({severity: 'Error', summary: 'Error Message', detail: 'No User Found'});
      }
    });
  }

  encryptPassword(userPassword): string {
    return new Endcrypt().encryptWithKey(userPassword, '16bytesecretkeys');
  }

  decryptPassword(encrypt): string {
    return new Endcrypt().decryptWithKey(encrypt, '16bytesecretkeys');
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToDashboard() {
    this.routeTo('/dashboard');
  }
}
