// core
import { Component } from '@angular/core';
// services
import { RSSFeedService } from '../../services/rssFeed.service';
// routes libs
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'feed-component',
  templateUrl: 'rssFeed.component.html',
  providers: [RSSFeedService]
})

export class RSSFeedComponent {
  data; title;
  espndata : any[] = []; cnbcdata : any[] = []; bbcdata : any[] = [];
  feed : any[] = [];
  constructor(private rssFeedService: RSSFeedService
  ,public routerext: RouterExtensions) {}

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.title = 'Feed Page';

      if (!localStorage.getItem('isAdmin')) {
        this.routeTo('/admin/login');
      }

      //   BBC News

      this.rssFeedService.getChannelORNewsInfo('https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=60eeb3fcaf11486eb8b56b626ad8cd7a'
      ).subscribe(data => {
          console.log(JSON.stringify(data));
          for(let i = 0; i < data.articles.length; i++) {
            console.log('BBC data.articles[',i,']', data.articles[i]);
            this.bbcdata.push(data.articles[i]);
          }
        },
        err => {
          console.log(err);
        });

      //   CNBC News

      this.rssFeedService.getChannelORNewsInfo('https://newsapi.org/v1/articles?source=cnbc&sortBy=top&apiKey=60eeb3fcaf11486eb8b56b626ad8cd7a'
      ).subscribe(data => {
          console.log(JSON.stringify(data));
          for(let i = 0; i < data.articles.length; i++) {
            console.log('CNBC data.articles[',i,']', data.articles[i]);
            this.cnbcdata.push(data.articles[i]);
          }
        },
        err => {
          console.log(err);
        });

      //   ESPN News

      this.rssFeedService.getChannelORNewsInfo('https://newsapi.org/v1/articles?source=espn-cric-info&sortBy=latest&apiKey=60eeb3fcaf11486eb8b56b626ad8cd7a'
      ).subscribe(data => {
          console.log(JSON.stringify(data));
          for(let i = 0; i < data.articles.length; i++) {
            console.log('ESPN data.articles[',i,']', data.articles[i]);
            this.espndata.push(data.articles[i]);
          }
        },
        err => {
          console.log(err);
        });

    } else {
      this.routeTo('/login');
    }
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
