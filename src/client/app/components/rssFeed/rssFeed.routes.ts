import { RSSFeedComponent } from './rssFeed.component';

export const RSSFeedRoutes: Array<any> = [
  {
    path: 'admin/feed',
    component: RSSFeedComponent
  }
];
