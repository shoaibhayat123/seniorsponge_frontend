import { CommunityTypeGridComponent } from './communityTypeGrid.component';
import { CommunityTypeSubmitComponent } from './communityTypeSubmit.component';

export const CommunityTypeRoutes: Array<any> = [
  {
    path: 'admin/community/type',
    component: CommunityTypeGridComponent
  },
  {
    path: 'admin/community/type/submit',
    component: CommunityTypeSubmitComponent
  }
];
