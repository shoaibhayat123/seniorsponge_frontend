// core libs
import { Component, OnInit } from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { CommunityType } from '../../interfaces/communityType';
import { User } from '../../interfaces/user';
import { UserType } from '../../interfaces/usertype';
import { Community } from '../../interfaces/community';

// services
import { CommunityTypeService } from '../../services/communityType.service';
import { UserService } from '../../services/user.service';
import { UserTypeService } from '../../services/usertype.service';
import { CommunityService } from '../../services/community.service';

// UI
import {MenuItem, Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-submitcommunitytype',
  templateUrl: 'communityTypeSubmit.component.html',
  providers: [CommunityTypeService, UserService, UserTypeService, CommunityService]
})

export class CommunityTypeSubmitComponent implements OnInit {

  title; edited;

  msgs: Message[] = [];

  _days: SelectItem[];
  _userid: SelectItem[];
  _communityid: SelectItem[];
  _usertypeid: SelectItem[];

  users : User[] = [];   communities : Community[] = []; usertypes : UserType[] = [];

  selectedUserType: string;  selectedUserId: string;  selectedCommunityId: string;

  btnedit: boolean;
  btnsubmit: boolean;
  dropdown = false;
  userdropdown = false;

  isActive; loginuser: User;

  communityType: CommunityType;

  constructor(private communityTypeService: CommunityTypeService
    , private userTypeService: UserTypeService
    , private userService: UserService
    ,private communityService: CommunityService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  logError(err: any) {
    console.log('error' , err);
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.title = 'Community Type Submit Page';
      this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

      if (!localStorage.getItem('isAdmin')) {
        this.routeTo('/admin/login');
      }

      this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
        console.log('finddata', finddata);
        if (finddata) {
          if (!finddata.isDelete) {
            let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
              : '';
            if (isAdmin.toLowerCase().trim() !== 'admin') {
              localStorage.removeItem('isAdmin');
              this.routeTo('/admin/login');
            }
          }
        }
      });

      this.btnsubmit = true;
      this.btnedit = false;
      this.dropdown = true;

      if (localStorage.getItem('editCommunityType')) {
        console.log('edit Initialize');
        this.communityType = JSON.parse(localStorage.getItem('editCommunityType'));
        console.log('this.communityType', this.communityType);
        if (this.communityType) {
          console.log('submit communityType', this.communityType.id_user
            , this.communityType.id_community, this.communityType.id_userType);
          this.btnedit = true;
          this.btnsubmit = false;
          this.edited = true;
          this.dropdown = false;

          this.isActive = this.communityType.isActive;
        }
      }

      this._userid = [];
      this._communityid = [];
      this._usertypeid = [];

      // User Dropdown
      this.userService.getUsers().subscribe(users => {
          this.users = users;
          for (let i = 0; i < this.users.length; i++) {
            console.log('users ', this.users);
            if (!this.users[i]['isDelete']) {
              this._userid.push({label: this.users[i]['email'], value: this.users[i]['id']});
            }
          }
          if (this.communityType) {
            console.log('this.communityType.id_user', this.communityType.id_user);
            this.communityType.id_user = this.communityType.id_user === undefined ? ''
              : this.communityType.id_user;
            this.selectedUserId = this.communityType.id_user === '' ? this._userid.length > 0 ?
              String(this._userid[0].value) : '' : this.communityType.id_user;
          }
        }
        , error => this.logError(error)
      );

      // User Type Dropdown
      this.userTypeService.getAll().subscribe(usertypes => {
          this.usertypes = usertypes;
          this._usertypeid.push({label: '', value: ''});
          for (let i = 0; i < this.usertypes.length; i++) {
            console.log('user types', this.usertypes);
            if (!this.usertypes[i]['isDelete']) {
              this._usertypeid.push({label: this.usertypes[i]['name'], value: this.usertypes[i]['id']});
            }
          }
          if (this.communityType) {
            console.log('this.communityType.id_userType', this.communityType.id_userType);
            this.communityType.id_userType = this.communityType.id_userType === undefined ? ''
              : this.communityType.id_userType;
            this.selectedUserType = this.communityType.id_userType === '' ? this._usertypeid.length > 0 ?
              String(this._usertypeid[0].value) : '' : this.communityType.id_userType;
          }
        }
        , error => this.logError(error)
      );


      // Community Dropdown
      this.communityService.getAll().subscribe(communities => {
          this.communities = communities;
          this._communityid.push({label: '', value: ''});
          for (let j = 0; j < this.communities.length; j++) {
            console.log('communities ', this.communities);
            if (!this.communities[j]['isDelete']) {
              this._communityid.push({label: this.communities[j]['name'], value: this.communities[j]['id']});
            }
          }
          if (this.communityType) {
            console.log('this.communityType.id_community', this.communityType.id_community);
            this.communityType.id_community = this.communityType.id_community === undefined ? ''
              : this.communityType.id_community;
            this.selectedCommunityId = this.communityType.id_community === '' ? this._communityid.length > 0 ?
              String(this._communityid[0].value) : '' : this.communityType.id_community;
          }
        }
        , error => this.logError(error)
      );
    } else {
      this.routeTo('/login');
    }
  }

  onUserChange() {
    if(this.selectedUserId) {
      this.userService.getById(this.selectedUserId).subscribe(finddata => {
        if (finddata) {
          this.selectedCommunityId = finddata.community_id[0]['id'];
          this.dropdown = false;
        }
      });
    }
  }

  onUserTypeChange() {
    if(this.selectedUserType) {
      this.userdropdown = true;
      this.userService.getByUserType(this.selectedUserType).subscribe(users => {
        if (users) {
          this.selectedUserId = this.selectedCommunityId = '';
          console.log('users', users);
          this.users = users;
          this._userid = [];
          this._userid.push({label: '', value: ''});
          for (let i = 0; i < this.users.length; i++) {
            if (!this.users[i]['isDelete']) {
              this._userid.push({label: this.users[i]['email'], value: this.users[i]['id']});
            }
          }
        }
      });
    }
  }

  submit() {

    if(this.selectedUserId && this.selectedCommunityId && this.selectedUserType) {
      console.log('enter', this.selectedUserId, this.selectedCommunityId, this.selectedUserType);

      this.communityTypeService.insert(
        this.selectedUserType,
        this.selectedCommunityId,
        this.selectedUserId,
        this.isActive = true).subscribe(data => {
          console.log(data);
          if (data) {
            this.title = 'Submit Success';
            this.routeToGrid();
          }},
        err => {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Error Message', detail: 'Community Type Already exist' +
            ' OR Bad Request ' + err
          });
        });
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Missing Selection', detail: 'Select User Email OR' +
        ' Community Name OR Community Type'
      });
    }
  }

  editCommunityType() {
    if (localStorage.getItem('editCommunityType')) {
      console.log('edit Update');
      if (this.communityType) {
        if (this.selectedUserId && this.selectedCommunityId && this.selectedUserType) {
          console.log('selectedUserType', this.selectedUserType);
          this.communityType.id_userType = this.selectedUserType;
          console.log('selectedCommunityId', this.selectedCommunityId);
          this.communityType.id_community = this.selectedCommunityId;
          console.log('selectedUserId', this.selectedUserId);
          this.communityType.id_user = this.selectedUserId;
          this.communityType.isActive = this.isActive;

          console.log('this.communityType', this.communityType);

          this.communityTypeService.updateBy(this.communityType.id, this.communityType)
            .subscribe(data => {
                console.log(data);
              },
              err => {
                this.msgs = [];
                this.msgs.push({
                  severity: 'error', summary: 'Error Message', detail: 'Cannot Update Something Invalid'
                });
              });

          this.routeToGrid();
        }
      }
    }
  }

  routeToGrid() {
    this.routeTo('/admin/community/type');
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
