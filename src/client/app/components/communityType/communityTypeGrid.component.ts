// core
import { Component, OnInit} from '@angular/core';

// services
import { CommunityTypeService } from '../../services/communityType.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

// interfaces
import { CommunityType } from '../../interfaces/communityType'
import { User } from '../../interfaces/user';

// routes
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-communitytypegrid',
  templateUrl: 'communityTypeGrid.component.html',
  providers: [CommunityTypeService, UserService]
})

export class CommunityTypeGridComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  title;

  cols: any[]; loginuser: User;

  communityTypegrids : any[] = [];

  communitiesType: CommunityType[];

  constructor(private communityTypeService: CommunityTypeService,
              private userService: UserService,
              public routerext: RouterExtensions) {
    this.communityTypeService.getAll().subscribe(communitiesType => { this.communitiesType = communitiesType;
        console.log('grid communitiesType', this.communitiesType);

        for(let j = 0; j < this.communitiesType.length ; j++) {
          console.log('this.communitiesType[j]["id_userType"].length',this.communitiesType[j]['id_userType']);
          console.log('this.communitiesType[j]["id_user"].length',this.communitiesType[j]['id_user']);
          console.log('this.communitiesType[j]["id_community"].length',this.communitiesType[j]['id_community']);
          let obj = {
            id: this.communitiesType[j]['id'],
            id_userType : this.communitiesType[j]['id_userType'].length > 0 ? this.communitiesType[j]['id_userType'][0]['id'] : '',
            usertype_name: this.communitiesType[j]['id_userType'].length > 0 ? this.communitiesType[j]['id_userType'][0]['name'] : '',
            id_user : this.communitiesType[j]['id_user'].length > 0 ? this.communitiesType[j]['id_user'][0]['id'] : '',
            user_email: this.communitiesType[j]['id_user'].length > 0 ? this.communitiesType[j]['id_user'][0]['email'] : '',
            id_community : this.communitiesType[j]['id_community'].length > 0 ? this.communitiesType[j]['id_community'][0]['id'] : '',
            community_name: this.communitiesType[j]['id_community'].length > 0 ? this.communitiesType[j]['id_community'][0]['name'] :'',
            isActive: this.communitiesType[j]['isActive']
          };
          this.communityTypegrids.push(obj);
        }
      }, error => this.logError(error)
    );

    this.cols = [
      {field: 'id' , header: 'Community Type ID'},
      {field: 'usertype_name' , header: 'UserType Name'},
      {field: 'user_email', header: 'User Email'},
      {field: 'community_name', header: 'Community Name'}
    ];
  }

  ngOnInit() {


    localStorage.removeItem('editCommunityType');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }


    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });


    this.title = 'Community Type Grid';

    this.items = [
      {
        label: 'Add Community Type', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/community/type/submit');
      }
      },
      {
        label: 'Delete All Community Type', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];
  }

  // ngAfterViewInit() {
  //
    // this.communityTypeService.getAll().subscribe(communitiesType => { this.communitiesType = communitiesType;
    //     console.log('grid communitiesType', this.communitiesType);
    //
    //     for(let j = 0; j < this.communitiesType.length ; j++) {
    //       console.log('this.communitiesType[j]["id_userType"].length',this.communitiesType[j]['id_userType']);
    //       console.log('this.communitiesType[j]["id_user"].length',this.communitiesType[j]['id_user']);
    //       console.log('this.communitiesType[j]["id_community"].length',this.communitiesType[j]['id_community']);
    //       let obj = {
    //         id: this.communitiesType[j]['id'],
    //         id_userType : this.communitiesType[j]['id_userType'].length > 0 ? this.communitiesType[j]['id_userType'][0]['id'] : '',
    //         usertype_name: this.communitiesType[j]['id_userType'].length > 0 ? this.communitiesType[j]['id_userType'][0]['name'] : '',
    //         id_user : this.communitiesType[j]['id_user'].length > 0 ? this.communitiesType[j]['id_user'][0]['id'] : '',
    //         user_email: this.communitiesType[j]['id_user'].length > 0 ? this.communitiesType[j]['id_user'][0]['email'] : '',
    //         id_community : this.communitiesType[j]['id_community'].length > 0 ? this.communitiesType[j]['id_community'][0]['id'] : '',
    //         community_name: this.communitiesType[j]['id_community'].length > 0 ? this.communitiesType[j]['id_community'][0]['name'] :'',
    //         isActive: this.communitiesType[j]['isActive']
    //       };
    //       this.communityTypegrids.push(obj);
    //     }
    //   }, error => this.logError(error)
    // );
    //
    // this.cols = [
    //   {field: 'id' , header: 'Community Type ID'},
    //   {field: 'usertype_name' , header: 'UserType Name'},
    //   {field: 'user_email', header: 'User Email'},
    //   {field: 'community_name', header: 'Community Name'}
    // ];
  // }

  routeTo(pageroute: string)  {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.communityTypeService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.communityTypegrids.length);
    this.communityTypegrids.splice(0, this.communityTypegrids.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All Community Type Succesfully'});
  }

  edit(communityType: CommunityType) {
    localStorage.setItem('editCommunityType', JSON.stringify(communityType));
    this.routeTo('/admin/community/type/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + communityType.id});
  }

  delete(communityType: CommunityType) {
    this.msgs = [];
    this.communityTypeService.deleteBy(communityType.id)
      .subscribe(communityType => console.log(communityType));

    let indexToDelete: number = 0;
    indexToDelete = this.communityTypegrids.findIndex((item) => item.id === communityType.id);
    this.communityTypegrids.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + communityType.id});
  }

}
