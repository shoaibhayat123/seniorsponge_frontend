// core
import { Component, OnInit} from '@angular/core';
// services
import { UserService } from '../../services/user.service';
// UI
import {MenuItem, Message } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
// interfaces
import { User } from '../../interfaces/user';
// routes
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-usergrid',
  templateUrl: 'userGrid.component.html',
  providers: [UserService]
})

export class UserGridComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  usersgrids : any[] = [];

  title;

  cols: any[];

  users: User[]; loginuser: User;

  constructor(private userService: UserService,
              public routerext: RouterExtensions) {

    this.userService.getUsers().subscribe(users => { this.users = users;
        console.log('grid communitiesType', this.users);

        for(let j = 0; j < this.users.length ; j++) {
          console.log('this.users[j]["id_userType"].length',this.users[j]['id_userType']);
          let obj = {
            id: this.users[j]['id'],
            firstName: this.users[j]['firstName'],
            lastName: this.users[j]['lastName'],
            userName: this.users[j]['userName'],
            email: this.users[j]['email'],
            password: this.users[j]['password'],
            age: this.users[j]['age'],
            createdDate: this.users[j]['createdDate'],
            createdBy: this.users[j]['createdBy'],
            modifiedDate: this.users[j]['modifiedDate'],
            lat: this.users[j]['lat'],
            long: this.users[j]['long'],
            isActive: this.users[j]['isActive'],
            isDelete: this.users[j]['isDelete'],
            id_userType: this.users[j]['id_userType'].length > 0 ? this.users[j]['id_userType'][0]['id'] : '',
            usertype_name: this.users[j]['id_userType'].length > 0 ? this.users[j]['id_userType'][0]['name'] : '',
            community_id: this.users[j]['community_id'].length > 0 ? this.users[j]['community_id'][0]['id'] : '',
            community_name: this.users[j]['community_id'].length > 0 ? this.users[j]['community_id'][0]['name'] : ''
          };
          this.usersgrids.push(obj);
        }
        this.users = this.usersgrids;
      }, error => this.logError(error)
    );

    this.cols = [
      {field: 'firstName' , header: 'FirstName'},
      {field: 'lastName' , header: 'LastName'},
      {field: 'userName' , header: 'UserName'},
      {field: 'email', header: 'Email'},
      {field: 'createdDate', header: 'CreatedDate'},
      {field: 'createdBy', header: 'CreatedBy'},
      {field: 'modifiedDate', header: 'ModifiedDate'},
      {field: 'lat', header: 'Latitude'},
      {field: 'long', header: 'Longitude'},
      {field: 'age', header: 'age'},
      {field: 'usertype_name', header: 'User Type'},
      {field: 'community_name', header: 'Community'}

    ];

  }

  ngOnInit() {

    localStorage.removeItem('editUser');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });

    this.title = 'User Grid';

    this.items = [
      {
        label: 'Add User', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/user/submit');
      }
      },
      {
        label: 'Delete All Users', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];

  }

  // ngAfterViewInit() {
  //
  //   this.userService.getUsers().subscribe(users => { this.users = users;
  //       console.log('grid communitiesType', this.users);
  //
  //       for(let j = 0; j < this.users.length ; j++) {
  //         console.log('this.users[j]["id_userType"].length',this.users[j]['id_userType']);
  //         let obj = {
  //           id: this.users[j]['id'],
  //           firstName: this.users[j]['firstName'],
  //           lastName: this.users[j]['lastName'],
  //           userName: this.users[j]['userName'],
  //           email: this.users[j]['email'],
  //           password: this.users[j]['password'],
  //           age: this.users[j]['age'],
  //           createdDate: this.users[j]['createdDate'],
  //           createdBy: this.users[j]['createdBy'],
  //           modifiedDate: this.users[j]['modifiedDate'],
  //           lat: this.users[j]['lat'],
  //           long: this.users[j]['long'],
  //           isActive: this.users[j]['isActive'],
  //           isDelete: this.users[j]['isDelete'],
  //           id_userType: this.users[j]['id_userType'].length > 0 ? this.users[j]['id_userType'][0]['id'] : '',
  //           usertype_name: this.users[j]['id_userType'].length > 0 ? this.users[j]['id_userType'][0]['name'] : '',
  //           community_id: this.users[j]['community_id'].length > 0 ? this.users[j]['community_id'][0]['id'] : '',
  //           community_name: this.users[j]['community_id'].length > 0 ? this.users[j]['community_id'][0]['name'] : ''
  //         };
  //         this.usersgrids.push(obj);
  //       }
  //       this.users = this.usersgrids;
  //     }, error => this.logError(error)
  //   );
  //
  //   this.cols = [
  //     {field: 'firstName' , header: 'FirstName'},
  //     {field: 'lastName' , header: 'LastName'},
  //     {field: 'userName' , header: 'UserName'},
  //     {field: 'email', header: 'Email'},
  //     {field: 'createdDate', header: 'CreatedDate'},
  //     {field: 'createdBy', header: 'CreatedBy'},
  //     {field: 'modifiedDate', header: 'ModifiedDate'},
  //     {field: 'lat', header: 'Latitude'},
  //     {field: 'long', header: 'Longitude'},
  //     {field: 'age', header: 'age'},
  //     {field: 'usertype_name', header: 'User Type'},
  //     {field: 'community_name', header: 'Community'}
  //
  //   ];
  // }

  routeTo(pageroute: string)  {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.userService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.usersgrids.length);
    this.usersgrids.splice(0, this.usersgrids.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All User Succesfully'});
  }

  edit(user: User) {
    localStorage.setItem('editUser', JSON.stringify(user));
    this.routeTo('/admin/user/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + user.email});
  }

  delete(user: User) {
    this.msgs = [];
    this.userService.deleteBy(user.email.toLowerCase())
      .subscribe(user => { console.log(user); },
        error => {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Delete'});
        });

    let indexToDelete: number = 0;
    indexToDelete = this.usersgrids.findIndex((item) => item.email.toLowerCase() === user.email.toLowerCase());
    this.usersgrids.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + user.email});
  }

}
