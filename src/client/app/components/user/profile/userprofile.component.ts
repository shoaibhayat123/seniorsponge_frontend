// core libs
import { Component, OnInit } from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../../modules/core/index';

// interfaces
import { User } from '../../../interfaces/user';
import { UserType } from '../../../interfaces/usertype';
import { Community } from '../../../interfaces/community';

// services
import { UserService } from '../../../services/user.service';
import { UserTypeService } from '../../../services/usertype.service';
import { CommunityService } from '../../../services/community.service';

// UI
import {Message, SelectItem } from 'primeng/primeng';

// password hash
import { Endcrypt } from 'endcrypt/endcrypt';

@Component({
  moduleId: module.id,
  selector: 'ss-userprofile',
  templateUrl: 'userprofile.component.html',
  styleUrls: ['userprofile.component.css'],
  providers: [UserService, UserTypeService, CommunityService]
})

export class UserProfileComponent implements OnInit {

  title; isedited: boolean;

  usertypes: UserType[] = [];
  communities : Community[] = [];
  _communityid: SelectItem[];
  _usertypeid: SelectItem[] = [];
  msgs: Message[] = [];

  btnedit: boolean;
  btnsubmit: boolean;
  btndashboard: boolean;

  selectedCommunityId: string;

  firstName; lastName;createdDate; createdBy ;modifiedDate; lat; long; isActive;
  isDelete; userName; password; repassword; email; age; selectedUserType: string;

  user: User;  loginuser: User;

  constructor(private userService: UserService
    ,private userTypeService: UserTypeService
    ,private communityService: CommunityService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.title = 'User Page';

      this.btndashboard = true;
      this.btnedit = false;

      this.isedited = true;
      this.user = JSON.parse(localStorage.getItem('currentUser'));

      this.btnedit = true;
      this.btnsubmit = false;

      if (this.user) {
        this.email = this.user.email;
        this.firstName = this.user.firstName;
        this.lastName = this.user.lastName;
        this.userName = this.user.userName;
        this.password = this.decryptPassword(JSON.parse(this.user.password));
        this.repassword = this.decryptPassword(JSON.parse(this.user.password));
        this.age = this.user.age;
        this.lat = this.user.lat;
        this.long = this.user.long;
        this.createdDate = this.user.createdDate;
        this.createdBy = this.user.createdBy;
        this.modifiedDate = this.user.modifiedDate;
        this.isActive = this.user.isActive;
        this.isDelete = this.user.isDelete;
      }

      // UserType Dropdown

      this.userTypeService.getAll().subscribe(usertypes => {
          this.usertypes = usertypes;
          this._usertypeid.push({label: '', value: ''});
          for (let i = 0; i < this.usertypes.length; i++) {
            console.log('user types', this.usertypes);
            if (!this.usertypes[i]['isDelete']) {
              this._usertypeid.push({label: this.usertypes[i]['name'], value: this.usertypes[i]['id']});
            }
          }
          if (this.user) {
            this.user.id_userType = this.user.id_userType === undefined ? ''
              : this.user.id_userType;
            this.selectedUserType = this.user.id_userType === '' ? this._usertypeid.length > 0 ?
              String(this._usertypeid[0].value) : '' : this.user.id_userType;
          }
        }
        , error => this.logError(error)
      );

      this._communityid = [];

      // Community Dropdown
      this.communityService.getAll().subscribe(communities => {
          this.communities = communities;
          this._communityid.push({label: '', value: ''});
          for (let j = 0; j < this.communities.length; j++) {
            console.log('communities ', this.communities);
            if (!this.communities[j]['isDelete']) {
              this._communityid.push({label: this.communities[j]['name'], value: this.communities[j]['id']});
            }
          }
          if (this.user) {
            this.user.community_id = this.user.community_id === undefined ? ''
              : this.user.community_id;
            this.selectedCommunityId = this.user.community_id === '' ? this._communityid.length > 0 ?
              String(this._communityid[0].value) : '' : this.user.community_id;
          }
        }
        , error => this.logError(error)
      );

    } else {
      this.routeTo('/login');
    }
  }

  encryptPassword(userPassword): string {
    return new Endcrypt().encryptWithKey(userPassword, '16bytesecretkeys');
  }

  decryptPassword(encrypt): string {
    return new Endcrypt().decryptWithKey(encrypt, '16bytesecretkeys');
  }

  logError(err: any) {
    console.log('error' , err);
  }

  editUser() {
    if (localStorage.getItem('currentUser')) {
      if (this.selectedUserType && this.selectedCommunityId) {
        if (this.password === this.repassword) {
          if (this.user) {
            // this.user.email = this.email !== undefined ? this.email.toLowerCase().trim()
            //   : this.email;
            this.user.firstName = this.firstName;
            this.user.lastName = this.lastName;
            this.user.userName = this.userName !== undefined ? this.userName.toLowerCase().trim()
              : this.userName;
            this.user.password = JSON.stringify(this.encryptPassword(this.password));
            this.user.age = this.age;
            this.user.lat = this.lat;
            this.user.long = this.long;
            this.user.createdDate = this.createdDate;
            this.user.createdBy = this.createdBy;
            this.user.modifiedDate = new Date().toDateString();
            this.user.isActive = this.isActive;
            this.user.isDelete = this.isDelete;
            this.user.userTypeID = this.selectedUserType;
            this.user.id_userType = this.selectedUserType;
            this.user.communityID = this.selectedCommunityId;
            this.user.community_id = this.selectedCommunityId;

            this.userService.updateUser(this.user.email, this.user)
              .subscribe(user => {
                  console.log(user);
                  localStorage.removeItem('currentUser');
                  localStorage.setItem('currentUser', JSON.stringify(this.user));
                  this.routeTo('/dashboard');
                },
                error => {
                  this.msgs = [];
                  this.msgs.push({severity: 'error', summary: 'Erro Message', detail: 'Cannot Be Update'});
                });
          }

        } else {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Password Not Matching'});
        }
      } else {
        this.msgs = [];
        this.msgs.push({
          severity: 'error', summary: 'Missing Selection', detail: 'Select User Type OR Community Type'
        });
      }
    } else {
      this.msgs = [];
      this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'User Not Found'});
    }
  }

  cleraAllfield() {
    this.firstName = this.password = this.lastName = this.createdBy = this.lat
      = this.long = this.userName = this.email = this.age = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
