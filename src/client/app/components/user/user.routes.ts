import { UserGridComponent } from './userGrid.component';
import { UserSubmitComponent } from './userSubmit.component';
import { UserProfileComponent } from '././profile/userprofile.component';

export const UserRoutes: Array<any> = [
  {
    path: 'admin/user',
    component: UserGridComponent
  },
  {
    path: 'admin/user/submit',
    component: UserSubmitComponent
  },
  {
    path: 'user/profile',
    component: UserProfileComponent
  }
];
