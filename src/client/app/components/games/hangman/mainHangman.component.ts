import {Component, Pipe, PipeTransform} from '@angular/core';
import {HangmanGame, GuessFeedback} from './hangman.component';
import {GuessComponent} from './guess.component';
import {ApiService} from './service/hangman.service';

@Component({
  selector: 'my-hangmanapp',
  templateUrl: 'app/components/games/hangman/mainHangman.component.html',
  providers: [ApiService]
})
export class HangmanAppComponent {
  title: string = 'Hangman App';
  game: HangmanGame;
  feedback: GuessFeedback;

  constructor(private api: ApiService) {
    this.game = new HangmanGame('');
    this.api.getWord()
      .subscribe(data => {
        this.game = new HangmanGame(data.word);
      });
    this.api.newWord();
  }

  newGame() {
    //window.location.reload();
    this.api.newWord();
  }

  makeGuess(guess: string) {
    let feedback = this.game.makeGuess(guess);
    this.feedback = feedback;
  }

}
