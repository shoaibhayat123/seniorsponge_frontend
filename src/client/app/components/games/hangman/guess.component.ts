import {Component, Output, Input, EventEmitter} from '@angular/core';
import {GuessFeedback} from './hangman.component';

@Component({
  selector: 'guess-component',
  templateUrl: 'app/components/games/hangman/guess.component.html'
})
export class GuessComponent {
  guess: string = 'a';

  @Input() title: string;

  @Output() keyup : EventEmitter<any> = new EventEmitter();
  @Output() guessMade: EventEmitter<string> = new EventEmitter();

  sendKeyUp() {
    this.keyup.emit(null);
  }

  makeGuess() {
    this.guessMade.emit(this.guess);
    this.guess = "";
  }
}
