import { HangmanAppComponent } from './hangman/mainHangman.component';

export const GamesRoutes: Array<any> = [
  {
    path: 'hangman',
    component: HangmanAppComponent
  }
];
