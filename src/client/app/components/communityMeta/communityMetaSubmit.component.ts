// core libs
import { Component, OnInit } from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { CommunityMeta } from '../../interfaces/communityMeta';
import { Community } from '../../interfaces/community';
import { User } from '../../interfaces/user';

// services
import { CommunityMetaService } from '../../services/communityMeta.service';
import { CommunityService } from '../../services/community.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-submitcommunitymeta',
  templateUrl: 'communityMetaSubmit.component.html',
  providers: [CommunityMetaService, CommunityService, UserService]
})

export class CommunityMetaSubmitComponent implements OnInit {

  title;

  msgs: Message[] = [];

  _communityid: SelectItem[];
  communities : Community[] = [];

  selectedCommunityId: string;

  btnedit: boolean;
  btnsubmit: boolean;

  name; loginuser: User;

  communityMeta: CommunityMeta;

  constructor(private communityMetaService: CommunityMetaService
    ,private communityService: CommunityService
    ,private userService: UserService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  logError(err: any) {
    console.log('error' , err);
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.title = 'Community Meta Submit Page';

      if (!localStorage.getItem('isAdmin')) {
        this.routeTo('/admin/login');
      }

      this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

      this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
        console.log('finddata', finddata);
        if (finddata) {
          if (!finddata.isDelete) {
            let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
              : '';
            if (isAdmin.toLowerCase().trim() !== 'admin') {
              localStorage.removeItem('isAdmin');
              this.routeTo('/admin/login');
            }
          }
        }
      });

      this.btnsubmit = true;
      this.btnedit = false;

      if (localStorage.getItem('editCommunityMeta')) {
        console.log('edit Initialize');
        this.communityMeta = JSON.parse(localStorage.getItem('editCommunityMeta'));
        if (this.communityMeta) {
          this.btnedit = true;
          this.btnsubmit = false;

          this.name = this.communityMeta.name;
        }
      }

      this._communityid = [];

      // Community Dropdown
      this.communityService.getAll().subscribe(communities => {
          this.communities = communities;
          this._communityid.push({label: '', value: ''});
          for (let j = 0; j < this.communities.length; j++) {
            console.log('communities ', this.communities);
            if (!this.communities[j]['isDelete']) {
              this._communityid.push({label: this.communities[j]['name'], value: this.communities[j]['id']});
            }
          }
          if (this.communityMeta) {
            this.communityMeta.community_id = this.communityMeta.community_id === undefined ? ''
              : this.communityMeta.community_id;
            this.selectedCommunityId = this.communityMeta.community_id === '' ? this._communityid.length > 0 ?
              String(this._communityid[0].value) : '' : this.communityMeta.community_id;
          }
        }
        , error => this.logError(error)
      );
    } else {
      this.routeTo('/login');
    }
  }

  submit() {

    if(this.selectedCommunityId) {

      this.communityMetaService.insert(
        this.name.toLowerCase().trim(),
        this.selectedCommunityId).subscribe(data => {
          console.log(data);
          if (data) {
            this.title = 'Submit Success';
            this.routeToGrid();
          }},
        err => {
          this.msgs = [];
          this.msgs.push({
            severity: 'error', summary: 'Error Message', detail: 'Community Meta Already exist' +
            ' OR Bad Request ' + err
          });
        });
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Missing Selection', detail: 'Select User Email OR' +
        ' Community Name OR Days'
      });
    }
  }

  editCommunityMeta() {
    if (localStorage.getItem('editCommunityMeta')) {
      console.log('edit Update');
      if (this.communityMeta) {
        if (this.selectedCommunityId) {
          this.communityMeta.name = this.name !== undefined ? this.name.toLowerCase().trim()
            : this.name;
          this.communityMeta.community_id = this.selectedCommunityId;

          this.communityMetaService.updateBy(this.communityMeta.id, this.communityMeta)
            .subscribe(data => {
                console.log(data);
              },
              err => {
                this.msgs = [];
                this.msgs.push({
                  severity: 'error', summary: 'Error Message', detail: 'Cannot Update Something Invalid'
                });
              });

          this.routeToGrid();
        }
      }
    }
  }

  routeToGrid() {
    this.routeTo('/admin/community/meta');
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

}
