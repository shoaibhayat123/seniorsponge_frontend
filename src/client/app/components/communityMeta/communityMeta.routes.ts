import { CommunityMetaGridComponent } from './communityMetaGrid.component';
import { CommunityMetaSubmitComponent } from './communityMetaSubmit.component';

export const CommunityMetaRoutes: Array<any> = [
  {
    path: 'admin/community/meta',
    component: CommunityMetaGridComponent
  },
  {
    path: 'admin/community/meta/submit',
    component: CommunityMetaSubmitComponent
  }
];
