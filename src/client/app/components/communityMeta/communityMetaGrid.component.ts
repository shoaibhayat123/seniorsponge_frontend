// core
import { Component, OnInit} from '@angular/core';

// services
import { CommunityMetaService } from '../../services/communityMeta.service';
import { UserService } from '../../services/user.service';

// UI
import {MenuItem, Message } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

// interfaces
import { CommunityMeta } from '../../interfaces/communityMeta';
import { User } from '../../interfaces/user';

// routes
import { RouterExtensions, Config } from '../../modules/core/index';

@Component({
  moduleId: module.id,
  selector: 'sd-communitymetagrid',
  templateUrl: 'communityMetaGrid.component.html',
  providers: [CommunityMetaService, UserService]
})

export class CommunityMetaGridComponent implements OnInit {

  msgs: Message[] = [];

  items: MenuItem[];

  title;

  cols: any[];

  communityMetagrids : any[] = [];

  communitiesMeta: CommunityMeta[]; loginuser: User;

  constructor(private communityMetaService: CommunityMetaService,
              private userService: UserService,
              public routerext: RouterExtensions) {
    this.communityMetaService.getAll().subscribe(communitiesMeta => { this.communitiesMeta = communitiesMeta;
        console.log('grid communitiesMeta', this.communitiesMeta);

        for(let j = 0; j < this.communitiesMeta.length ; j++) {
          let obj = {
            id: this.communitiesMeta[j]['id'],
            name : this.communitiesMeta[j]['name'],
            community_id : this.communitiesMeta[j]['community_id'].length > 0 ? this.communitiesMeta[j]['community_id'][0]['id'] : '',
            community_name: this.communitiesMeta[j]['community_id'].length > 0 ? this.communitiesMeta[j]['community_id'][0]['name'] :''
          };
          this.communityMetagrids.push(obj);
        }
      }, error => this.logError(error)
    );

    this.cols = [
      {field: 'name' , header: 'Name'},
      {field: 'community_name', header: 'Community Name'}
    ];
  }

  ngOnInit() {

    localStorage.removeItem('editCommunityMeta');

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    }

    if (!localStorage.getItem('isAdmin')) {
      this.routeTo('/admin/login');
    }

    this.loginuser = JSON.parse(localStorage.getItem('isAdmin'));

    this.userService.getBy(this.loginuser.email.toLowerCase()).subscribe(finddata => {
      console.log('finddata', finddata);
      if (finddata) {
        if (!finddata.isDelete) {
          let isAdmin = finddata.id_userType.length > 0 ? finddata.id_userType[0]['name']
            : '';
          if (isAdmin.toLowerCase().trim() !== 'admin') {
            localStorage.removeItem('isAdmin');
            this.routeTo('/admin/login');
          }
        }
      }
    });

    this.title = 'Community Meta Grid';

    this.items = [
      {
        label: 'Add Community Meta', icon: 'fa-user-plus', command: () => {
        this.routeTo('/admin/community/meta/submit');
      }
      },
      {
        label: 'Delete All Community Meta', icon: 'fa-trash', command: () => {
        this.deleteAll();
      }
      }
    ];
  }

  // ngAfterViewInit() {
  //
    // this.communityMetaService.getAll().subscribe(communitiesMeta => { this.communitiesMeta = communitiesMeta;
    //     console.log('grid communitiesMeta', this.communitiesMeta);
    //
    //     for(let j = 0; j < this.communitiesMeta.length ; j++) {
    //       let obj = {
    //         id: this.communitiesMeta[j]['id'],
    //         name : this.communitiesMeta[j]['name'],
    //         community_id : this.communitiesMeta[j]['community_id'].length > 0 ? this.communitiesMeta[j]['community_id'][0]['id'] : '',
    //         community_name: this.communitiesMeta[j]['community_id'].length > 0 ? this.communitiesMeta[j]['community_id'][0]['name'] :''
    //       };
    //       this.communityMetagrids.push(obj);
    //     }
    //   }, error => this.logError(error)
    // );
    //
    // this.cols = [
    //   {field: 'name' , header: 'Name'},
    //   {field: 'community_name', header: 'Community Name'}
    // ];
  // }

  routeTo(pageroute: string)  {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error' , err);
  }

  deleteAll() {
    this.communityMetaService.deleteAll().subscribe(status => console.log(status));
    this.cols.forEach(c => {  if (c.filterInput) c.filterInput.value = '';  });
    console.log(this.communityMetagrids.length);
    this.communityMetagrids.splice(0, this.communityMetagrids.length);
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Delete All Community Meta Succesfully'});
  }

  edit(communityMeta: CommunityMeta) {
    localStorage.setItem('editCommunityMeta', JSON.stringify(communityMeta));
    this.routeTo('/admin/community/meta/submit');
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Updated ' + communityMeta.name});
  }

  delete(communityMeta: CommunityMeta) {
    this.msgs = [];
    this.communityMetaService.deleteBy(communityMeta.id)
      .subscribe(communityMeta => console.log(communityMeta));

    let indexToDelete: number = 0;
    indexToDelete = this.communityMetagrids.findIndex((item) => item.id === communityMeta.id);
    this.communityMetagrids.splice(indexToDelete, 1);

    this.msgs.push({severity:'info', summary:'Success',
      detail:'Data Deleted ' + communityMeta.id});
  }

}
