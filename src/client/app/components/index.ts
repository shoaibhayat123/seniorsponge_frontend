import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CommunityGridComponent } from './community/communityGrid.component';
import { AddEditCommunityComponent } from './community/addEditCommunity.component';
import { UserTypeSubmitComponent } from './usertype/usertypeSubmit.component';
import { UserTypeGridComponent } from './usertype/usertypeGrid.component';
import { UserGridComponent } from './user/userGrid.component';
import { UserSubmitComponent } from './user/userSubmit.component';
import { ClassesGridComponent } from './classes/classesGrid.component';
import { ClassesSubmitComponent } from './classes/classesSubmit.component';
import { FileUploadComponent } from './fileupload/fileupload.component';
import { CommunityTypeGridComponent } from './communityType/communityTypeGrid.component';
import { CommunityTypeSubmitComponent } from './communityType/communityTypeSubmit.component';
import { CommunityMetaGridComponent } from './communityMeta/communityMetaGrid.component';
import { CommunityMetaSubmitComponent } from './communityMeta/communityMetaSubmit.component';
import { CommunityLocationComponent } from './communityLocation/communityLocation.component';
import { CommunityLocationSubmitComponent } from './communityLocation/communityLocationSubmit.component';
import { EventGridComponent } from './event/eventGrid.component';
import { EventSubmitComponent } from './event/eventSubmit.component';
import { AdminDashboardComponent } from './admin/admindashboard.component';
import { AdminLoginComponent } from './admin/adminlogin.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ImagesComponent } from './gallery/images/images.component';
import { AlbumComponent } from './gallery/images/album.component';
import { VideosComponent } from './gallery/videos/videos.component';
import { DocumentsComponent } from './gallery/documents/documents.component';
import { AudiosComponent } from './gallery/audios/audios.component';
import { UserProfileComponent } from './user/profile/userprofile.component';
import { AlbumsComponent } from './fileupload/album/albums.component';
import { AudioComponent } from './fileupload/audio/audio.component';
import { VideoComponent } from './fileupload/video/video.component';
import { EventComponent } from './event/event.component';
import { TasksComponent } from './todo/task/task.component';
import { RSSFeedComponent } from './rssFeed/rssFeed.component';
import { EmailGridComponent } from './email/emailGrid.component';
import { GuessComponent } from './games/hangman/guess.component';
import { HangmanAppComponent } from './games/hangman/mainHangman.component';

export const APP_COMPONENTS: any[] = [
  AppComponent,
  AboutComponent,
  HomeComponent,
  LoginComponent,
  SignUpComponent,
  DashboardComponent,
  CommunityGridComponent,
  AddEditCommunityComponent,
  UserTypeSubmitComponent,
  UserTypeGridComponent,
  UserGridComponent,
  UserSubmitComponent,
  ClassesGridComponent,
  ClassesSubmitComponent,
  FileUploadComponent,
  CommunityTypeGridComponent,
  CommunityTypeSubmitComponent,
  CommunityMetaGridComponent,
  CommunityMetaSubmitComponent,
  CommunityLocationComponent,
  CommunityLocationSubmitComponent,
  EventGridComponent,
  EventSubmitComponent,
  AdminDashboardComponent,
  AdminLoginComponent,
  GalleryComponent,
  ImagesComponent,
  AlbumComponent,
  VideosComponent,
  DocumentsComponent,
  AudiosComponent,
  UserProfileComponent,
  AlbumsComponent,
  AudioComponent,
  VideoComponent,
  TasksComponent,
  EventComponent,
  RSSFeedComponent,
  EmailGridComponent,
  GuessComponent,
  HangmanAppComponent
];

export * from './app.component';
export * from './about/about.component';
export * from './home/home.component';
export * from './login/login.component';
export * from './signup/signup.component';
export * from './dashboard/dashboard.component';
export * from './community/communityGrid.component';
export * from './community/addEditCommunity.component';
export * from './usertype/usertypeSubmit.component';
export * from './usertype/usertypeGrid.component';
export * from './user/userGrid.component';
export * from './user/userSubmit.component';
export * from './classes/classesGrid.component';
export * from './classes/classesSubmit.component';
export * from './fileupload/fileupload.component';
export * from './communityType/communityTypeGrid.component';
export * from './communityType/communityTypeSubmit.component';
export * from './communityMeta/communityMetaGrid.component';
export * from './communityMeta/communityMetaSubmit.component';
export * from './communityLocation/communityLocation.component';
export * from './communityLocation/communityLocationSubmit.component';
export * from './event/eventGrid.component';
export * from './event/eventSubmit.component';
export * from './admin/admindashboard.component';
export * from './admin/adminlogin.component';
export * from './gallery/gallery.component';
export * from './gallery/images/images.component';
export * from './gallery/images/album.component';
export * from './gallery/videos/videos.component';
export * from './gallery/documents/documents.component';
export * from './gallery/audios/audios.component';
export * from './user/profile/userprofile.component';
export * from './fileupload/album/albums.component';
export * from './fileupload/audio/audio.component';
export * from './fileupload/video/video.component';
export * from './todo/task/task.component';
export * from './event/event.component';
export * from './rssFeed/rssFeed.component';
export * from './email/emailGrid.component';
export * from './games/hangman/guess.component';
export * from './games/hangman/mainHangman.component';
