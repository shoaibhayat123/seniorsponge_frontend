// core
import { Component, OnInit} from '@angular/core';

// interfaces
import { FileUpload } from '../../../interfaces/fileUpload';
import { User} from '../../../interfaces/user';

// UI
import {MenuItem, Message } from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';

// routes
import { RouterExtensions, Config } from '../../../modules/core/index';

import { Http, Headers, Response } from '@angular/http';


@Component({
  moduleId: module.id,
  selector: 'sd-audio',
  templateUrl: 'audio.component.html'
})

export class AudioComponent implements OnInit {

  // types: SelectItem[];
  //
  // selectedFileType: string;

  audio: any[] = ['mp3', 'aiff', 'wma'];

  msgs: Message[] = [];

  items: MenuItem[];

  title;
  user: User;

  cols: any[];

  load: any[];
  filesubmit: boolean;

  avatar;
  files: FileUpload[];

  // filesToUpload: Array<File> = [];

  fileUploads: FileUpload[];

  constructor(private http: Http,
              public routerext: RouterExtensions) {
  }

  ngOnInit() {

    if (!localStorage.getItem('currentUser')) {
      this.routeTo('/login');
    } else {

      this.user = JSON.parse(localStorage.getItem('currentUser'));
      if (this.user) {

        this.http.post('http://localhost:3000/file', JSON.stringify({
          id_user: this.user.id
          , type: 'audio'
        }))
          .map(response => response.json())
          .subscribe(files => {
              this.fileUploads = files;
              console.log(this.files);
            }, error => this.logError(error)
          );

        this.cols = [
          {field: 'name', header: 'Name'},
          {field: 'path', header: 'Path'},
          {field: 'type', header: 'Type'},
          {field: 'ext', header: 'Extension'},
          {field: 'status', header: 'Status'},
          {field: 'size', header: 'Size'}
        ];

        this.items = [
          {
            label: 'Upload Audio', icon: 'fa-trash', command: () => {
            this.submit();
          }
          }
        ];

        this.title = 'Audio Upload Grid';
      }
    }
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  logError(err: any) {
    console.log('error', err);
  }

  dashboard() {
    this.routeTo('/dashboard');
  }


  fileChanged(e: Event) {
    var target: HTMLInputElement = e.target as HTMLInputElement;
    this.load = [];
    this.filesubmit = true;

    // if (this.selectedFileType) {
    for (var i = 0; i < target.files.length; i++) {
      let ext = target.files[i]['name'].substr((target.files[i]['name']).lastIndexOf('.') + 1);
      // if (this.selectedFileType === 'audio') {
      this.audio.find(item => item === ext.toLowerCase().trim()) !== undefined ? this.loadFile(target.files[i])
        : this.error();
      // }
    }
    // } else {
    //   this.filesubmit = false;
    //   this.msgs = [];
    //   this.msgs.push({severity:'error', summary:'Error Message',
    //     detail:'Please Chose file'});
    // }
  }

  error() {
    this.filesubmit = false;
    this.msgs = [];
    this.msgs.push({
      severity: 'error', summary: 'Error Message',
      detail: 'Please Chose file'
    });
  }

  loadFile(img: File) {
    var formData: FormData = new FormData();
    formData.append('avatar', img, img.name);
    this.http.post('http://localhost:3000/file/upload', formData)
      .subscribe(response => this.load.push(response.json()));

    this.msgs = [];
    this.msgs.push({
      severity: 'info', summary: 'Info Message',
      detail: 'Click Submit If you want to Chose file Again'
    });
  }

  async submit() {
    this.filesubmit = false;
    console.log('this.load', this.load);
    if (this.load) {
      if (this.load.length > 0) {
        for (let i = 0; i < this.load.length; i++) {

          var types = this.load[i].files[i]['type'];
          var result = types.split('/');
          var ext = result.pop();               //Removes last value and grap the last value
          var type = result.join('/');        //Grabs the previous part

          await this.http.post('http://localhost:3000/file/add', JSON.stringify(
            {
              name: (this.load[i].files[i]['fd']).substr((this.load[i].files[i]['fd']).lastIndexOf('/') + 1),
              path: this.load[i].files[i]['fd'],
              type: type,
              ext: ext,
              status: this.load[i].files[i]['status'],
              size: this.load[i].files[i]['size'],
              id_user: this.user.id,
              userId: this.user.id
            })).subscribe((res) => {
              console.log(res.json());
            },
            (err) => {
              console.log(err.json());
            });
        }
      } else {
        this.msgs = [];
        this.msgs.push({
          severity: 'error', summary: 'Error Message',
          detail: 'Please Chose Valid file'
        });
      }
    } else {
      this.msgs = [];
      this.msgs.push({
        severity: 'error', summary: 'Error Message',
        detail: 'Please Chose file'
      });
    }
  }
}
