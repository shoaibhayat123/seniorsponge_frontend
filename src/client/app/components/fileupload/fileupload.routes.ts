import { FileUploadComponent } from './fileupload.component';
import { AlbumsComponent } from '././album/albums.component';
import { AudioComponent } from '././audio/audio.component';
import { VideoComponent } from '././video/video.component';

export const FileUploadRoutes: Array<any> = [
  {
    path: 'admin/upload',
    component: FileUploadComponent
  },
  {
    path: 'admin/album',
    component: AlbumsComponent
  },
  {
    path: 'admin/audio',
    component: AudioComponent
  },
  {
    path: 'admin/video',
    component: VideoComponent
  }
];
