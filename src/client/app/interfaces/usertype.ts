export interface UserType {
  id: string;
  name: string;
  isActive: boolean;
  isDelete: boolean;
}
