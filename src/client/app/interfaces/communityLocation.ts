
export interface CommunityLocation {
  id: string;
  name: string;
  address: string;
  lat: string;
  long: string;
  isActive: boolean;
  isDelete: boolean;
  id_community: string;
}
