
export interface CommunityType {
  id: string;
  id_userType: string;
  id_community: string;
  id_user: string;
  isActive: boolean;
}
