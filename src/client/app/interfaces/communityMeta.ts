
export interface CommunityMeta {
  id: string;
  name: string;
  community_id: string;
}
