export interface Community {
  id: string;
  name: string;
  address: string;
  lat: string;
  long: string;
  isActive:boolean;
  isDelete: boolean;
  profileName: string;
}
