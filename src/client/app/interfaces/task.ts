// import { firebase } from '../../firebase';


export interface ITask {
  id: string;
  complete: boolean;
  createdAt: string;
  title: string;
  id_user: string;
  user_id: string;
}


export class Task implements ITask {
  complete = false;
  createdAt;
  id_user;
  user_id;
  title;
  id;

  constructor(title: string, id_user: string) {
    this.title = title;
    this.id_user = id_user;
    this.user_id = id_user;
  }
}
