
export interface Classes {
  id: string;
  name: string;
  dateTime: string;
  startDate: string;
  endDate: string;
  days: any[];
  id_user: string;
  isPublic: boolean;
  community_id: string;
}
