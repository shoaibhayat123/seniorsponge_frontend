
export interface FileUpload {
  id: string;
  name: string;
  path: string;
  type: string;
  ext: string;
  status: string;
  size: string;
  id_user: string;
  userId: string;
}
