export interface User {
  id: string;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  age: string;
  createdDate: string;
  createdBy: string;
  modifiedDate: string;
  lat: string;
  long: string;
  isActive:boolean;
  isDelete:boolean;
  userTypeID: string;
  id_userType: string;
  communityID: string;
  community_id: string;
}

export interface Album {
  id: string;
  albumName: string;
  albumThumbnail: string;
  createdDate: string;
  createdBy: string;
  modifiedDate: string;
  isActive: boolean;
  isDelete: boolean;
}

export interface Image {
  id: string;
  albumId: string;
  Image: string;
  createdDate: string;
  createBy: string;
  modifiedDate: string;
  isActive: boolean;
  isDelete: boolean;
}
