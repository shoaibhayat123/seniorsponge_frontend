
export interface Event {
  id: string;
  name: string;
  startDate: string;
  endDate: string;
  id_user: string;
  days: any[];
  community_id: string;
}
