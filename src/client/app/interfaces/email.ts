
export interface IEmail {
  email: string;
  password: string;
  servicetype: string;
  mailtype: string;
  limit: string;
  seen: string;
  UID: string;
}

export interface IEmailBody {
  type : string;
  contentType : string;
  text: string;
}


export class Email implements IEmail {
  email;
  password;
  servicetype;
  mailtype;
  limit;
  seen;
  UID;

  constructor(email: string, password: string, servicetype: string,
              mailtype: string, limit: string, seen: string, UID: string) {
    this.email = email;
    this.password = password;
    this.servicetype = servicetype;
    this.mailtype = mailtype;
    this.limit = limit;
    this.seen = seen;
    this.UID = UID;
  }
}
