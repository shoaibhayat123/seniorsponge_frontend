// core libs
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class RSSFeedService {          //HackerNewsService
  constructor (
    private http: Http
  ) {}

  getChannelORNewsInfo(urlapi: string) {
    return this.http.get(urlapi).map((res:Response) => res.json());
  }
}
