import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
// interface
import { Classes } from '../interfaces/classes';
import { API_STARTPOINT} from '../components/global.constant';


@Injectable()
export class ClassesService {

  constructor(private http: Http) {}

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  insert( name: string, dateTime:string, startDate:string, endDate:string,
          id_user:string , days:any[], community_id: string, isPublic: boolean) {

    return this.http.post(API_STARTPOINT + '/classes', JSON.stringify(
      {name: name,
        dateTime:dateTime,
        startDate:startDate,
        endDate:endDate,
        id_user:id_user,
        days:days,
        community_id: community_id,
        isPublic: isPublic
      })).map((response) => response.json());
  }

  //----------------------- Read or Read All API-------------------------------//

  getAll() {
    return this.http.get(API_STARTPOINT + '/classes')
      .map(response => response.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(anyfield:string) {
    console.log('deleted',anyfield);
    return this.http.delete(API_STARTPOINT + '/classes/id/' + anyfield)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/classes')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(id: string, classes: Classes) {
    console.log('classes', JSON.stringify(classes));
    return this.http.put(API_STARTPOINT + '/classes/id/' + id, JSON.stringify(
      classes
    ))
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

}

