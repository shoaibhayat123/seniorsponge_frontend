// core libs
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';

// interfaces
import { User } from '../interfaces/user';
import { API_STARTPOINT} from '../components/global.constant';

// UI libs
import {MenuItem, Message } from 'primeng/primeng';

@Injectable()
export class UserService {
  msgs: Message[] = [];

  constructor(private http: Http) {}

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  signUp(firstName: string, lastName:string, createdDate:string,
         createdBy:string, modifiedDate:string, lat: string, long:string,
         isActive:boolean, isDelete:boolean, userName: string, password:string,
         email: string, age: string, id_usertype: string, community_id: string) {
    return this.http.post(API_STARTPOINT + '/user', JSON.stringify(
      {firstName: firstName,
        lastName:lastName,
        createdDate:createdDate,
        createdBy:createdBy,
        modifiedDate:modifiedDate,
        lat: lat,
        long:long,
        isActive:isActive,
        isDelete:isDelete,
        userName: userName,
        password:password,
        email: email,
        age: age,
        userTypeID: id_usertype,
        id_userType: id_usertype,
        communityID: community_id,
        community_id: community_id}
    )).map((res) => { res.json(); });
  }

  //----------------------- Read or Login API-------------------------------//

  logIn(email:string, password:string) {
    return this.http.post(API_STARTPOINT + '/user/singin', JSON.stringify({
      email:email,
      password:password
    })).map((res) => res.json());
  }

  //----------------------- Edit or Update API-------------------------------//

  updateUser(email:string, user:User) {
    console.log('user', JSON.stringify(user));
    return this.http.put(API_STARTPOINT + '/user/email/' + email, JSON.stringify(user
    )).map((res) => res.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(email:string) {
    console.log('deleted',email);
    return this.http.delete(API_STARTPOINT + '/user/email/' + email)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/user')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Read or Read All API-------------------------------//

  getUsers() {
    return this.http.get(API_STARTPOINT + '/user')
      .map(response => response.json());
  }

  getBy(email:string) {
    return this.http.get(API_STARTPOINT + '/user/email/' + email)
      .map(response => response.json());
  }

  getById(id:string) {
    return this.http.get(API_STARTPOINT + '/user/id/' + id)
      .map(response => response.json());
  }

  getByUserType(userTypeID:string) {
    return this.http.get(API_STARTPOINT + '/users/userTypeID/' + userTypeID)
      .map(response => response.json());
  }
}
