import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';


import { Event } from '../interfaces/event';
import { User } from '../interfaces/user';
import { Community } from '../interfaces/community';
import { API_STARTPOINT} from '../components/global.constant';


@Injectable()
export class EventService {

  constructor(private http: Http) { }

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  insert( name: string, startDate:string, endDate:string,
          id_user:string , days:any[], community_id: string) {

    return this.http.post(API_STARTPOINT + '/events', JSON.stringify(
      {name: name,
        startDate:startDate,
        endDate:endDate,
        id_user:id_user,
        days:days,
        community_id: community_id
      })).map((response) => response.json());
  }

  //----------------------- Read or Read All API-------------------------------//

  getAll() {
    return this.http.get(API_STARTPOINT + '/events')
      .map(response => response.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(anyfield:string) {
    console.log('deleted',anyfield);
    return this.http.delete(API_STARTPOINT + '/events/id/' + anyfield)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/events')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(id: string, event: Event) {
    console.log('event', JSON.stringify(event));
    return this.http.put(API_STARTPOINT + '/events/id/' + id, JSON.stringify(
      event
    ))
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

}

