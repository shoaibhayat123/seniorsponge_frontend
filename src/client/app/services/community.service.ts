import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { Community } from '../interfaces/community';
import { API_STARTPOINT} from '../components/global.constant';


@Injectable()
export class CommunityService {

  constructor(private http: Http) { }

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  insert( name: string, address:string, lat:string, long:string,
          isActive: boolean, isDelete: boolean, profileName: string) {

    return this.http.post(API_STARTPOINT + '/community', JSON.stringify(
      {name: name,
        address: address,
        lat: lat,
        long: long,
        isActive: isActive,
        isDelete: isDelete,
        profileName: profileName
      })).map((response) => response.json());
  }

  //----------------------- Read or Read All API-------------------------------//

  getAll() {
    return this.http.get(API_STARTPOINT + '/community')
      .map(response => response.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(name:string) {
    console.log('deleted',name);
    return this.http.delete(API_STARTPOINT + '/community/name/' + name)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/community')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(name: string, community: Community) {
    console.log('community', JSON.stringify(community));
    return this.http.put(API_STARTPOINT + '/community/name/' + name, JSON.stringify(
      community
    ))
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

}

