import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { UserType } from '../interfaces/usertype';
import { API_STARTPOINT} from '../components/global.constant';


@Injectable()
export class UserTypeService {

  constructor(private http: Http) { }

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  insert( name: string, isActive: boolean, isDelete: boolean) {

    return this.http.post(API_STARTPOINT + '/usertype', JSON.stringify(
      {name: name,
        isActive:isActive,
        isDelete:isDelete
      })).map((response) => response.json());
  }

  //----------------------- Read or Read All API-------------------------------//

  getAll() {
    return this.http.get(API_STARTPOINT + '/usertype')
      .map(response => response.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(id:string) {
    console.log('deleted',id);
    return this.http.delete(API_STARTPOINT + '/usertype/id/' + id)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/usertype')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(anyfield: string, userType: UserType) {
    console.log('userType', JSON.stringify(userType));
    return this.http.put(API_STARTPOINT + '/usertype/name/' + anyfield, JSON.stringify(
      userType
    ))
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

}

