// core libs
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';

// interfaces
import {User, Album,Image} from '../interfaces/user';
import { API_STARTPOINT} from '../components/global.constant';

// UI libs
import {MenuItem, Message} from 'primeng/primeng';

@Injectable()
export class GalleryService {
  album: Album[];
  msgs: Message[] = [];

  constructor(private http: Http) {
  }

  //----------------------- Create or Insert API (Album)------------------------------//

  createAlbum(albumName: string, albumThumbnail: string, createdDate: string,
              createdBy: string, modifiedDate: string,
              isActive: boolean, isDelete: boolean) {
    console.log('thumb',albumThumbnail);
    return this.http.post(API_STARTPOINT + '/user/album', JSON.stringify(
      {
        albumName: albumName,
        albumThumbnail: albumThumbnail,
        createdDate: createdDate,
        createdBy: createdBy,
        modifiedDate: modifiedDate,
        isActive: isActive,
        isDelete: isDelete,
      }
    )).map((res) => {
      res.json();
    });
  }

  //----------------------- Edit or Update API-------------------------------//

  updateAlbum(id: string, album: Album[]) {
    console.log('user', JSON.stringify(album[0]));
    return this.http.put(API_STARTPOINT + '/user/album/' + id, JSON.stringify(album[0]
    )).map((res) => res.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(id: string) {
    console.log('deleted', id);
    return this.http.delete(API_STARTPOINT + '/user/album/' + id)
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/user/album')
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

  //----------------------- Read or Read All API-------------------------------//

  getAlbums() {
    return this.http.get(API_STARTPOINT + '/user/album')
      .map(response => response.json());
  }

  getBy(id: string) {
    return this.http.get(API_STARTPOINT + '/user/album/' + id)
      .map(response => response.json());
  }


  createImage (albumId: string, Image: string, createdDate: string,
               createdBy: string, modifiedDate: string,
               isActive: boolean, isDelete: boolean) {

    console.log('image',Image);
    return this.http.post(API_STARTPOINT + '/user/album/image/add', JSON.stringify(
      {
        albumId: albumId,
        Image: Image,
        createdDate: createdDate,
        createdBy: createdBy,
        modifiedDate: modifiedDate,
        isActive: isActive,
        isDelete: isDelete,
      }
    )).map((res) => {
      res.json();
    });

  }
  getImagesByAlbum(albumId:string) {
    return this.http.get(API_STARTPOINT + '/user/album/images/' + albumId)
      .map(response => response.json());
  }
}
