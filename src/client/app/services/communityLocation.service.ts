import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';


import { CommunityLocation } from '../interfaces/communityLocation';
import { API_STARTPOINT} from '../components/global.constant';


@Injectable()
export class CommunityLocationService {

  constructor(private http: Http) { }

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  insert( name: string, address:string, lat:string, long:string,
          isActive: boolean, isDelete: boolean, id_community: string) {

    return this.http.post(API_STARTPOINT + '/communitylocation', JSON.stringify(
      {name: name,
        address: address,
        lat: lat,
        long: long,
        isActive: isActive,
        isDelete: isDelete,
        id_community: id_community
      })).map((response) => response.json());
  }

  //----------------------- Read or Read All API-------------------------------//

  getAll() {
    return this.http.get(API_STARTPOINT + '/communitylocation')
      .map(response => response.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(anyfield:string) {
    console.log('deleted',anyfield);
    return this.http.delete(API_STARTPOINT + '/communitylocation/id/' + anyfield)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/communitylocation')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(id: string, communityLocation: CommunityLocation) {
    console.log('communityLocation', JSON.stringify(communityLocation));
    return this.http.put(API_STARTPOINT + '/communitylocation/id/' + id, JSON.stringify(
      communityLocation
    ))
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

}

