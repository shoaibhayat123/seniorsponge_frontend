import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { CommunityType } from '../interfaces/communityType';
import { API_STARTPOINT} from '../components/global.constant';


@Injectable()
export class CommunityTypeService {

  constructor(private http: Http) { }

  //----------------------- Create or Insert API ------------------------------//

  insert( id_userType: string, id_community: string,
          id_user: string, isActive: boolean) {

    return this.http.post(API_STARTPOINT + '/communitytype', JSON.stringify(
      {id_userType: id_userType,
        id_community:id_community,
        id_user:id_user,
        isActive:isActive
      })).map((response) => response.json());
  }

  //----------------------- Read or Read All API-------------------------------//

  getAll() {
    return this.http.get(API_STARTPOINT + '/communitytype')
      .map(response => response.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(anyfield:string) {
    console.log('deleted',anyfield);
    return this.http.delete(API_STARTPOINT + '/communitytype/id/' + anyfield)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/communitytype')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(id: string, communityType: CommunityType) {
    console.log('communityType', JSON.stringify(communityType));
    return this.http.put(API_STARTPOINT + '/communitytype/id/' + id, JSON.stringify(
      communityType
    ))
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

}

