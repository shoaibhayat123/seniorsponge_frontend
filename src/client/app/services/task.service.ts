
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ITask, Task } from '../interfaces/task';
import { API_STARTPOINT} from '../components/global.constant';

@Injectable()
export class TasksService {

  constructor(private http: Http) {}

  getAll() {
    return this.http.get(API_STARTPOINT + '/todo')
      .map(response => response.json());
  }

  getByUserId(user_id: string) {
    return this.http.get(API_STARTPOINT + '/todo/' + user_id)
      .map(response => response.json());
  }

  getByUserTaskComplete(user_id: string, complete: boolean) {
    return this.http.get(API_STARTPOINT + '/todo/' + user_id + '/' + complete)
      .map(response => response.json());
  }

  createTask(title: string , id_user: string) {
    console.log('user id', id_user);
    return this.http.post(API_STARTPOINT + '/todo', JSON.stringify(
      new Task(title, id_user)
    )).map((res) => { res.json(); });
  }

  removeTask(id: string) {
    console.log('task.id', JSON.stringify(id));
    return this.http.delete(API_STARTPOINT + '/todo/' + id)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  removeAll() {
    return this.http.delete(API_STARTPOINT + '/todo')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  updateTask(task: ITask) {
    console.log('task.id', JSON.stringify(task.id));
    return this.http.put(API_STARTPOINT + '/todo/' + task.id, JSON.stringify(task
    )).map((res) => res.json());
  }
}
