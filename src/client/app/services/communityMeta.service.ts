import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { CommunityMeta } from '../interfaces/communityMeta';
import { API_STARTPOINT} from '../components/global.constant';


@Injectable()
export class CommunityMetaService {

  constructor(private http: Http) { }

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  insert( name: string, community_id: string) {

    return this.http.post(API_STARTPOINT + '/communitymeta', JSON.stringify(
      {name: name,
        community_id: community_id
      })).map((response) => response.json());
  }

  //----------------------- Read or Read All API-------------------------------//

  getAll() {
    return this.http.get(API_STARTPOINT + '/communitymeta')
      .map(response => response.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(anyfield:string) {
    console.log('deleted',anyfield);
    return this.http.delete(API_STARTPOINT + '/communitymeta/id/' + anyfield)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  deleteAll() {
    return this.http.delete(API_STARTPOINT + '/communitymeta')
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(id: string, communityMeta: CommunityMeta) {
    console.log('communityMeta', JSON.stringify(communityMeta));
    return this.http.put(API_STARTPOINT + '/communitymeta/id/' + id, JSON.stringify(
      communityMeta
    ))
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

}

