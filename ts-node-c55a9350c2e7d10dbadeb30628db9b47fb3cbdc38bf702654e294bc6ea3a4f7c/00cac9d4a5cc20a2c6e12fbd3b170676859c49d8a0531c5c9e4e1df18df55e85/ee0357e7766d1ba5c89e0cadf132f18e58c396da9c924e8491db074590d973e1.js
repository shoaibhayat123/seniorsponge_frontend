"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var yargs_1 = require("yargs");
var seed_config_1 = require("./seed.config");
var path = require("path");
var SeedAdvancedConfig = (function (_super) {
    __extends(SeedAdvancedConfig, _super);
    function SeedAdvancedConfig() {
        var _this = _super.call(this) || this;
        _this.TNS_BASE_DIR = 'nativescript';
        _this.srcSubdir = 'src';
        _this.destSubdir = 'app';
        _this.TNS_APP_SRC = _this.TNS_BASE_DIR + "/" + _this.srcSubdir;
        _this.TNS_APP_DEST = _this.TNS_BASE_DIR + "/" + _this.destSubdir;
        _this.TNS_CONFIG = {
            ANALYTICS_TRACKING_ID: '',
        };
        _this.DESKTOP_PACKAGES = [];
        _this.ENABLE_SCSS = true;
        if (yargs_1.argv && yargs_1.argv._) {
            if (yargs_1.argv['desktop']) {
                _this.TARGET_DESKTOP = true;
                if (yargs_1.argv['desktopBuild']) {
                    _this.TARGET_DESKTOP_BUILD = true;
                }
            }
            else if (yargs_1.argv['hybrid']) {
                _this.TARGET_MOBILE_HYBRID = true;
            }
        }
        var bootstrap = 'main.web';
        if (_this.TARGET_MOBILE_HYBRID) {
            bootstrap = 'main.mobile.hybrid';
        }
        if (yargs_1.argv['analytics']) {
            _this.TNS_CONFIG.ANALYTICS_TRACKING_ID = yargs_1.argv['analytics'];
        }
        _this.BOOTSTRAP_DIR = yargs_1.argv['app'] ? (yargs_1.argv['app'] + '/') : '';
        _this.BOOTSTRAP_MODULE = "" + _this.BOOTSTRAP_DIR + bootstrap;
        _this.NG_FACTORY_FILE = bootstrap + ".prod";
        _this.BOOTSTRAP_PROD_MODULE = "" + _this.BOOTSTRAP_DIR + bootstrap;
        _this.BOOTSTRAP_FACTORY_PROD_MODULE = "" + _this.BOOTSTRAP_DIR + bootstrap + ".prod";
        _this.APP_TITLE = 'Angular Seed Advanced';
        _this.APP_BASE = _this.TARGET_DESKTOP ? ''
            : '/';
        var additionalPackages = [
            {
                name: 'lodash',
                path: 'node_modules/lodash/lodash.js',
                packageMeta: {
                    main: 'index.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: '@ngrx/core',
                packageMeta: {
                    main: 'bundles/core.umd.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: '@ngrx/store',
                packageMeta: {
                    main: 'bundles/store.umd.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: '@ngrx/effects',
                packageMeta: {
                    main: 'bundles/effects.umd.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: '@ngrx/effects/testing',
                path: 'node_modules/@ngrx/effects/testing/index.js'
            },
            {
                name: '@ngrx/store-devtools',
                packageMeta: {
                    main: 'bundles/store-devtools.umd.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: '@ngx-translate/core',
                packageMeta: {
                    main: 'bundles/core.umd.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: '@ngx-translate/http-loader',
                packageMeta: {
                    main: 'bundles/http-loader.umd.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: 'angulartics2',
                packageMeta: {
                    main: 'dist/core.umd.js',
                    defaultExtension: 'js'
                }
            },
            {
                name: 'ngrx-store-freeze',
                path: 'node_modules/ngrx-store-freeze/dist/index.js'
            },
            {
                name: 'deep-freeze-strict',
                path: 'node_modules/deep-freeze-strict/index.js'
            }
        ];
        _this.DESKTOP_PACKAGES = _this.DESKTOP_PACKAGES.concat(additionalPackages);
        _this.addPackagesBundles(additionalPackages);
        _this.PLUGIN_CONFIGS['gulp-sass'] = {
            includePaths: [
                './src/client/scss/',
                './node_modules/',
                './'
            ]
        };
        _this.PLUGIN_CONFIGS['gulp-sass-tns'] = {
            includePaths: [
                _this.srcSubdir,
                './node_modules/',
                './node_modules/nativescript-theme-core/scss/'
            ].map(function (dir) { return path.resolve(_this.TNS_BASE_DIR, dir); }),
        };
        _this.SYSTEM_CONFIG.paths[_this.BOOTSTRAP_MODULE] = "" + _this.APP_BASE + _this.BOOTSTRAP_MODULE;
        delete _this.SYSTEM_BUILDER_CONFIG['packageConfigPaths'];
        return _this;
    }
    return SeedAdvancedConfig;
}(seed_config_1.SeedConfig));
exports.SeedAdvancedConfig = SeedAdvancedConfig;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvcGMtMDkvRGVza3RvcC93d3cuc2VuaW9yc3BvbmdlLmNvbS11c2VyLWNvbW11bml0eS1iOTVmZjIzNTRkNWU3NTkzNGI2N2MzMThhMTY4ZDg4MDM1NTA3NGFjL3Rvb2xzL2NvbmZpZy9zZWVkLWFkdmFuY2VkLmNvbmZpZy50cyIsInNvdXJjZXMiOlsiL2hvbWUvcGMtMDkvRGVza3RvcC93d3cuc2VuaW9yc3BvbmdlLmNvbS11c2VyLWNvbW11bml0eS1iOTVmZjIzNTRkNWU3NTkzNGI2N2MzMThhMTY4ZDg4MDM1NTA3NGFjL3Rvb2xzL2NvbmZpZy9zZWVkLWFkdmFuY2VkLmNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSwrQkFBNkI7QUFDN0IsNkNBQTJDO0FBQzNDLDJCQUE2QjtBQUc3QjtJQUF3QyxzQ0FBVTtJQXVCaEQ7UUFBQSxZQUNFLGlCQUFPLFNBaUpSO1FBcEtELGtCQUFZLEdBQUcsY0FBYyxDQUFDO1FBRTlCLGVBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsZ0JBQVUsR0FBRyxLQUFLLENBQUM7UUFFbkIsaUJBQVcsR0FBTSxLQUFJLENBQUMsWUFBWSxTQUFJLEtBQUksQ0FBQyxTQUFXLENBQUM7UUFFdkQsa0JBQVksR0FBTSxLQUFJLENBQUMsWUFBWSxTQUFJLEtBQUksQ0FBQyxVQUFZLENBQUM7UUFFekQsZ0JBQVUsR0FBRztZQUNYLHFCQUFxQixFQUFFLEVBQUU7U0FDMUIsQ0FBQztRQUtGLHNCQUFnQixHQUFxQixFQUFFLENBQUM7UUFLdEMsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFFeEIsRUFBRSxDQUFDLENBQUMsWUFBSSxJQUFJLFlBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25CLEVBQUUsQ0FBQyxDQUFDLFlBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2dCQUMzQixFQUFFLENBQUMsQ0FBQyxZQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN6QixLQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2dCQUNuQyxDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxQixLQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1lBQ25DLENBQUM7UUFDSCxDQUFDO1FBQ0QsSUFBSSxTQUFTLEdBQUcsVUFBVSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFHOUIsU0FBUyxHQUFLLG9CQUFvQixDQUFDO1FBQ3JDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxZQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLEdBQUcsWUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzVELENBQUM7UUFHRCxLQUFJLENBQUMsYUFBYSxHQUFHLFlBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM1RCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBRyxLQUFJLENBQUMsYUFBYSxHQUFHLFNBQVcsQ0FBQztRQUM1RCxLQUFJLENBQUMsZUFBZSxHQUFNLFNBQVMsVUFBTyxDQUFDO1FBQzNDLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFHLEtBQUksQ0FBQyxhQUFhLEdBQUcsU0FBVyxDQUFDO1FBQ2pFLEtBQUksQ0FBQyw2QkFBNkIsR0FBRyxLQUFHLEtBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxVQUFPLENBQUM7UUFFOUUsS0FBSSxDQUFDLFNBQVMsR0FBRyx1QkFBdUIsQ0FBQztRQUN6QyxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztRQUdSLElBQUksa0JBQWtCLEdBQXFCO1lBQ3pDO2dCQUNFLElBQUksRUFBRSxRQUFRO2dCQUNkLElBQUksRUFBRSwrQkFBK0I7Z0JBQ3JDLFdBQVcsRUFBRTtvQkFDWCxJQUFJLEVBQUUsVUFBVTtvQkFDaEIsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixXQUFXLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLHFCQUFxQjtvQkFDM0IsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxhQUFhO2dCQUNuQixXQUFXLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLHNCQUFzQjtvQkFDNUIsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxlQUFlO2dCQUNyQixXQUFXLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLHdCQUF3QjtvQkFDOUIsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSx1QkFBdUI7Z0JBQzdCLElBQUksRUFBRSw2Q0FBNkM7YUFDcEQ7WUFDRDtnQkFDRSxJQUFJLEVBQUUsc0JBQXNCO2dCQUM1QixXQUFXLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLCtCQUErQjtvQkFDckMsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxxQkFBcUI7Z0JBQzNCLFdBQVcsRUFBRTtvQkFDWCxJQUFJLEVBQUUscUJBQXFCO29CQUMzQixnQkFBZ0IsRUFBRSxJQUFJO2lCQUN2QjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLDRCQUE0QjtnQkFDbEMsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSw0QkFBNEI7b0JBQ2xDLGdCQUFnQixFQUFFLElBQUk7aUJBQ3ZCO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsY0FBYztnQkFDcEIsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSxrQkFBa0I7b0JBQ3hCLGdCQUFnQixFQUFFLElBQUk7aUJBQ3ZCO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsbUJBQW1CO2dCQUN6QixJQUFJLEVBQUUsOENBQThDO2FBQ3JEO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLG9CQUFvQjtnQkFDMUIsSUFBSSxFQUFFLDBDQUEwQzthQUNqRDtTQUNGLENBQUM7UUFNRCxLQUFJLENBQUMsZ0JBQWdCLEdBQ2pCLEtBQUksQ0FBQyxnQkFBZ0IsUUFDckIsa0JBQWtCLENBQ3BCLENBQUM7UUFFSixLQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUk1QyxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxHQUFHO1lBQ2pDLFlBQVksRUFBRTtnQkFDWixvQkFBb0I7Z0JBQ3BCLGlCQUFpQjtnQkFDakIsSUFBSTthQUNMO1NBQ0YsQ0FBQztRQUdGLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLEdBQUc7WUFDckMsWUFBWSxFQUFFO2dCQUNaLEtBQUksQ0FBQyxTQUFTO2dCQUNkLGlCQUFpQjtnQkFDakIsOENBQThDO2FBQy9DLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxFQUFwQyxDQUFvQyxDQUFDO1NBQ3JELENBQUM7UUFHRixLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxLQUFHLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLGdCQUFrQixDQUFDO1FBSTdGLE9BQU8sS0FBSSxDQUFDLHFCQUFxQixDQUFDLG9CQUFvQixDQUFDLENBQUM7O0lBQzFELENBQUM7SUFDSCx5QkFBQztBQUFELENBQUMsQUExS0QsQ0FBd0Msd0JBQVUsR0EwS2pEO0FBMUtZLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGFyZ3YgfSBmcm9tICd5YXJncyc7XG5pbXBvcnQgeyBTZWVkQ29uZmlnIH0gZnJvbSAnLi9zZWVkLmNvbmZpZyc7XG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gJ3BhdGgnO1xuaW1wb3J0IHsgRXh0ZW5kUGFja2FnZXMgfSBmcm9tICcuL3NlZWQuY29uZmlnLmludGVyZmFjZXMnO1xuXG5leHBvcnQgY2xhc3MgU2VlZEFkdmFuY2VkQ29uZmlnIGV4dGVuZHMgU2VlZENvbmZpZyB7XG4gIC8qKlxuICAgKiBUaGUgYmFzZSBmb2xkZXIgb2YgdGhlIG5hdGl2ZXNjcmlwdCBhcHBsaWNhdGlvbnMgc291cmNlIGZpbGVzLlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgVE5TX0JBU0VfRElSID0gJ25hdGl2ZXNjcmlwdCc7XG5cbiAgc3JjU3ViZGlyID0gJ3NyYyc7XG4gIGRlc3RTdWJkaXIgPSAnYXBwJztcblxuICBUTlNfQVBQX1NSQyA9IGAke3RoaXMuVE5TX0JBU0VfRElSfS8ke3RoaXMuc3JjU3ViZGlyfWA7XG5cbiAgVE5TX0FQUF9ERVNUID0gYCR7dGhpcy5UTlNfQkFTRV9ESVJ9LyR7dGhpcy5kZXN0U3ViZGlyfWA7XG5cbiAgVE5TX0NPTkZJRyA9IHtcbiAgICBBTkFMWVRJQ1NfVFJBQ0tJTkdfSUQ6ICcnLFxuICB9O1xuXG4gICAvKipcbiAgICogSG9sZHMgYWRkZWQgcGFja2FnZXMgZm9yIGRlc2t0b3AgYnVpbGQuXG4gICAqL1xuICBERVNLVE9QX1BBQ0tBR0VTOiBFeHRlbmRQYWNrYWdlc1tdID0gW107XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgc3VwZXIoKTtcblxuICAgIHRoaXMuRU5BQkxFX1NDU1MgPSB0cnVlO1xuXG4gICAgaWYgKGFyZ3YgJiYgYXJndi5fKSB7XG4gICAgICBpZiAoYXJndlsnZGVza3RvcCddKSB7XG4gICAgICAgIHRoaXMuVEFSR0VUX0RFU0tUT1AgPSB0cnVlO1xuICAgICAgICBpZiAoYXJndlsnZGVza3RvcEJ1aWxkJ10pIHtcbiAgICAgICAgICB0aGlzLlRBUkdFVF9ERVNLVE9QX0JVSUxEID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChhcmd2WydoeWJyaWQnXSkge1xuICAgICAgICB0aGlzLlRBUkdFVF9NT0JJTEVfSFlCUklEID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG4gICAgbGV0IGJvb3RzdHJhcCA9ICdtYWluLndlYic7XG4gICAgaWYgKHRoaXMuVEFSR0VUX01PQklMRV9IWUJSSUQpIHtcbiAgICAgIC8vIFBlcmhhcHMgSW9uaWMgb3IgQ29yZG92YVxuICAgICAgLy8gVGhpcyBpcyBub3QgaW1wbGVtZW50ZWQgaW4gdGhlIHNlZWQgYnV0IGhlcmUgdG8gc2hvdyB5b3Ugd2F5IGZvcndhcmQgaWYgeW91IHdhbnRlZCB0byBhZGRcbiAgICAgIGJvb3RzdHJhcCAgID0gJ21haW4ubW9iaWxlLmh5YnJpZCc7XG4gICAgfVxuXG4gICAgaWYgKGFyZ3ZbJ2FuYWx5dGljcyddKSB7XG4gICAgICB0aGlzLlROU19DT05GSUcuQU5BTFlUSUNTX1RSQUNLSU5HX0lEID0gYXJndlsnYW5hbHl0aWNzJ107XG4gICAgfVxuXG4gICAgLy8gT3ZlcnJpZGUgc2VlZCBkZWZhdWx0c1xuICAgIHRoaXMuQk9PVFNUUkFQX0RJUiA9IGFyZ3ZbJ2FwcCddID8gKGFyZ3ZbJ2FwcCddICsgJy8nKSA6ICcnO1xuICAgIHRoaXMuQk9PVFNUUkFQX01PRFVMRSA9IGAke3RoaXMuQk9PVFNUUkFQX0RJUn0ke2Jvb3RzdHJhcH1gO1xuICAgIHRoaXMuTkdfRkFDVE9SWV9GSUxFID0gYCR7Ym9vdHN0cmFwfS5wcm9kYDtcbiAgICB0aGlzLkJPT1RTVFJBUF9QUk9EX01PRFVMRSA9IGAke3RoaXMuQk9PVFNUUkFQX0RJUn0ke2Jvb3RzdHJhcH1gO1xuICAgIHRoaXMuQk9PVFNUUkFQX0ZBQ1RPUllfUFJPRF9NT0RVTEUgPSBgJHt0aGlzLkJPT1RTVFJBUF9ESVJ9JHtib290c3RyYXB9LnByb2RgO1xuXG4gICAgdGhpcy5BUFBfVElUTEUgPSAnQW5ndWxhciBTZWVkIEFkdmFuY2VkJztcbiAgICB0aGlzLkFQUF9CQVNFID0gdGhpcy5UQVJHRVRfREVTS1RPUCA/ICcnIC8vIHBhdGhzIG11c3QgcmVtYWluIHJlbGF0aXZlIGZvciBkZXNrdG9wIGJ1aWxkXG4gICAgICA6ICcvJztcblxuICAgIC8vIEFkdmFuY2VkIHNlZWQgcGFja2FnZXNcbiAgICBsZXQgYWRkaXRpb25hbFBhY2thZ2VzOiBFeHRlbmRQYWNrYWdlc1tdID0gW1xuICAgICAge1xuICAgICAgICBuYW1lOiAnbG9kYXNoJyxcbiAgICAgICAgcGF0aDogJ25vZGVfbW9kdWxlcy9sb2Rhc2gvbG9kYXNoLmpzJyxcbiAgICAgICAgcGFja2FnZU1ldGE6IHtcbiAgICAgICAgICBtYWluOiAnaW5kZXguanMnLFxuICAgICAgICAgIGRlZmF1bHRFeHRlbnNpb246ICdqcydcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0BuZ3J4L2NvcmUnLFxuICAgICAgICBwYWNrYWdlTWV0YToge1xuICAgICAgICAgIG1haW46ICdidW5kbGVzL2NvcmUudW1kLmpzJyxcbiAgICAgICAgICBkZWZhdWx0RXh0ZW5zaW9uOiAnanMnXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdAbmdyeC9zdG9yZScsXG4gICAgICAgIHBhY2thZ2VNZXRhOiB7XG4gICAgICAgICAgbWFpbjogJ2J1bmRsZXMvc3RvcmUudW1kLmpzJyxcbiAgICAgICAgICBkZWZhdWx0RXh0ZW5zaW9uOiAnanMnXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdAbmdyeC9lZmZlY3RzJyxcbiAgICAgICAgcGFja2FnZU1ldGE6IHtcbiAgICAgICAgICBtYWluOiAnYnVuZGxlcy9lZmZlY3RzLnVtZC5qcycsXG4gICAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQG5ncngvZWZmZWN0cy90ZXN0aW5nJyxcbiAgICAgICAgcGF0aDogJ25vZGVfbW9kdWxlcy9AbmdyeC9lZmZlY3RzL3Rlc3RpbmcvaW5kZXguanMnXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQG5ncngvc3RvcmUtZGV2dG9vbHMnLFxuICAgICAgICBwYWNrYWdlTWV0YToge1xuICAgICAgICAgIG1haW46ICdidW5kbGVzL3N0b3JlLWRldnRvb2xzLnVtZC5qcycsXG4gICAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQG5neC10cmFuc2xhdGUvY29yZScsXG4gICAgICAgIHBhY2thZ2VNZXRhOiB7XG4gICAgICAgICAgbWFpbjogJ2J1bmRsZXMvY29yZS51bWQuanMnLFxuICAgICAgICAgIGRlZmF1bHRFeHRlbnNpb246ICdqcydcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0BuZ3gtdHJhbnNsYXRlL2h0dHAtbG9hZGVyJyxcbiAgICAgICAgcGFja2FnZU1ldGE6IHtcbiAgICAgICAgICBtYWluOiAnYnVuZGxlcy9odHRwLWxvYWRlci51bWQuanMnLFxuICAgICAgICAgIGRlZmF1bHRFeHRlbnNpb246ICdqcydcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2FuZ3VsYXJ0aWNzMicsXG4gICAgICAgIHBhY2thZ2VNZXRhOiB7XG4gICAgICAgICAgbWFpbjogJ2Rpc3QvY29yZS51bWQuanMnLFxuICAgICAgICAgIGRlZmF1bHRFeHRlbnNpb246ICdqcydcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ25ncngtc3RvcmUtZnJlZXplJyxcbiAgICAgICAgcGF0aDogJ25vZGVfbW9kdWxlcy9uZ3J4LXN0b3JlLWZyZWV6ZS9kaXN0L2luZGV4LmpzJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2RlZXAtZnJlZXplLXN0cmljdCcsXG4gICAgICAgIHBhdGg6ICdub2RlX21vZHVsZXMvZGVlcC1mcmVlemUtc3RyaWN0L2luZGV4LmpzJ1xuICAgICAgfVxuICAgIF07XG5cbiAgICAvKipcbiAgICAgKiBOZWVkIHRvIGR1cGxpY2F0ZSB0aGlzIGluIHRoZSBwcm9qZWN0LmNvbmZpZy50cyB0b1xuICAgICAqIHBpY2sgdXAgcGFja2FnZXMgdGhlcmUgdG9vLlxuICAgICAqL1xuICAgICB0aGlzLkRFU0tUT1BfUEFDS0FHRVMgPSBbXG4gICAgICAuLi50aGlzLkRFU0tUT1BfUEFDS0FHRVMsXG4gICAgICAuLi5hZGRpdGlvbmFsUGFja2FnZXMsXG4gICAgICBdO1xuXG4gICAgdGhpcy5hZGRQYWNrYWdlc0J1bmRsZXMoYWRkaXRpb25hbFBhY2thZ2VzKTtcblxuICAgIC8vIFNldHRpbmdzIGZvciBidWlsZGluZyBzYXNzIChpbmNsdWRlIC4vc3JzL2NsaWVudC9zY3NzIGluIGluY2x1ZGVzKVxuICAgIC8vIE5lZWRlZCBiZWNhdXNlIGZvciBjb21wb25lbnRzIHlvdSBjYW5ub3QgdXNlIC4uLy4uLy4uLyBzeW50YXhcbiAgICB0aGlzLlBMVUdJTl9DT05GSUdTWydndWxwLXNhc3MnXSA9IHtcbiAgICAgIGluY2x1ZGVQYXRoczogW1xuICAgICAgICAnLi9zcmMvY2xpZW50L3Njc3MvJyxcbiAgICAgICAgJy4vbm9kZV9tb2R1bGVzLycsXG4gICAgICAgICcuLydcbiAgICAgIF1cbiAgICB9O1xuXG4gICAgLy8gU2V0dGluZ3MgZm9yIGJ1aWxkaW5nIHNhc3MgZm9yIHRucyBtb2R1bGVzXG4gICAgdGhpcy5QTFVHSU5fQ09ORklHU1snZ3VscC1zYXNzLXRucyddID0ge1xuICAgICAgaW5jbHVkZVBhdGhzOiBbXG4gICAgICAgIHRoaXMuc3JjU3ViZGlyLFxuICAgICAgICAnLi9ub2RlX21vZHVsZXMvJyxcbiAgICAgICAgJy4vbm9kZV9tb2R1bGVzL25hdGl2ZXNjcmlwdC10aGVtZS1jb3JlL3Njc3MvJ1xuICAgICAgXS5tYXAoKGRpcikgPT4gcGF0aC5yZXNvbHZlKHRoaXMuVE5TX0JBU0VfRElSLCBkaXIpKSxcbiAgICB9O1xuXG4gICAgLy8gRml4IHVwIHBhdGggdG8gYm9vdHN0cmFwIG1vZHVsZVxuICAgIHRoaXMuU1lTVEVNX0NPTkZJRy5wYXRoc1t0aGlzLkJPT1RTVFJBUF9NT0RVTEVdID0gYCR7dGhpcy5BUFBfQkFTRX0ke3RoaXMuQk9PVFNUUkFQX01PRFVMRX1gO1xuXG4gICAgLyoqIFByb2R1Y3Rpb24gKiovXG5cbiAgICBkZWxldGUgdGhpcy5TWVNURU1fQlVJTERFUl9DT05GSUdbJ3BhY2thZ2VDb25maWdQYXRocyddOyAvLyBub3QgYWxsIGxpYnMgYXJlIGRpc3RyaWJ1dGVkIHRoZSBzYW1lXG4gIH1cbn1cbiJdfQ==