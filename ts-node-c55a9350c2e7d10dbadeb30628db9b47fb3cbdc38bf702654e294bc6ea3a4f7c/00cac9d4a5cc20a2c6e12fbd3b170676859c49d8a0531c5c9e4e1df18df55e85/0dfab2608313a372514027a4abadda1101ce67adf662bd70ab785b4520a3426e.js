"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var util = require("gulp-util");
var yargs_1 = require("yargs");
var path_1 = require("path");
var config_1 = require("../../config");
var TemplateLocalsBuilder = (function () {
    function TemplateLocalsBuilder() {
        this.stringifySystemConfigDev = false;
        this.stringifyEnvConfig = true;
    }
    TemplateLocalsBuilder.prototype.withStringifiedSystemConfigDev = function () {
        this.stringifySystemConfigDev = true;
        return this;
    };
    TemplateLocalsBuilder.prototype.withoutStringifiedEnvConfig = function () {
        this.stringifyEnvConfig = false;
        return this;
    };
    TemplateLocalsBuilder.prototype.build = function () {
        var configEnvName = yargs_1.argv['env-config'] || yargs_1.argv['config-env'] || 'dev';
        var configPath = config_1.default.getPluginConfig('environment-config');
        var envOnlyConfig = this.getConfig(configPath, configEnvName);
        var baseConfig = this.getConfig(configPath, 'base');
        if (!envOnlyConfig) {
            throw new Error(configEnvName + ' is an invalid configuration name');
        }
        var envConfig = Object.assign({}, baseConfig, envOnlyConfig);
        var locals = Object.assign({}, config_1.default, { ENV_CONFIG: this.stringifyEnvConfig ? JSON.stringify(envConfig) : envConfig });
        if (this.stringifySystemConfigDev) {
            Object.assign(locals, { SYSTEM_CONFIG_DEV: JSON.stringify(config_1.default.SYSTEM_CONFIG_DEV) });
        }
        return locals;
    };
    TemplateLocalsBuilder.prototype.getConfig = function (path, env) {
        var configPath = path_1.join(path, env);
        var config;
        try {
            config = JSON.parse(JSON.stringify(require(configPath)));
        }
        catch (e) {
            config = null;
            util.log(util.colors.red(e.message));
        }
        return config;
    };
    return TemplateLocalsBuilder;
}());
exports.TemplateLocalsBuilder = TemplateLocalsBuilder;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvcGMtMDkvRGVza3RvcC93d3cuc2VuaW9yc3BvbmdlLmNvbS11c2VyLWNvbW11bml0eS1iOTVmZjIzNTRkNWU3NTkzNGI2N2MzMThhMTY4ZDg4MDM1NTA3NGFjL3Rvb2xzL3V0aWxzL3NlZWQvdGVtcGxhdGVfbG9jYWxzLnRzIiwic291cmNlcyI6WyIvaG9tZS9wYy0wOS9EZXNrdG9wL3d3dy5zZW5pb3JzcG9uZ2UuY29tLXVzZXItY29tbXVuaXR5LWI5NWZmMjM1NGQ1ZTc1OTM0YjY3YzMxOGExNjhkODgwMzU1MDc0YWMvdG9vbHMvdXRpbHMvc2VlZC90ZW1wbGF0ZV9sb2NhbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxnQ0FBa0M7QUFDbEMsK0JBQTZCO0FBQzdCLDZCQUE0QjtBQUU1Qix1Q0FBa0M7QUFPbEM7SUFBQTtRQUNVLDZCQUF3QixHQUFHLEtBQUssQ0FBQztRQUNqQyx1QkFBa0IsR0FBRyxJQUFJLENBQUM7SUE2Q3BDLENBQUM7SUEzQ0MsOERBQThCLEdBQTlCO1FBQ0UsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQztRQUNyQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUNELDJEQUEyQixHQUEzQjtRQUNFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDaEMsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFHRCxxQ0FBSyxHQUFMO1FBQ0UsSUFBTSxhQUFhLEdBQUcsWUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLFlBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxLQUFLLENBQUM7UUFDeEUsSUFBTSxVQUFVLEdBQUcsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNoRSxJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNoRSxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUV0RCxFQUFFLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDbkIsTUFBTSxJQUFJLEtBQUssQ0FBQyxhQUFhLEdBQUcsbUNBQW1DLENBQUMsQ0FBQztRQUN2RSxDQUFDO1FBRUQsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQy9ELElBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUMzQixnQkFBTSxFQUNOLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQ2hGLENBQUM7UUFDRixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLEVBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBTSxDQUFDLGlCQUFpQixDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBQ3ZGLENBQUM7UUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFTyx5Q0FBUyxHQUFqQixVQUFrQixJQUFZLEVBQUUsR0FBVztRQUN6QyxJQUFNLFVBQVUsR0FBRyxXQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ25DLElBQUksTUFBVyxDQUFDO1FBQ2hCLElBQUksQ0FBQztZQUNILE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMzRCxDQUFDO1FBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNYLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDZCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7UUFFRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFDSCw0QkFBQztBQUFELENBQUMsQUEvQ0QsSUErQ0M7QUEvQ1ksc0RBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgdXRpbCBmcm9tICdndWxwLXV0aWwnO1xuaW1wb3J0IHsgYXJndiB9IGZyb20gJ3lhcmdzJztcbmltcG9ydCB7IGpvaW4gfSBmcm9tICdwYXRoJztcblxuaW1wb3J0IENvbmZpZyBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIEJ1aWxkcyBhbiBvYmplY3QgY29uc2lzdGluZyBvZiB0aGUgYmFzZSBjb25maWd1cmF0aW9uIHByb3ZpZGVkIGJ5IGNvbmZnL3NlZWQuY29uZmlnLnRzLCB0aGUgYWRkaXRpb25hbFxuICogcHJvamVjdCBzcGVjaWZpYyBvdmVycmlkZXMgYXMgZGVmaW5lZCBpbiBjb25maWcvcHJvamVjdC5jb25maWcudHMgYW5kIGluY2x1ZGluZyB0aGUgYmFzZSBlbnZpcm9ubWVudCBjb25maWcgYXMgZGVmaW5lZCBpbiBlbnYvYmFzZS50c1xuICogYW5kIHRoZSBlbnZpcm9ubWVudCBzcGVjaWZpYyBvdmVycmlkZXMgKGZvciBpbnN0YW5jZSBpZiBlbnY9ZGV2IHRoZW4gYXMgZGVmaW5lZCBpbiBlbnYvZGV2LnRzKS5cbiAqL1xuZXhwb3J0IGNsYXNzIFRlbXBsYXRlTG9jYWxzQnVpbGRlciB7XG4gIHByaXZhdGUgc3RyaW5naWZ5U3lzdGVtQ29uZmlnRGV2ID0gZmFsc2U7XG4gIHByaXZhdGUgc3RyaW5naWZ5RW52Q29uZmlnID0gdHJ1ZTtcblxuICB3aXRoU3RyaW5naWZpZWRTeXN0ZW1Db25maWdEZXYoKSB7XG4gICAgdGhpcy5zdHJpbmdpZnlTeXN0ZW1Db25maWdEZXYgPSB0cnVlO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG4gIHdpdGhvdXRTdHJpbmdpZmllZEVudkNvbmZpZygpIHtcbiAgICB0aGlzLnN0cmluZ2lmeUVudkNvbmZpZyA9IGZhbHNlO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cblxuICBidWlsZCgpIHtcbiAgICBjb25zdCBjb25maWdFbnZOYW1lID0gYXJndlsnZW52LWNvbmZpZyddIHx8IGFyZ3ZbJ2NvbmZpZy1lbnYnXSB8fCAnZGV2JztcbiAgICBjb25zdCBjb25maWdQYXRoID0gQ29uZmlnLmdldFBsdWdpbkNvbmZpZygnZW52aXJvbm1lbnQtY29uZmlnJyk7XG4gICAgY29uc3QgZW52T25seUNvbmZpZyA9IHRoaXMuZ2V0Q29uZmlnKGNvbmZpZ1BhdGgsIGNvbmZpZ0Vudk5hbWUpO1xuICAgIGNvbnN0IGJhc2VDb25maWcgPSB0aGlzLmdldENvbmZpZyhjb25maWdQYXRoLCAnYmFzZScpO1xuXG4gICAgaWYgKCFlbnZPbmx5Q29uZmlnKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoY29uZmlnRW52TmFtZSArICcgaXMgYW4gaW52YWxpZCBjb25maWd1cmF0aW9uIG5hbWUnKTtcbiAgICB9XG5cbiAgICBjb25zdCBlbnZDb25maWcgPSBPYmplY3QuYXNzaWduKHt9LCBiYXNlQ29uZmlnLCBlbnZPbmx5Q29uZmlnKTtcbiAgICBsZXQgbG9jYWxzID0gT2JqZWN0LmFzc2lnbih7fSxcbiAgICAgIENvbmZpZyxcbiAgICAgIHsgRU5WX0NPTkZJRzogdGhpcy5zdHJpbmdpZnlFbnZDb25maWcgPyBKU09OLnN0cmluZ2lmeShlbnZDb25maWcpIDogZW52Q29uZmlnIH1cbiAgICApO1xuICAgIGlmICh0aGlzLnN0cmluZ2lmeVN5c3RlbUNvbmZpZ0Rldikge1xuICAgICAgT2JqZWN0LmFzc2lnbihsb2NhbHMsIHtTWVNURU1fQ09ORklHX0RFVjogSlNPTi5zdHJpbmdpZnkoQ29uZmlnLlNZU1RFTV9DT05GSUdfREVWKX0pO1xuICAgIH1cbiAgICByZXR1cm4gbG9jYWxzO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRDb25maWcocGF0aDogc3RyaW5nLCBlbnY6IHN0cmluZykge1xuICAgIGNvbnN0IGNvbmZpZ1BhdGggPSBqb2luKHBhdGgsIGVudik7XG4gICAgbGV0IGNvbmZpZzogYW55O1xuICAgIHRyeSB7XG4gICAgICBjb25maWcgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHJlcXVpcmUoY29uZmlnUGF0aCkpKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBjb25maWcgPSBudWxsO1xuICAgICAgdXRpbC5sb2codXRpbC5jb2xvcnMucmVkKGUubWVzc2FnZSkpO1xuICAgIH1cblxuICAgIHJldHVybiBjb25maWc7XG4gIH1cbn1cbiJdfQ==