"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
var slash = require("slash");
var yargs_1 = require("yargs");
exports.BUILD_TYPES = {
    DEVELOPMENT: 'dev',
    PRODUCTION: 'prod'
};
var SeedConfig = (function () {
    function SeedConfig() {
        this.PORT = yargs_1.argv['port'] || 5555;
        this.PROJECT_ROOT = path_1.join(__dirname, '../..');
        this.BUILD_TYPE = getBuildType();
        this.DEBUG = yargs_1.argv['debug'] || false;
        this.DOCS_PORT = yargs_1.argv['docs-port'] || 4003;
        this.COVERAGE_PORT = yargs_1.argv['coverage-port'] || 4004;
        this.COVERAGE_DIR = 'coverage_js';
        this.COVERAGE_TS_DIR = 'coverage';
        this.APP_BASE = yargs_1.argv['base'] || '/';
        this.NPM_BASE = slash(path_1.join('.', this.APP_BASE, 'node_modules/'));
        this.TARGET_MOBILE_HYBRID = false;
        this.TARGET_DESKTOP = false;
        this.TARGET_DESKTOP_BUILD = false;
        this.TYPED_COMPILE_INTERVAL = 0;
        this.BOOTSTRAP_DIR = yargs_1.argv['app'] || 'app';
        this.APP_CLIENT = yargs_1.argv['client'] || 'client';
        this.BOOTSTRAP_MODULE = this.BOOTSTRAP_DIR + "/main";
        this.BOOTSTRAP_PROD_MODULE = this.BOOTSTRAP_DIR + "/" + 'main';
        this.NG_FACTORY_FILE = 'main-prod';
        this.BOOTSTRAP_FACTORY_PROD_MODULE = this.BOOTSTRAP_DIR + "/" + this.NG_FACTORY_FILE;
        this.APP_TITLE = 'Welcome to angular-seed!';
        this.GOOGLE_ANALYTICS_ID = 'UA-XXXXXXXX-X';
        this.APP_SRC = "src/" + this.APP_CLIENT;
        this.APP_PROJECTNAME = 'tsconfig.json';
        this.ASSETS_SRC = this.APP_SRC + "/assets";
        this.CSS_SRC = this.APP_SRC + "/css";
        this.E2E_SRC = 'src/e2e';
        this.SCSS_SRC = this.APP_SRC + "/scss";
        this.TOOLS_DIR = 'tools';
        this.SEED_TASKS_DIR = path_1.join(process.cwd(), this.TOOLS_DIR, 'tasks', 'seed');
        this.SEED_COMPOSITE_TASKS = path_1.join(process.cwd(), this.TOOLS_DIR, 'config', 'seed.tasks.json');
        this.PROJECT_COMPOSITE_TASKS = path_1.join(process.cwd(), this.TOOLS_DIR, 'config', 'project.tasks.json');
        this.DOCS_DEST = 'docs';
        this.DIST_DIR = 'dist';
        this.DEV_DEST = this.DIST_DIR + "/dev";
        this.PROD_DEST = this.DIST_DIR + "/prod";
        this.E2E_DEST = this.DIST_DIR + "/e2e";
        this.TMP_DIR = this.DIST_DIR + "/tmp";
        this.APP_DEST = this.BUILD_TYPE === exports.BUILD_TYPES.DEVELOPMENT ? this.DEV_DEST : this.PROD_DEST;
        this.CSS_DEST = this.APP_DEST + "/css";
        this.JS_DEST = this.APP_DEST + "/js";
        this.VERSION = appVersion();
        this.SERVER_STARTUP_BANNER = 'Starting the server..';
        this.CSS_BUNDLE_NAME = 'main';
        this.JS_PROD_SHIMS_BUNDLE = 'shims.js';
        this.JS_PROD_APP_BUNDLE = 'app.js';
        this.VERSION_NPM = '3.0.0';
        this.VERSION_NODE = '5.0.0';
        this.ENABLE_SCSS = ['true', '1'].indexOf(("" + process.env.ENABLE_SCSS).toLowerCase()) !== -1 || yargs_1.argv['scss'] || false;
        this.FORCE_TSLINT_EMIT_ERROR = !!process.env.FORCE_TSLINT_EMIT_ERROR;
        this.EXTRA_WATCH_PATHS = [];
        this.NPM_DEPENDENCIES = [
            { src: 'core-js/client/shim.min.js', inject: 'shims' },
            { src: 'zone.js/dist/zone.js', inject: 'libs' },
            { src: 'zone.js/dist/long-stack-trace-zone.js', inject: 'libs', buildType: exports.BUILD_TYPES.DEVELOPMENT },
            { src: 'intl/dist/Intl.min.js', inject: 'shims' },
            { src: 'systemjs/dist/system.src.js', inject: 'shims', buildType: exports.BUILD_TYPES.DEVELOPMENT },
            { src: '.tmp/Rx.min.js', inject: 'libs', buildType: exports.BUILD_TYPES.DEVELOPMENT },
        ];
        this.APP_ASSETS = [];
        this.TEMP_FILES = [
            '**/*___jb_tmp___',
            '**/*~',
            '**/*.tns.scss',
            '**/*.tns.css',
            '**/*.tns.html',
            'src/**/*.js',
            'src/**/*.js.map',
        ];
        this.ROLLUP_INCLUDE_DIR = ['node_modules/**'];
        this.ROLLUP_NAMED_EXPORTS = [];
        this.SYSTEM_CONFIG_DEV = {
            defaultJSExtensions: true,
            paths: (_a = {},
                _a[this.BOOTSTRAP_MODULE] = "" + this.APP_BASE + this.BOOTSTRAP_MODULE,
                _a['@angular/animations'] = 'node_modules/@angular/animations/bundles/animations.umd.js',
                _a['@angular/platform-browser/animations'] = 'node_modules/@angular/platform-browser/bundles/platform-browser-animations.umd.js',
                _a['@angular/common'] = 'node_modules/@angular/common/bundles/common.umd.js',
                _a['@angular/compiler'] = 'node_modules/@angular/compiler/bundles/compiler.umd.js',
                _a['@angular/core'] = 'node_modules/@angular/core/bundles/core.umd.js',
                _a['@angular/forms'] = 'node_modules/@angular/forms/bundles/forms.umd.js',
                _a['@angular/http'] = 'node_modules/@angular/http/bundles/http.umd.js',
                _a['@angular/platform-browser'] = 'node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
                _a['@angular/platform-browser-dynamic'] = 'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
                _a['@angular/router'] = 'node_modules/@angular/router/bundles/router.umd.js',
                _a['@angular/animations/browser'] = 'node_modules/@angular/animations/bundles/animations-browser.umd.js',
                _a['@angular/common/testing'] = 'node_modules/@angular/common/bundles/common-testing.umd.js',
                _a['@angular/compiler/testing'] = 'node_modules/@angular/compiler/bundles/compiler-testing.umd.js',
                _a['@angular/core/testing'] = 'node_modules/@angular/core/bundles/core-testing.umd.js',
                _a['@angular/http/testing'] = 'node_modules/@angular/http/bundles/http-testing.umd.js',
                _a['@angular/platform-browser/testing'] = 'node_modules/@angular/platform-browser/bundles/platform-browser-testing.umd.js',
                _a['@angular/platform-browser-dynamic/testing'] = 'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic-testing.umd.js',
                _a['@angular/router/testing'] = 'node_modules/@angular/router/bundles/router-testing.umd.js',
                _a['app/*'] = '/app/*',
                _a['dist/dev/*'] = '/base/dist/dev/*',
                _a['*'] = 'node_modules/*',
                _a),
            packages: {}
        };
        this.SYSTEM_CONFIG = this.SYSTEM_CONFIG_DEV;
        this.SYSTEM_BUILDER_CONFIG = {
            defaultJSExtensions: true,
            base: this.PROJECT_ROOT,
            packageConfigPaths: [
                path_1.join('node_modules', '*', 'package.json'),
                path_1.join('node_modules', '@angular', '*', 'package.json')
            ],
            paths: (_b = {},
                _b[path_1.join(this.TMP_DIR, '*')] = this.TMP_DIR + "/*",
                _b['@angular/platform-browser/animations'] = 'node_modules/@angular/platform-browser/bundles/platform-browser-animations.umd.js',
                _b['@angular/animations/browser'] = 'node_modules/@angular/animations/bundles/animations-browser.umd.js',
                _b['dist/tmp/node_modules/*'] = 'dist/tmp/node_modules/*',
                _b['node_modules/*'] = 'node_modules/*',
                _b['*'] = 'node_modules/*',
                _b),
            packages: {
                '@angular/animations': {
                    main: 'bundles/animations.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/common': {
                    main: 'bundles/common.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/compiler': {
                    main: 'bundles/compiler.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/core/testing': {
                    main: 'bundles/core-testing.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/core': {
                    main: 'bundles/core.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/forms': {
                    main: 'bundles/forms.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/http': {
                    main: 'bundles/http.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/platform-browser': {
                    main: 'bundles/platform-browser.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/platform-browser-dynamic': {
                    main: 'bundles/platform-browser-dynamic.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/router': {
                    main: 'bundles/router.umd.js',
                    defaultExtension: 'js'
                },
                '@angular/service-worker': {
                    main: 'bundles/service-worker.umd.js',
                    defaultExtension: 'js'
                },
                'rxjs': {
                    main: 'Rx.js',
                    defaultExtension: 'js'
                }
            }
        };
        this.BROWSER_LIST = [
            'ie >= 10',
            'ie_mob >= 10',
            'ff >= 30',
            'chrome >= 34',
            'safari >= 7',
            'opera >= 23',
            'ios >= 7',
            'android >= 4.4',
            'bb >= 10'
        ];
        this.COLOR_GUARD_WHITE_LIST = [];
        this.PROXY_MIDDLEWARE = [];
        this.PLUGIN_CONFIGS = {};
        this.QUERY_STRING_GENERATOR = function () {
            return Date.now().toString();
        };
        var _a, _b;
    }
    Object.defineProperty(SeedConfig.prototype, "DEPENDENCIES", {
        get: function () {
            return normalizeDependencies(this.NPM_DEPENDENCIES.filter(filterDependency.bind(null, this.BUILD_TYPE)))
                .concat(this._APP_ASSETS.filter(filterDependency.bind(null, this.BUILD_TYPE)));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeedConfig.prototype, "_APP_ASSETS", {
        get: function () {
            return [
                { src: this.CSS_SRC + "/" + this.CSS_BUNDLE_NAME + "." + this.getInjectableStyleExtension(), inject: true, vendor: false }
            ].concat(this.APP_ASSETS);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeedConfig.prototype, "_PLUGIN_CONFIGS", {
        get: function () {
            var defaults = {
                'browser-sync': {
                    middleware: [require('connect-history-api-fallback')({
                            index: this.APP_BASE + "index.html"
                        })].concat(this.PROXY_MIDDLEWARE),
                    port: this.PORT,
                    startPath: this.APP_BASE,
                    open: yargs_1.argv['b'] ? false : true,
                    injectChanges: false,
                    server: {
                        baseDir: this.DIST_DIR + "/empty/",
                        routes: (_a = {},
                            _a["" + this.APP_BASE + this.APP_SRC] = this.APP_SRC,
                            _a["" + this.APP_BASE + this.APP_DEST] = this.APP_DEST,
                            _a[this.APP_BASE + "node_modules"] = 'node_modules',
                            _a["" + this.APP_BASE.replace(/\/$/, '')] = this.APP_DEST,
                            _a)
                    }
                },
                'environment-config': path_1.join(this.PROJECT_ROOT, this.TOOLS_DIR, 'env'),
                'gulp-sass': {
                    includePaths: ['./node_modules/']
                },
                'gulp-concat-css': {
                    targetFile: this.CSS_BUNDLE_NAME + ".css",
                    options: {
                        rebaseUrls: false
                    }
                }
            };
            this.mergeObject(defaults, this.PLUGIN_CONFIGS);
            return defaults;
            var _a;
        },
        enumerable: true,
        configurable: true
    });
    SeedConfig.prototype.getKarmaReporters = function () {
        return {
            preprocessors: {
                'dist/**/!(*spec|index|*.module|*.routes).js': ['coverage']
            },
            reporters: ['mocha', 'coverage', 'karma-remap-istanbul'],
            coverageReporter: {
                dir: this.COVERAGE_DIR + '/',
                reporters: [
                    { type: 'json', subdir: '.', file: 'coverage-final.json' },
                    { type: 'html', subdir: '.' }
                ]
            },
            remapIstanbulReporter: {
                reports: {
                    html: this.COVERAGE_TS_DIR
                }
            }
        };
    };
    SeedConfig.prototype.mergeObject = function (target, source) {
        var deepExtend = require('deep-extend');
        deepExtend(target, source);
    };
    SeedConfig.prototype.getPluginConfig = function (pluginKey) {
        if (this._PLUGIN_CONFIGS[pluginKey]) {
            return this._PLUGIN_CONFIGS[pluginKey];
        }
        return null;
    };
    SeedConfig.prototype.getInjectableStyleExtension = function () {
        return this.BUILD_TYPE === exports.BUILD_TYPES.PRODUCTION && this.ENABLE_SCSS ? 'scss' : 'css';
    };
    SeedConfig.prototype.addPackageBundles = function (pack) {
        if (pack.path) {
            this.SYSTEM_CONFIG_DEV.paths[pack.name] = pack.path;
            this.SYSTEM_BUILDER_CONFIG.paths[pack.name] = pack.path;
        }
        if (pack.packageMeta) {
            this.SYSTEM_CONFIG_DEV.packages[pack.name] = pack.packageMeta;
            this.SYSTEM_BUILDER_CONFIG.packages[pack.name] = pack.packageMeta;
        }
    };
    SeedConfig.prototype.addPackagesBundles = function (packs) {
        var _this = this;
        packs.forEach(function (pack) {
            _this.addPackageBundles(pack);
        });
    };
    SeedConfig.prototype.getRollupNamedExports = function () {
        var namedExports = {};
        this.ROLLUP_NAMED_EXPORTS.map(function (namedExport) {
            namedExports = Object.assign(namedExports, namedExport);
        });
        return namedExports;
    };
    return SeedConfig;
}());
exports.SeedConfig = SeedConfig;
function normalizeDependencies(deps) {
    deps
        .filter(function (d) { return !/\*/.test(d.src); })
        .forEach(function (d) { return d.src = require.resolve(d.src); });
    return deps;
}
exports.normalizeDependencies = normalizeDependencies;
function filterDependency(type, d) {
    var t = d.buildType || d.env;
    d.buildType = t;
    if (!t) {
        d.buildType = Object.keys(exports.BUILD_TYPES).map(function (k) { return exports.BUILD_TYPES[k]; });
    }
    if (!(d.buildType instanceof Array)) {
        d.env = [d.buildType];
    }
    return d.buildType.indexOf(type) >= 0;
}
function appVersion() {
    var pkg = require('../../package.json');
    return pkg.version;
}
function getBuildType() {
    var type = (yargs_1.argv['build-type'] || yargs_1.argv['env'] || '').toLowerCase();
    var base = yargs_1.argv['_'];
    var prodKeyword = !!base.filter(function (o) { return o.indexOf(exports.BUILD_TYPES.PRODUCTION) >= 0; }).pop();
    if ((base && prodKeyword) || type === exports.BUILD_TYPES.PRODUCTION) {
        return exports.BUILD_TYPES.PRODUCTION;
    }
    else {
        return exports.BUILD_TYPES.DEVELOPMENT;
    }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvcGMtMDkvRGVza3RvcC93d3cuc2VuaW9yc3BvbmdlLmNvbS11c2VyLWNvbW11bml0eS1iOTVmZjIzNTRkNWU3NTkzNGI2N2MzMThhMTY4ZDg4MDM1NTA3NGFjL3Rvb2xzL2NvbmZpZy9zZWVkLmNvbmZpZy50cyIsInNvdXJjZXMiOlsiL2hvbWUvcGMtMDkvRGVza3RvcC93d3cuc2VuaW9yc3BvbmdlLmNvbS11c2VyLWNvbW11bml0eS1iOTVmZjIzNTRkNWU3NTkzNGI2N2MzMThhMTY4ZDg4MDM1NTA3NGFjL3Rvb2xzL2NvbmZpZy9zZWVkLmNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDZCQUE0QjtBQUM1Qiw2QkFBK0I7QUFDL0IsK0JBQTZCO0FBd0NoQixRQUFBLFdBQVcsR0FBYztJQUNwQyxXQUFXLEVBQUUsS0FBSztJQUNsQixVQUFVLEVBQUUsTUFBTTtDQUNuQixDQUFDO0FBY0Y7SUFBQTtRQU9FLFNBQUksR0FBRyxZQUFJLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDO1FBSzVCLGlCQUFZLEdBQUcsV0FBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztRQU14QyxlQUFVLEdBQUcsWUFBWSxFQUFFLENBQUM7UUFPNUIsVUFBSyxHQUFHLFlBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLENBQUM7UUFPL0IsY0FBUyxHQUFHLFlBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLENBQUM7UUFPdEMsa0JBQWEsR0FBRyxZQUFJLENBQUMsZUFBZSxDQUFDLElBQUksSUFBSSxDQUFDO1FBTTlDLGlCQUFZLEdBQUcsYUFBYSxDQUFDO1FBQzdCLG9CQUFlLEdBQUcsVUFBVSxDQUFDO1FBUTdCLGFBQVEsR0FBRyxZQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDO1FBTS9CLGFBQVEsR0FBRyxLQUFLLENBQUMsV0FBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFPNUQseUJBQW9CLEdBQUcsS0FBSyxDQUFDO1FBTzdCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBT3ZCLHlCQUFvQixHQUFHLEtBQUssQ0FBQztRQVc3QiwyQkFBc0IsR0FBRyxDQUFDLENBQUM7UUFPM0Isa0JBQWEsR0FBRyxZQUFJLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDO1FBT3JDLGVBQVUsR0FBRyxZQUFJLENBQUMsUUFBUSxDQUFDLElBQUksUUFBUSxDQUFDO1FBTXhDLHFCQUFnQixHQUFNLElBQUksQ0FBQyxhQUFhLFVBQU8sQ0FBQztRQUVoRCwwQkFBcUIsR0FBTSxJQUFJLENBQUMsYUFBYSxNQUFHLEdBQUcsTUFBTSxDQUFDO1FBRTFELG9CQUFlLEdBQUcsV0FBVyxDQUFDO1FBRTlCLGtDQUE2QixHQUFNLElBQUksQ0FBQyxhQUFhLFNBQUksSUFBSSxDQUFDLGVBQWlCLENBQUM7UUFNaEYsY0FBUyxHQUFHLDBCQUEwQixDQUFDO1FBTXZDLHdCQUFtQixHQUFHLGVBQWUsQ0FBQztRQU10QyxZQUFPLEdBQUcsU0FBTyxJQUFJLENBQUMsVUFBWSxDQUFDO1FBTW5DLG9CQUFlLEdBQUcsZUFBZSxDQUFDO1FBTWxDLGVBQVUsR0FBTSxJQUFJLENBQUMsT0FBTyxZQUFTLENBQUM7UUFNdEMsWUFBTyxHQUFNLElBQUksQ0FBQyxPQUFPLFNBQU0sQ0FBQztRQUtoQyxZQUFPLEdBQUcsU0FBUyxDQUFDO1FBTXBCLGFBQVEsR0FBTSxJQUFJLENBQUMsT0FBTyxVQUFPLENBQUM7UUFNbEMsY0FBUyxHQUFHLE9BQU8sQ0FBQztRQUtwQixtQkFBYyxHQUFHLFdBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFLdEUseUJBQW9CLEdBQUcsV0FBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBT3hGLDRCQUF1QixHQUFHLFdBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztRQU05RixjQUFTLEdBQUcsTUFBTSxDQUFDO1FBTW5CLGFBQVEsR0FBRyxNQUFNLENBQUM7UUFNbEIsYUFBUSxHQUFNLElBQUksQ0FBQyxRQUFRLFNBQU0sQ0FBQztRQU1sQyxjQUFTLEdBQU0sSUFBSSxDQUFDLFFBQVEsVUFBTyxDQUFDO1FBTXBDLGFBQVEsR0FBTSxJQUFJLENBQUMsUUFBUSxTQUFNLENBQUM7UUFNbEMsWUFBTyxHQUFNLElBQUksQ0FBQyxRQUFRLFNBQU0sQ0FBQztRQU1qQyxhQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxtQkFBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQU14RixhQUFRLEdBQU0sSUFBSSxDQUFDLFFBQVEsU0FBTSxDQUFDO1FBTWxDLFlBQU8sR0FBTSxJQUFJLENBQUMsUUFBUSxRQUFLLENBQUM7UUFLaEMsWUFBTyxHQUFHLFVBQVUsRUFBRSxDQUFDO1FBTXZCLDBCQUFxQixHQUFHLHVCQUF1QixDQUFDO1FBTWhELG9CQUFlLEdBQUcsTUFBTSxDQUFDO1FBTXpCLHlCQUFvQixHQUFHLFVBQVUsQ0FBQztRQU1sQyx1QkFBa0IsR0FBRyxRQUFRLENBQUM7UUFNOUIsZ0JBQVcsR0FBRyxPQUFPLENBQUM7UUFNdEIsaUJBQVksR0FBRyxPQUFPLENBQUM7UUFPdkIsZ0JBQVcsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQSxLQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBYSxDQUFBLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxZQUFJLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDO1FBTWhILDRCQUF1QixHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDO1FBTWhFLHNCQUFpQixHQUFhLEVBQUUsQ0FBQztRQU1qQyxxQkFBZ0IsR0FBMkI7WUFDekMsRUFBRSxHQUFHLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRTtZQUN0RCxFQUFFLEdBQUcsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFO1lBQy9DLEVBQUUsR0FBRyxFQUFFLHVDQUF1QyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLG1CQUFXLENBQUMsV0FBVyxFQUFFO1lBQ3BHLEVBQUUsR0FBRyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUU7WUFDakQsRUFBRSxHQUFHLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsbUJBQVcsQ0FBQyxXQUFXLEVBQUU7WUFFM0YsRUFBRSxHQUFHLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsbUJBQVcsQ0FBQyxXQUFXLEVBQUU7U0FDOUUsQ0FBQztRQU1GLGVBQVUsR0FBMkIsRUFBRSxDQUFDO1FBTXhDLGVBQVUsR0FBYTtZQUNyQixrQkFBa0I7WUFDbEIsT0FBTztZQUdQLGVBQWU7WUFDZixjQUFjO1lBQ2QsZUFBZTtZQUNmLGFBQWE7WUFDYixpQkFBaUI7U0FDbEIsQ0FBQztRQU1GLHVCQUFrQixHQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQU9uRCx5QkFBb0IsR0FBVSxFQUFFLENBQUM7UUFlakMsc0JBQWlCLEdBQVE7WUFDdkIsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixLQUFLO2dCQUNILEdBQUMsSUFBSSxDQUFDLGdCQUFnQixJQUFHLEtBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWtCO2dCQUNuRSx5QkFBcUIsR0FBRSw0REFBNEQ7Z0JBQ25GLDBDQUFzQyxHQUFFLG1GQUFtRjtnQkFDM0gscUJBQWlCLEdBQUUsb0RBQW9EO2dCQUN2RSx1QkFBbUIsR0FBRSx3REFBd0Q7Z0JBQzdFLG1CQUFlLEdBQUUsZ0RBQWdEO2dCQUNqRSxvQkFBZ0IsR0FBRSxrREFBa0Q7Z0JBQ3BFLG1CQUFlLEdBQUUsZ0RBQWdEO2dCQUNqRSwrQkFBMkIsR0FBRSx3RUFBd0U7Z0JBQ3JHLHVDQUFtQyxHQUFFLHdGQUF3RjtnQkFDN0gscUJBQWlCLEdBQUUsb0RBQW9EO2dCQUN2RSxpQ0FBNkIsR0FBRSxvRUFBb0U7Z0JBRW5HLDZCQUF5QixHQUFFLDREQUE0RDtnQkFDdkYsK0JBQTJCLEdBQUUsZ0VBQWdFO2dCQUM3RiwyQkFBdUIsR0FBRSx3REFBd0Q7Z0JBQ2pGLDJCQUF1QixHQUFFLHdEQUF3RDtnQkFDakYsdUNBQW1DLEdBQ25DLGdGQUFnRjtnQkFDaEYsK0NBQTJDLEdBQzNDLGdHQUFnRztnQkFDaEcsNkJBQXlCLEdBQUUsNERBQTREO2dCQUV2RixXQUFPLEdBQUUsUUFBUTtnQkFFakIsZ0JBQVksR0FBRSxrQkFBa0I7Z0JBQ2hDLE9BQUcsR0FBRSxnQkFBZ0I7bUJBQ3RCO1lBQ0QsUUFBUSxFQUFFLEVBQ1Q7U0FDRixDQUFDO1FBT0Ysa0JBQWEsR0FBUSxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFNNUMsMEJBQXFCLEdBQVE7WUFDM0IsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVk7WUFDdkIsa0JBQWtCLEVBQUU7Z0JBQ2xCLFdBQUksQ0FBQyxjQUFjLEVBQUUsR0FBRyxFQUFFLGNBQWMsQ0FBQztnQkFDekMsV0FBSSxDQUFDLGNBQWMsRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLGNBQWMsQ0FBQzthQUl0RDtZQUNELEtBQUs7Z0JBSUgsR0FBQyxXQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsSUFBTSxJQUFJLENBQUMsT0FBTyxPQUFJO2dCQUU5QywwQ0FBc0MsR0FBRSxtRkFBbUY7Z0JBQzNILGlDQUE2QixHQUFFLG9FQUFvRTtnQkFDbkcsNkJBQXlCLEdBQUUseUJBQXlCO2dCQUNwRCxvQkFBZ0IsR0FBRSxnQkFBZ0I7Z0JBQ2xDLE9BQUcsR0FBRSxnQkFBZ0I7bUJBQ3RCO1lBQ0QsUUFBUSxFQUFFO2dCQUNSLHFCQUFxQixFQUFFO29CQUNyQixJQUFJLEVBQUUsMkJBQTJCO29CQUNqQyxnQkFBZ0IsRUFBRSxJQUFJO2lCQUN2QjtnQkFDRCxpQkFBaUIsRUFBRTtvQkFDakIsSUFBSSxFQUFFLHVCQUF1QjtvQkFDN0IsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7Z0JBQ0QsbUJBQW1CLEVBQUU7b0JBQ25CLElBQUksRUFBRSx5QkFBeUI7b0JBQy9CLGdCQUFnQixFQUFFLElBQUk7aUJBQ3ZCO2dCQUNELHVCQUF1QixFQUFFO29CQUN2QixJQUFJLEVBQUUsNkJBQTZCO29CQUNuQyxnQkFBZ0IsRUFBRSxJQUFJO2lCQUN2QjtnQkFDRCxlQUFlLEVBQUU7b0JBQ2YsSUFBSSxFQUFFLHFCQUFxQjtvQkFDM0IsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7Z0JBQ0QsZ0JBQWdCLEVBQUU7b0JBQ2hCLElBQUksRUFBRSxzQkFBc0I7b0JBQzVCLGdCQUFnQixFQUFFLElBQUk7aUJBQ3ZCO2dCQUNELGVBQWUsRUFBRTtvQkFDZixJQUFJLEVBQUUscUJBQXFCO29CQUMzQixnQkFBZ0IsRUFBRSxJQUFJO2lCQUN2QjtnQkFDRCwyQkFBMkIsRUFBRTtvQkFDM0IsSUFBSSxFQUFFLGlDQUFpQztvQkFDdkMsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7Z0JBQ0QsbUNBQW1DLEVBQUU7b0JBQ25DLElBQUksRUFBRSx5Q0FBeUM7b0JBQy9DLGdCQUFnQixFQUFFLElBQUk7aUJBQ3ZCO2dCQUNELGlCQUFpQixFQUFFO29CQUNqQixJQUFJLEVBQUUsdUJBQXVCO29CQUM3QixnQkFBZ0IsRUFBRSxJQUFJO2lCQUN2QjtnQkFDRCx5QkFBeUIsRUFBRTtvQkFDekIsSUFBSSxFQUFFLCtCQUErQjtvQkFDckMsZ0JBQWdCLEVBQUUsSUFBSTtpQkFDdkI7Z0JBQ0QsTUFBTSxFQUFFO29CQUNOLElBQUksRUFBRSxPQUFPO29CQUNiLGdCQUFnQixFQUFFLElBQUk7aUJBQ3ZCO2FBQ0Y7U0FDRixDQUFDO1FBTUYsaUJBQVksR0FBRztZQUNiLFVBQVU7WUFDVixjQUFjO1lBQ2QsVUFBVTtZQUNWLGNBQWM7WUFDZCxhQUFhO1lBQ2IsYUFBYTtZQUNiLFVBQVU7WUFDVixnQkFBZ0I7WUFDaEIsVUFBVTtTQUNYLENBQUM7UUFNRiwyQkFBc0IsR0FBdUIsRUFDNUMsQ0FBQztRQU1GLHFCQUFnQixHQUFVLEVBQUUsQ0FBQztRQU03QixtQkFBYyxHQUFRLEVBQUUsQ0FBQztRQUt6QiwyQkFBc0IsR0FBRztZQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQy9CLENBQUMsQ0FBQTs7SUEwSkgsQ0FBQztJQW5VQyxzQkFBSSxvQ0FBWTthQUFoQjtZQUNFLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7aUJBQ3JHLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkYsQ0FBQzs7O09BQUE7SUE0S0Qsc0JBQVksbUNBQVc7YUFBdkI7WUFDRSxNQUFNO2dCQUNKLEVBQUUsR0FBRyxFQUFLLElBQUksQ0FBQyxPQUFPLFNBQUksSUFBSSxDQUFDLGVBQWUsU0FBSSxJQUFJLENBQUMsMkJBQTJCLEVBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7cUJBQ2xILElBQUksQ0FBQyxVQUFVLEVBQ2xCO1FBQ0osQ0FBQzs7O09BQUE7SUFLRCxzQkFBWSx1Q0FBZTthQUEzQjtZQVFFLElBQUksUUFBUSxHQUFHO2dCQUNiLGNBQWMsRUFBRTtvQkFDZCxVQUFVLEdBQUcsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUM7NEJBQ25ELEtBQUssRUFBSyxJQUFJLENBQUMsUUFBUSxlQUFZO3lCQUNwQyxDQUFDLFNBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDO29CQUM3QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7b0JBQ2YsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRO29CQUN4QixJQUFJLEVBQUUsWUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBQzlCLGFBQWEsRUFBRSxLQUFLO29CQUNwQixNQUFNLEVBQUU7d0JBQ04sT0FBTyxFQUFLLElBQUksQ0FBQyxRQUFRLFlBQVM7d0JBQ2xDLE1BQU07NEJBQ0osR0FBQyxLQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQVMsSUFBRyxJQUFJLENBQUMsT0FBTzs0QkFDakQsR0FBQyxLQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVUsSUFBRyxJQUFJLENBQUMsUUFBUTs0QkFDbkQsR0FBSSxJQUFJLENBQUMsUUFBUSxpQkFBYyxJQUFHLGNBQWM7NEJBQ2hELEdBQUMsS0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFHLElBQUcsSUFBSSxDQUFDLFFBQVE7K0JBQ3ZEO3FCQUNGO2lCQUNGO2dCQUdELG9CQUFvQixFQUFFLFdBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDO2dCQU9wRSxXQUFXLEVBQUU7b0JBQ1gsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7aUJBQ2xDO2dCQU9ELGlCQUFpQixFQUFFO29CQUNqQixVQUFVLEVBQUssSUFBSSxDQUFDLGVBQWUsU0FBTTtvQkFDekMsT0FBTyxFQUFFO3dCQUNQLFVBQVUsRUFBRSxLQUFLO3FCQUNsQjtpQkFDRjthQUNGLENBQUM7WUFFRixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7WUFFaEQsTUFBTSxDQUFDLFFBQVEsQ0FBQzs7UUFDbEIsQ0FBQzs7O09BQUE7SUFLRCxzQ0FBaUIsR0FBakI7UUFDRSxNQUFNLENBQUM7WUFDTCxhQUFhLEVBQUU7Z0JBQ2IsNkNBQTZDLEVBQUUsQ0FBQyxVQUFVLENBQUM7YUFDNUQ7WUFDRCxTQUFTLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLHNCQUFzQixDQUFDO1lBQ3hELGdCQUFnQixFQUFFO2dCQUNoQixHQUFHLEVBQUUsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHO2dCQUM1QixTQUFTLEVBQUU7b0JBQ1QsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLHFCQUFxQixFQUFFO29CQUMxRCxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRTtpQkFDOUI7YUFDRjtZQUNELHFCQUFxQixFQUFFO2dCQUNyQixPQUFPLEVBQUU7b0JBQ1AsSUFBSSxFQUFFLElBQUksQ0FBQyxlQUFlO2lCQUMzQjthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUFPRCxnQ0FBVyxHQUFYLFVBQVksTUFBVyxFQUFFLE1BQVc7UUFDbEMsSUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQU1ELG9DQUFlLEdBQWYsVUFBZ0IsU0FBaUI7UUFDL0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDekMsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsZ0RBQTJCLEdBQTNCO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssbUJBQVcsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDekYsQ0FBQztJQUVELHNDQUFpQixHQUFqQixVQUFrQixJQUFvQjtRQUVwQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDcEQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUMxRCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUM5RCxJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ3BFLENBQUM7SUFDSCxDQUFDO0lBRUQsdUNBQWtCLEdBQWxCLFVBQW1CLEtBQXVCO1FBQTFDLGlCQU1DO1FBSkMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQW9CO1lBQ2pDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFLRCwwQ0FBcUIsR0FBckI7UUFDRSxJQUFJLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxVQUFBLFdBQVc7WUFDdkMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQzFELENBQUMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUN0QixDQUFDO0lBQ0gsaUJBQUM7QUFBRCxDQUFDLEFBeHFCRCxJQXdxQkM7QUF4cUJZLGdDQUFVO0FBOHFCdkIsK0JBQXNDLElBQTRCO0lBQ2hFLElBQUk7U0FDRCxNQUFNLENBQUMsVUFBQyxDQUF1QixJQUFLLE9BQUEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBakIsQ0FBaUIsQ0FBQztTQUN0RCxPQUFPLENBQUMsVUFBQyxDQUF1QixJQUFLLE9BQUEsQ0FBQyxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO0lBQ3hFLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZCxDQUFDO0FBTEQsc0RBS0M7QUFRRCwwQkFBMEIsSUFBWSxFQUFFLENBQXVCO0lBQzdELElBQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUMvQixDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztJQUNoQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLG1CQUFXLENBQUMsQ0FBQyxDQUFDLEVBQWQsQ0FBYyxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxZQUFZLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5QixDQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFDRCxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3hDLENBQUM7QUFNRDtJQUNFLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ3hDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO0FBQ3JCLENBQUM7QUFLRDtJQUNFLElBQUksSUFBSSxHQUFHLENBQUMsWUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLFlBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNuRSxJQUFJLElBQUksR0FBYSxZQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDL0IsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsT0FBTyxDQUFDLG1CQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUF0QyxDQUFzQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDbkYsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksV0FBVyxDQUFDLElBQUksSUFBSSxLQUFLLG1CQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUM3RCxNQUFNLENBQUMsbUJBQVcsQ0FBQyxVQUFVLENBQUM7SUFDaEMsQ0FBQztJQUFDLElBQUksQ0FBQyxDQUFDO1FBQ04sTUFBTSxDQUFDLG1CQUFXLENBQUMsV0FBVyxDQUFDO0lBQ2pDLENBQUM7QUFDSCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgam9pbiB9IGZyb20gJ3BhdGgnO1xuaW1wb3J0ICogYXMgc2xhc2ggZnJvbSAnc2xhc2gnO1xuaW1wb3J0IHsgYXJndiB9IGZyb20gJ3lhcmdzJztcblxuaW1wb3J0IHsgQnVpbGRUeXBlLCBFeHRlbmRQYWNrYWdlcywgSW5qZWN0YWJsZURlcGVuZGVuY3kgfSBmcm9tICcuL3NlZWQuY29uZmlnLmludGVyZmFjZXMnO1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKiBETyBOT1QgQ0hBTkdFICoqKioqKioqKioqKioqKioqKioqKioqKlxuICpcbiAqIERPIE5PVCBtYWtlIGFueSBjaGFuZ2VzIGluIHRoaXMgZmlsZSBiZWNhdXNlIGl0IHdpbGxcbiAqIG1ha2UgeW91ciBtaWdyYXRpb24gdG8gbmV3ZXIgdmVyc2lvbnMgb2YgdGhlIHNlZWQgaGFyZGVyLlxuICpcbiAqIFlvdXIgYXBwbGljYXRpb24tc3BlY2lmaWMgY29uZmlndXJhdGlvbnMgc2hvdWxkIGJlXG4gKiBpbiBwcm9qZWN0LmNvbmZpZy50cy4gSWYgeW91IG5lZWQgdG8gY2hhbmdlIGFueSB0YXNrc1xuICogZnJvbSBcIi4vdGFza3NcIiBvdmVyd3JpdGUgdGhlbSBieSBjcmVhdGluZyBhIHRhc2sgd2l0aCB0aGVcbiAqIHNhbWUgbmFtZSBpbiBcIi4vcHJvamVjdHNcIi4gRm9yIGZ1cnRoZXIgaW5mb3JtYXRpb24gdGFrZSBhXG4gKiBsb29rIGF0IHRoZSBkb2N1bWVudGF0aW9uOlxuICpcbiAqIDEpIGh0dHBzOi8vZ2l0aHViLmNvbS9tZ2VjaGV2L2FuZ3VsYXItc2VlZC90cmVlL21hc3Rlci90b29sc1xuICogMikgaHR0cHM6Ly9naXRodWIuY29tL21nZWNoZXYvYW5ndWxhci1zZWVkL3dpa2lcbiAqXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqIERPIE5PVCBDSEFOR0UgKioqKioqKioqKioqKioqKioqKioqKioqXG4gKlxuICogRE8gTk9UIG1ha2UgYW55IGNoYW5nZXMgaW4gdGhpcyBmaWxlIGJlY2F1c2UgaXQgd2lsbFxuICogbWFrZSB5b3VyIG1pZ3JhdGlvbiB0byBuZXdlciB2ZXJzaW9ucyBvZiB0aGUgc2VlZCBoYXJkZXIuXG4gKlxuICogWW91ciBhcHBsaWNhdGlvbi1zcGVjaWZpYyBjb25maWd1cmF0aW9ucyBzaG91bGQgYmVcbiAqIGluIHByb2plY3QuY29uZmlnLnRzLiBJZiB5b3UgbmVlZCB0byBjaGFuZ2UgYW55IHRhc2tzXG4gKiBmcm9tIFwiLi90YXNrc1wiIG92ZXJ3cml0ZSB0aGVtIGJ5IGNyZWF0aW5nIGEgdGFzayB3aXRoIHRoZVxuICogc2FtZSBuYW1lIGluIFwiLi9wcm9qZWN0c1wiLiBGb3IgZnVydGhlciBpbmZvcm1hdGlvbiB0YWtlIGFcbiAqIGxvb2sgYXQgdGhlIGRvY3VtZW50YXRpb246XG4gKlxuICogMSkgaHR0cHM6Ly9naXRodWIuY29tL21nZWNoZXYvYW5ndWxhci1zZWVkL3RyZWUvbWFzdGVyL3Rvb2xzXG4gKiAyKSBodHRwczovL2dpdGh1Yi5jb20vbWdlY2hldi9hbmd1bGFyLXNlZWQvd2lraVxuICpcbiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuLyoqXG4gKiBUaGUgZW51bWVyYXRpb24gb2YgYXZhaWxhYmxlIGVudmlyb25tZW50cy5cbiAqIEB0eXBlIHtFbnZpcm9ubWVudHN9XG4gKi9cbmV4cG9ydCBjb25zdCBCVUlMRF9UWVBFUzogQnVpbGRUeXBlID0ge1xuICBERVZFTE9QTUVOVDogJ2RldicsXG4gIFBST0RVQ1RJT046ICdwcm9kJ1xufTtcblxuLyoqXG4gKiBUaGlzIGNsYXNzIHJlcHJlc2VudHMgdGhlIGJhc2ljIGNvbmZpZ3VyYXRpb24gb2YgdGhlIHNlZWQuXG4gKiBJdCBwcm92aWRlcyB0aGUgZm9sbG93aW5nOlxuICogLSBDb25zdGFudHMgZm9yIGRpcmVjdG9yaWVzLCBwb3J0cywgdmVyc2lvbnMgZXRjLlxuICogLSBJbmplY3RhYmxlIE5QTSBkZXBlbmRlbmNpZXNcbiAqIC0gSW5qZWN0YWJsZSBhcHBsaWNhdGlvbiBhc3NldHNcbiAqIC0gVGVtcG9yYXJ5IGVkaXRvciBmaWxlcyB0byBiZSBpZ25vcmVkIGJ5IHRoZSB3YXRjaGVyIGFuZCBhc3NldCBidWlsZGVyXG4gKiAtIFN5c3RlbUpTIGNvbmZpZ3VyYXRpb25cbiAqIC0gQXV0b3ByZWZpeGVyIGNvbmZpZ3VyYXRpb25cbiAqIC0gQnJvd3NlclN5bmMgY29uZmlndXJhdGlvblxuICogLSBVdGlsaXRpZXNcbiAqL1xuZXhwb3J0IGNsYXNzIFNlZWRDb25maWcge1xuXG4gIC8qKlxuICAgKiBUaGUgcG9ydCB3aGVyZSB0aGUgYXBwbGljYXRpb24gd2lsbCBydW4uXG4gICAqIFRoZSBkZWZhdWx0IHBvcnQgaXMgYDU1NTVgLCB3aGljaCBjYW4gYmUgb3ZlcnJpZGVuIGJ5IHRoZSAgYC0tcG9ydGAgZmxhZyB3aGVuIHJ1bm5pbmcgYG5wbSBzdGFydGAuXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBQT1JUID0gYXJndlsncG9ydCddIHx8IDU1NTU7XG5cbiAgLyoqXG4gICAqIFRoZSByb290IGZvbGRlciBvZiB0aGUgcHJvamVjdCAodXAgdHdvIGxldmVscyBmcm9tIHRoZSBjdXJyZW50IGRpcmVjdG9yeSkuXG4gICAqL1xuICBQUk9KRUNUX1JPT1QgPSBqb2luKF9fZGlybmFtZSwgJy4uLy4uJyk7XG5cbiAgLyoqXG4gICAqIFRoZSBjdXJyZW50IGJ1aWxkIHR5cGUuXG4gICAqIFRoZSBkZWZhdWx0IGJ1aWxkIHR5cGUgaXMgYGRldmAsIHdoaWNoIGNhbiBiZSBvdmVycmlkZW4gYnkgdGhlIGAtLWJ1aWxkLXR5cGUgZGV2fHByb2RgIGZsYWcgd2hlbiBydW5uaW5nIGBucG0gc3RhcnRgLlxuICAgKi9cbiAgQlVJTERfVFlQRSA9IGdldEJ1aWxkVHlwZSgpO1xuXG4gIC8qKlxuICAgKiBUaGUgZmxhZyBmb3IgdGhlIGRlYnVnIG9wdGlvbiBvZiB0aGUgYXBwbGljYXRpb24uXG4gICAqIFRoZSBkZWZhdWx0IHZhbHVlIGlzIGBmYWxzZWAsIHdoaWNoIGNhbiBiZSBvdmVycmlkZW4gYnkgdGhlIGAtLWRlYnVnYCBmbGFnIHdoZW4gcnVubmluZyBgbnBtIHN0YXJ0YC5cbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqL1xuICBERUJVRyA9IGFyZ3ZbJ2RlYnVnJ10gfHwgZmFsc2U7XG5cbiAgLyoqXG4gICAqIFRoZSBwb3J0IHdoZXJlIHRoZSBkb2N1bWVudGF0aW9uIGFwcGxpY2F0aW9uIHdpbGwgcnVuLlxuICAgKiBUaGUgZGVmYXVsdCBkb2NzIHBvcnQgaXMgYDQwMDNgLCB3aGljaCBjYW4gYmUgb3ZlcnJpZGVuIGJ5IHRoZSBgLS1kb2NzLXBvcnRgIGZsYWcgd2hlbiBydW5uaW5nIGBucG0gc3RhcnRgLlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgRE9DU19QT1JUID0gYXJndlsnZG9jcy1wb3J0J10gfHwgNDAwMztcblxuICAvKipcbiAgICogVGhlIHBvcnQgd2hlcmUgdGhlIHVuaXQgdGVzdCBjb3ZlcmFnZSByZXBvcnQgYXBwbGljYXRpb24gd2lsbCBydW4uXG4gICAqIFRoZSBkZWZhdWx0IGNvdmVyYWdlIHBvcnQgaXMgYDQwMDRgLCB3aGljaCBjYW4gYnkgb3ZlcnJpZGVuIGJ5IHRoZSBgLS1jb3ZlcmFnZS1wb3J0YCBmbGFnIHdoZW4gcnVubmluZyBgbnBtIHN0YXJ0YC5cbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIENPVkVSQUdFX1BPUlQgPSBhcmd2Wydjb3ZlcmFnZS1wb3J0J10gfHwgNDAwNDtcblxuICAvKipcbiAgKiBUaGUgcGF0aCB0byB0aGUgY292ZXJhZ2Ugb3V0cHV0XG4gICogTkI6IHRoaXMgbXVzdCBtYXRjaCB3aGF0IGlzIGNvbmZpZ3VyZWQgaW4gLi9rYXJtYS5jb25mLmpzXG4gICovXG4gIENPVkVSQUdFX0RJUiA9ICdjb3ZlcmFnZV9qcyc7XG4gIENPVkVSQUdFX1RTX0RJUiA9ICdjb3ZlcmFnZSc7XG5cbiAgLyoqXG4gICAqIFRoZSBwYXRoIGZvciB0aGUgYmFzZSBvZiB0aGUgYXBwbGljYXRpb24gYXQgcnVudGltZS5cbiAgICogVGhlIGRlZmF1bHQgcGF0aCBpcyBiYXNlZCBvbiB0aGUgZW52aXJvbm1lbnQgJy8nLFxuICAgKiB3aGljaCBjYW4gYmUgb3ZlcnJpZGVuIGJ5IHRoZSBgLS1iYXNlYCBmbGFnIHdoZW4gcnVubmluZyBgbnBtIHN0YXJ0YC5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIEFQUF9CQVNFID0gYXJndlsnYmFzZSddIHx8ICcvJztcblxuICAvKipcbiAgICogVGhlIGJhc2UgcGF0aCBvZiBub2RlIG1vZHVsZXMuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBOUE1fQkFTRSA9IHNsYXNoKGpvaW4oJy4nLCB0aGlzLkFQUF9CQVNFLCAnbm9kZV9tb2R1bGVzLycpKTtcblxuICAvKipcbiAgICogVGhlIGZsYWcgZm9yIHRoZSB0YXJnZXRpbmcgb2YgbW9iaWxlIGh5YnJpZCBvcHRpb24gb2YgdGhlIGFwcGxpY2F0aW9uLlxuICAgKiBQZXIgZGVmYXVsdCB0aGUgb3B0aW9uIGlzIGZhbHNlIGFuZCBub3QgY3VycmVudGx5IHN1cHBvcnRlZCBidXQgbWF5IGJlIGluIHRoZSBmdXR1cmUuXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cbiAgVEFSR0VUX01PQklMRV9IWUJSSUQgPSBmYWxzZTtcblxuICAvKipcbiAgICogVGhlIGZsYWcgZm9yIHRoZSB0YXJnZXRpbmcgb2YgZGVza3RvcCBvcHRpb24gb2YgdGhlIGFwcGxpY2F0aW9uLlxuICAgKiBQZXIgZGVmYXVsdCB0aGUgb3B0aW9uIGlzIGZhbHNlLlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICovXG4gIFRBUkdFVF9ERVNLVE9QID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIFRoZSBmbGFnIGZvciB0aGUgdGFyZ2V0aW5nIG9mIGRlc2t0b3AgYnVpbGQgb3B0aW9uIG9mIHRoZSBhcHBsaWNhdGlvbi5cbiAgICogUGVyIGRlZmF1bHQgdGhlIG9wdGlvbiBpcyBmYWxzZS5cbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqL1xuICBUQVJHRVRfREVTS1RPUF9CVUlMRCA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBUaGUgYnVpbGQgaW50ZXJ2YWwgd2hpY2ggd2lsbCBmb3JjZSB0aGUgVHlwZVNjcmlwdCBjb21waWxlciB0byBwZXJmb3JtIGEgdHlwZWQgY29tcGlsZSBydW4uXG4gICAqIEJldHdlZW4gdGhlIHR5cGVkIHJ1bnMsIGEgdHlwZWxlc3MgY29tcGlsZSBpcyBydW4sIHdoaWNoIGlzIHR5cGljYWxseSBtdWNoIGZhc3Rlci5cbiAgICogRm9yIGV4YW1wbGUsIGlmIHNldCB0byA1LCB0aGUgaW5pdGlhbCBjb21waWxlIHdpbGwgYmUgdHlwZWQsIGZvbGxvd2VkIGJ5IDUgdHlwZWxlc3MgcnVucyxcbiAgICogdGhlbiBhbm90aGVyIHR5cGVkIHJ1biwgYW5kIHNvIG9uLlxuICAgKiBJZiBhIGNvbXBpbGUgZXJyb3IgaXMgZW5jb3VudGVyZWQsIHRoZSBidWlsZCB3aWxsIHVzZSB0eXBlZCBjb21waWxhdGlvbiB1bnRpbCB0aGUgZXJyb3IgaXMgcmVzb2x2ZWQuXG4gICAqIFRoZSBkZWZhdWx0IHZhbHVlIGlzIGAwYCwgbWVhbmluZyB0eXBlZCBjb21waWxhdGlvbiB3aWxsIGFsd2F5cyBiZSBwZXJmb3JtZWQuXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBUWVBFRF9DT01QSUxFX0lOVEVSVkFMID0gMDtcblxuICAvKipcbiAgICogVGhlIGRpcmVjdG9yeSB3aGVyZSB0aGUgYm9vdHN0cmFwIGZpbGUgaXMgbG9jYXRlZC5cbiAgICogVGhlIGRlZmF1bHQgZGlyZWN0b3J5IGlzIGBhcHBgLlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgQk9PVFNUUkFQX0RJUiA9IGFyZ3ZbJ2FwcCddIHx8ICdhcHAnO1xuXG4gIC8qKlxuICAgKiBUaGUgZGlyZWN0b3J5IHdoZXJlIHRoZSBjbGllbnQgZmlsZXMgYXJlIGxvY2F0ZWQuXG4gICAqIFRoZSBkZWZhdWx0IGRpcmVjdG9yeSBpcyBgY2xpZW50YC5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIEFQUF9DTElFTlQgPSBhcmd2WydjbGllbnQnXSB8fCAnY2xpZW50JztcblxuICAvKipcbiAgICogVGhlIGJvb3RzdHJhcCBmaWxlIHRvIGJlIHVzZWQgdG8gYm9vdCB0aGUgYXBwbGljYXRpb24uXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBCT09UU1RSQVBfTU9EVUxFID0gYCR7dGhpcy5CT09UU1RSQVBfRElSfS9tYWluYDtcblxuICBCT09UU1RSQVBfUFJPRF9NT0RVTEUgPSBgJHt0aGlzLkJPT1RTVFJBUF9ESVJ9L2AgKyAnbWFpbic7XG5cbiAgTkdfRkFDVE9SWV9GSUxFID0gJ21haW4tcHJvZCc7XG5cbiAgQk9PVFNUUkFQX0ZBQ1RPUllfUFJPRF9NT0RVTEUgPSBgJHt0aGlzLkJPT1RTVFJBUF9ESVJ9LyR7dGhpcy5OR19GQUNUT1JZX0ZJTEV9YDtcbiAgLyoqXG4gICAqIFRoZSBkZWZhdWx0IHRpdGxlIG9mIHRoZSBhcHBsaWNhdGlvbiBhcyB1c2VkIGluIHRoZSBgPHRpdGxlPmAgdGFnIG9mIHRoZVxuICAgKiBgaW5kZXguaHRtbGAuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBBUFBfVElUTEUgPSAnV2VsY29tZSB0byBhbmd1bGFyLXNlZWQhJztcblxuICAvKipcbiAgICogVHJhY2tpbmcgSUQuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBHT09HTEVfQU5BTFlUSUNTX0lEID0gJ1VBLVhYWFhYWFhYLVgnO1xuXG4gIC8qKlxuICAgKiBUaGUgYmFzZSBmb2xkZXIgb2YgdGhlIGFwcGxpY2F0aW9ucyBzb3VyY2UgZmlsZXMuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBBUFBfU1JDID0gYHNyYy8ke3RoaXMuQVBQX0NMSUVOVH1gO1xuXG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgVHlwZVNjcmlwdCBwcm9qZWN0IGZpbGVcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIEFQUF9QUk9KRUNUTkFNRSA9ICd0c2NvbmZpZy5qc29uJztcblxuICAvKipcbiAgICogVGhlIGZvbGRlciBvZiB0aGUgYXBwbGljYXRpb25zIGFzc2V0IGZpbGVzLlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgQVNTRVRTX1NSQyA9IGAke3RoaXMuQVBQX1NSQ30vYXNzZXRzYDtcblxuICAvKipcbiAgICogVGhlIGZvbGRlciBvZiB0aGUgYXBwbGljYXRpb25zIGNzcyBmaWxlcy5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIENTU19TUkMgPSBgJHt0aGlzLkFQUF9TUkN9L2Nzc2A7XG5cbiAgLyoqXG4gICAqIFRoZSBmb2xkZXIgb2YgdGhlIGUyZSBzcGVjcyBhbmQgZnJhbWV3b3JrXG4gICAqL1xuICBFMkVfU1JDID0gJ3NyYy9lMmUnO1xuXG4gIC8qKlxuICAgKiBUaGUgZm9sZGVyIG9mIHRoZSBhcHBsaWNhdGlvbnMgc2NzcyBmaWxlcy5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIFNDU1NfU1JDID0gYCR7dGhpcy5BUFBfU1JDfS9zY3NzYDtcblxuICAvKipcbiAgICogVGhlIGRpcmVjdG9yeSBvZiB0aGUgYXBwbGljYXRpb25zIHRvb2xzXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBUT09MU19ESVIgPSAndG9vbHMnO1xuXG4gIC8qKlxuICAgKiBUaGUgZGlyZWN0b3J5IG9mIHRoZSB0YXNrcyBwcm92aWRlZCBieSB0aGUgc2VlZC5cbiAgICovXG4gIFNFRURfVEFTS1NfRElSID0gam9pbihwcm9jZXNzLmN3ZCgpLCB0aGlzLlRPT0xTX0RJUiwgJ3Rhc2tzJywgJ3NlZWQnKTtcblxuICAvKipcbiAgICogU2VlZCB0YXNrcyB3aGljaCBhcmUgY29tcG9zaXRpb24gb2Ygb3RoZXIgdGFza3MuXG4gICAqL1xuICBTRUVEX0NPTVBPU0lURV9UQVNLUyA9IGpvaW4ocHJvY2Vzcy5jd2QoKSwgdGhpcy5UT09MU19ESVIsICdjb25maWcnLCAnc2VlZC50YXNrcy5qc29uJyk7XG5cbiAgLyoqXG4gICAqIFByb2plY3QgdGFza3Mgd2hpY2ggYXJlIGNvbXBvc2l0aW9uIG9mIG90aGVyIHRhc2tzXG4gICAqIGFuZCBhaW0gdG8gb3ZlcnJpZGUgdGhlIHRhc2tzIGRlZmluZWQgaW5cbiAgICogU0VFRF9DT01QT1NJVEVfVEFTS1MuXG4gICAqL1xuICBQUk9KRUNUX0NPTVBPU0lURV9UQVNLUyA9IGpvaW4ocHJvY2Vzcy5jd2QoKSwgdGhpcy5UT09MU19ESVIsICdjb25maWcnLCAncHJvamVjdC50YXNrcy5qc29uJyk7XG5cbiAgLyoqXG4gICAqIFRoZSBkZXN0aW5hdGlvbiBmb2xkZXIgZm9yIHRoZSBnZW5lcmF0ZWQgZG9jdW1lbnRhdGlvbi5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIERPQ1NfREVTVCA9ICdkb2NzJztcblxuICAvKipcbiAgICogVGhlIGJhc2UgZm9sZGVyIGZvciBidWlsdCBmaWxlcy5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIERJU1RfRElSID0gJ2Rpc3QnO1xuXG4gIC8qKlxuICAgKiBUaGUgZm9sZGVyIGZvciBidWlsdCBmaWxlcyBpbiB0aGUgYGRldmAgZW52aXJvbm1lbnQuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBERVZfREVTVCA9IGAke3RoaXMuRElTVF9ESVJ9L2RldmA7XG5cbiAgLyoqXG4gICAqIFRoZSBmb2xkZXIgZm9yIHRoZSBidWlsdCBmaWxlcyBpbiB0aGUgYHByb2RgIGVudmlyb25tZW50LlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgUFJPRF9ERVNUID0gYCR7dGhpcy5ESVNUX0RJUn0vcHJvZGA7XG5cbiAgLyoqXG4gICAqIFRoZSBmb2xkZXIgZm9yIHRoZSBidWlsdCBmaWxlcyBvZiB0aGUgZTJlLXNwZWNzLlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgRTJFX0RFU1QgPSBgJHt0aGlzLkRJU1RfRElSfS9lMmVgO1xuXG4gIC8qKlxuICAgKiBUaGUgZm9sZGVyIGZvciB0ZW1wb3JhcnkgZmlsZXMuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBUTVBfRElSID0gYCR7dGhpcy5ESVNUX0RJUn0vdG1wYDtcblxuICAvKipcbiAgICogVGhlIGZvbGRlciBmb3IgdGhlIGJ1aWx0IGZpbGVzLCBjb3JyZXNwb25kaW5nIHRvIHRoZSBjdXJyZW50IGVudmlyb25tZW50LlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgQVBQX0RFU1QgPSB0aGlzLkJVSUxEX1RZUEUgPT09IEJVSUxEX1RZUEVTLkRFVkVMT1BNRU5UID8gdGhpcy5ERVZfREVTVCA6IHRoaXMuUFJPRF9ERVNUO1xuXG4gIC8qKlxuICAgKiBUaGUgZm9sZGVyIGZvciB0aGUgYnVpbHQgQ1NTIGZpbGVzLlxuICAgKiBAdHlwZSB7c3RyaW5nc31cbiAgICovXG4gIENTU19ERVNUID0gYCR7dGhpcy5BUFBfREVTVH0vY3NzYDtcblxuICAvKipcbiAgICogVGhlIGZvbGRlciBmb3IgdGhlIGJ1aWx0IEphdmFTY3JpcHQgZmlsZXMuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBKU19ERVNUID0gYCR7dGhpcy5BUFBfREVTVH0vanNgO1xuXG4gIC8qKlxuICAgKiBUaGUgdmVyc2lvbiBvZiB0aGUgYXBwbGljYXRpb24gYXMgZGVmaW5lZCBpbiB0aGUgYHBhY2thZ2UuanNvbmAuXG4gICAqL1xuICBWRVJTSU9OID0gYXBwVmVyc2lvbigpO1xuXG4gIC8qKlxuICAgKiBUaGUgYmFubmVyIHN0cmluZyB0byBwcmludCB3aGVuIHN0YXJ0aW5nIHRoZSBzZXJ2ZXIuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBTRVJWRVJfU1RBUlRVUF9CQU5ORVIgPSAnU3RhcnRpbmcgdGhlIHNlcnZlci4uJztcblxuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIGJ1bmRsZSBmaWxlIHRvIGluY2x1ZGVzIGFsbCBDU1MgZmlsZXMuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBDU1NfQlVORExFX05BTUUgPSAnbWFpbic7XG5cbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBidW5kbGUgZmlsZSB0byBpbmNsdWRlIGFsbCBKYXZhU2NyaXB0IHNoaW1zLlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgSlNfUFJPRF9TSElNU19CVU5ETEUgPSAnc2hpbXMuanMnO1xuXG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgYnVuZGxlIGZpbGUgdG8gaW5jbHVkZSBhbGwgSmF2YVNjcmlwdCBhcHBsaWNhdGlvbiBmaWxlcy5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIEpTX1BST0RfQVBQX0JVTkRMRSA9ICdhcHAuanMnO1xuXG4gIC8qKlxuICAgKiBUaGUgcmVxdWlyZWQgTlBNIHZlcnNpb24gdG8gcnVuIHRoZSBhcHBsaWNhdGlvbi5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIFZFUlNJT05fTlBNID0gJzMuMC4wJztcblxuICAvKipcbiAgICogVGhlIHJlcXVpcmVkIE5vZGVKUyB2ZXJzaW9uIHRvIHJ1biB0aGUgYXBwbGljYXRpb24uXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBWRVJTSU9OX05PREUgPSAnNS4wLjAnO1xuXG4gIC8qKlxuICAgKiBFbmFibGUgU0NTUyBzdHlsZXNoZWV0IGNvbXBpbGF0aW9uLlxuICAgKiBTZXQgRU5BQkxFX1NDU1MgZW52aXJvbm1lbnQgdmFyaWFibGUgdG8gJ3RydWUnIG9yICcxJ1xuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICovXG4gIEVOQUJMRV9TQ1NTID0gWyd0cnVlJywgJzEnXS5pbmRleE9mKGAke3Byb2Nlc3MuZW52LkVOQUJMRV9TQ1NTfWAudG9Mb3dlckNhc2UoKSkgIT09IC0xIHx8IGFyZ3ZbJ3Njc3MnXSB8fCBmYWxzZTtcblxuICAvKipcbiAgICogRW5hYmxlIHRzbGludCBlbWl0IGVycm9yIGJ5IHNldHRpbmcgZW52IHZhcmlhYmxlIEZPUkNFX1RTTElOVF9FTUlUX0VSUk9SXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cbiAgRk9SQ0VfVFNMSU5UX0VNSVRfRVJST1IgPSAhIXByb2Nlc3MuZW52LkZPUkNFX1RTTElOVF9FTUlUX0VSUk9SO1xuXG4gIC8qKlxuICAgKiBFeHRyYSBwYXRocyBmb3IgdGhlIGd1bHAgcHJvY2VzcyB0byB3YXRjaCBmb3IgdG8gdHJpZ2dlciBjb21waWxhdGlvbi5cbiAgICogQHR5cGUge3N0cmluZ1tdfVxuICAgKi9cbiAgRVhUUkFfV0FUQ0hfUEFUSFM6IHN0cmluZ1tdID0gW107XG5cbiAgLyoqXG4gICAqIFRoZSBsaXN0IG9mIE5QTSBkZXBlbmRjaWVzIHRvIGJlIGluamVjdGVkIGluIHRoZSBgaW5kZXguaHRtbGAuXG4gICAqIEB0eXBlIHtJbmplY3RhYmxlRGVwZW5kZW5jeVtdfVxuICAgKi9cbiAgTlBNX0RFUEVOREVOQ0lFUzogSW5qZWN0YWJsZURlcGVuZGVuY3lbXSA9IFtcbiAgICB7IHNyYzogJ2NvcmUtanMvY2xpZW50L3NoaW0ubWluLmpzJywgaW5qZWN0OiAnc2hpbXMnIH0sXG4gICAgeyBzcmM6ICd6b25lLmpzL2Rpc3Qvem9uZS5qcycsIGluamVjdDogJ2xpYnMnIH0sXG4gICAgeyBzcmM6ICd6b25lLmpzL2Rpc3QvbG9uZy1zdGFjay10cmFjZS16b25lLmpzJywgaW5qZWN0OiAnbGlicycsIGJ1aWxkVHlwZTogQlVJTERfVFlQRVMuREVWRUxPUE1FTlQgfSxcbiAgICB7IHNyYzogJ2ludGwvZGlzdC9JbnRsLm1pbi5qcycsIGluamVjdDogJ3NoaW1zJyB9LFxuICAgIHsgc3JjOiAnc3lzdGVtanMvZGlzdC9zeXN0ZW0uc3JjLmpzJywgaW5qZWN0OiAnc2hpbXMnLCBidWlsZFR5cGU6IEJVSUxEX1RZUEVTLkRFVkVMT1BNRU5UIH0sXG4gICAgLy8gVGVtcG9yYXJ5IGZpeC4gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2FuZ3VsYXIvaXNzdWVzLzkzNTlcbiAgICB7IHNyYzogJy50bXAvUngubWluLmpzJywgaW5qZWN0OiAnbGlicycsIGJ1aWxkVHlwZTogQlVJTERfVFlQRVMuREVWRUxPUE1FTlQgfSxcbiAgXTtcblxuICAvKipcbiAgICogVGhlIGxpc3Qgb2YgbG9jYWwgZmlsZXMgdG8gYmUgaW5qZWN0ZWQgaW4gdGhlIGBpbmRleC5odG1sYC5cbiAgICogQHR5cGUge0luamVjdGFibGVEZXBlbmRlbmN5W119XG4gICAqL1xuICBBUFBfQVNTRVRTOiBJbmplY3RhYmxlRGVwZW5kZW5jeVtdID0gW107XG5cbiAgLyoqXG4gICAqIFRoZSBsaXN0IG9mIGVkaXRvciB0ZW1wb3JhcnkgZmlsZXMgdG8gaWdub3JlIGluIHdhdGNoZXIgYW5kIGFzc2V0IGJ1aWxkZXIuXG4gICAqIEB0eXBlIHtzdHJpbmdbXX1cbiAgICovXG4gIFRFTVBfRklMRVM6IHN0cmluZ1tdID0gW1xuICAgICcqKi8qX19famJfdG1wX19fJyxcbiAgICAnKiovKn4nLFxuXG4gICAgLy8gdG5zLWZpbGVzIGFyZSBpZ25vcmVkIGFzd2VsbFxuICAgICcqKi8qLnRucy5zY3NzJyxcbiAgICAnKiovKi50bnMuY3NzJyxcbiAgICAnKiovKi50bnMuaHRtbCcsXG4gICAgJ3NyYy8qKi8qLmpzJyxcbiAgICAnc3JjLyoqLyouanMubWFwJyxcbiAgXTtcblxuICAvKipcbiAgICogTGlzdCBvZiBkaXJlY3RvcmllcyB0byBpbmNsdWRlIGluIGNvbW1vbmpzXG4gICAqIEB0eXBlIHtzdHJpbmdbXX1cbiAgICovXG4gIFJPTExVUF9JTkNMVURFX0RJUjogc3RyaW5nW10gPSBbJ25vZGVfbW9kdWxlcy8qKiddO1xuXG4gLyoqXG4gICogTGlzdCBvZiBuYW1lZCBleHBvcnQgT2JqZWN0IGtleSB2YWx1ZSBwYWlyc1xuICAqIGtleTogZGVwZW5kZW5jaWUgZmlsZVxuICAqIHZhbHVlOiBleHBvcnRlZCBPYmplY3RzXG4gICovXG4gIFJPTExVUF9OQU1FRF9FWFBPUlRTOiBhbnlbXSA9IFtdO1xuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBhcnJheSBvZiBpbmplY3RhYmxlIGRlcGVuZGVuY2llcyAobnBtIGRlcGVuZGVuY2llcyBhbmQgYXNzZXRzKS5cbiAgICogQHJldHVybiB7SW5qZWN0YWJsZURlcGVuZGVuY3lbXX0gVGhlIGFycmF5IG9mIG5wbSBkZXBlbmRlbmNpZXMgYW5kIGFzc2V0cy5cbiAgICovXG4gIGdldCBERVBFTkRFTkNJRVMoKTogSW5qZWN0YWJsZURlcGVuZGVuY3lbXSB7XG4gICAgcmV0dXJuIG5vcm1hbGl6ZURlcGVuZGVuY2llcyh0aGlzLk5QTV9ERVBFTkRFTkNJRVMuZmlsdGVyKGZpbHRlckRlcGVuZGVuY3kuYmluZChudWxsLCB0aGlzLkJVSUxEX1RZUEUpKSlcbiAgICAgIC5jb25jYXQodGhpcy5fQVBQX0FTU0VUUy5maWx0ZXIoZmlsdGVyRGVwZW5kZW5jeS5iaW5kKG51bGwsIHRoaXMuQlVJTERfVFlQRSkpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGUgY29uZmlndXJhdGlvbiBvZiBTeXN0ZW1KUyBmb3IgdGhlIGBkZXZgIGVudmlyb25tZW50LlxuICAgKiBAdHlwZSB7YW55fVxuICAgKi9cbiAgU1lTVEVNX0NPTkZJR19ERVY6IGFueSA9IHtcbiAgICBkZWZhdWx0SlNFeHRlbnNpb25zOiB0cnVlLFxuICAgIHBhdGhzOiB7XG4gICAgICBbdGhpcy5CT09UU1RSQVBfTU9EVUxFXTogYCR7dGhpcy5BUFBfQkFTRX0ke3RoaXMuQk9PVFNUUkFQX01PRFVMRX1gLFxuICAgICAgJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnOiAnbm9kZV9tb2R1bGVzL0Bhbmd1bGFyL2FuaW1hdGlvbnMvYnVuZGxlcy9hbmltYXRpb25zLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci9hbmltYXRpb25zJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyL2J1bmRsZXMvcGxhdGZvcm0tYnJvd3Nlci1hbmltYXRpb25zLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvY29tbW9uJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9jb21tb24vYnVuZGxlcy9jb21tb24udW1kLmpzJyxcbiAgICAgICdAYW5ndWxhci9jb21waWxlcic6ICdub2RlX21vZHVsZXMvQGFuZ3VsYXIvY29tcGlsZXIvYnVuZGxlcy9jb21waWxlci51bWQuanMnLFxuICAgICAgJ0Bhbmd1bGFyL2NvcmUnOiAnbm9kZV9tb2R1bGVzL0Bhbmd1bGFyL2NvcmUvYnVuZGxlcy9jb3JlLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvZm9ybXMnOiAnbm9kZV9tb2R1bGVzL0Bhbmd1bGFyL2Zvcm1zL2J1bmRsZXMvZm9ybXMudW1kLmpzJyxcbiAgICAgICdAYW5ndWxhci9odHRwJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9odHRwL2J1bmRsZXMvaHR0cC51bWQuanMnLFxuICAgICAgJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInOiAnbm9kZV9tb2R1bGVzL0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXIvYnVuZGxlcy9wbGF0Zm9ybS1icm93c2VyLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci1keW5hbWljJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyLWR5bmFtaWMvYnVuZGxlcy9wbGF0Zm9ybS1icm93c2VyLWR5bmFtaWMudW1kLmpzJyxcbiAgICAgICdAYW5ndWxhci9yb3V0ZXInOiAnbm9kZV9tb2R1bGVzL0Bhbmd1bGFyL3JvdXRlci9idW5kbGVzL3JvdXRlci51bWQuanMnLFxuICAgICAgJ0Bhbmd1bGFyL2FuaW1hdGlvbnMvYnJvd3Nlcic6ICdub2RlX21vZHVsZXMvQGFuZ3VsYXIvYW5pbWF0aW9ucy9idW5kbGVzL2FuaW1hdGlvbnMtYnJvd3Nlci51bWQuanMnLFxuXG4gICAgICAnQGFuZ3VsYXIvY29tbW9uL3Rlc3RpbmcnOiAnbm9kZV9tb2R1bGVzL0Bhbmd1bGFyL2NvbW1vbi9idW5kbGVzL2NvbW1vbi10ZXN0aW5nLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvY29tcGlsZXIvdGVzdGluZyc6ICdub2RlX21vZHVsZXMvQGFuZ3VsYXIvY29tcGlsZXIvYnVuZGxlcy9jb21waWxlci10ZXN0aW5nLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9jb3JlL2J1bmRsZXMvY29yZS10ZXN0aW5nLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvaHR0cC90ZXN0aW5nJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9odHRwL2J1bmRsZXMvaHR0cC10ZXN0aW5nLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci90ZXN0aW5nJzpcbiAgICAgICdub2RlX21vZHVsZXMvQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci9idW5kbGVzL3BsYXRmb3JtLWJyb3dzZXItdGVzdGluZy51bWQuanMnLFxuICAgICAgJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXItZHluYW1pYy90ZXN0aW5nJzpcbiAgICAgICdub2RlX21vZHVsZXMvQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci1keW5hbWljL2J1bmRsZXMvcGxhdGZvcm0tYnJvd3Nlci1keW5hbWljLXRlc3RpbmcudW1kLmpzJyxcbiAgICAgICdAYW5ndWxhci9yb3V0ZXIvdGVzdGluZyc6ICdub2RlX21vZHVsZXMvQGFuZ3VsYXIvcm91dGVyL2J1bmRsZXMvcm91dGVyLXRlc3RpbmcudW1kLmpzJyxcblxuICAgICAgJ2FwcC8qJzogJy9hcHAvKicsXG4gICAgICAvLyBGb3IgdGVzdCBjb25maWdcbiAgICAgICdkaXN0L2Rldi8qJzogJy9iYXNlL2Rpc3QvZGV2LyonLFxuICAgICAgJyonOiAnbm9kZV9tb2R1bGVzLyonXG4gICAgfSxcbiAgICBwYWNrYWdlczoge1xuICAgIH1cbiAgfTtcblxuICAvKipcbiAgICogVGhlIGNvbmZpZ3VyYXRpb24gb2YgU3lzdGVtSlMgb2YgdGhlIGFwcGxpY2F0aW9uLlxuICAgKiBQZXIgZGVmYXVsdCwgdGhlIGNvbmZpZ3VyYXRpb24gb2YgdGhlIGBkZXZgIGVudmlyb25tZW50IHdpbGwgYmUgdXNlZC5cbiAgICogQHR5cGUge2FueX1cbiAgICovXG4gIFNZU1RFTV9DT05GSUc6IGFueSA9IHRoaXMuU1lTVEVNX0NPTkZJR19ERVY7XG5cbiAgLyoqXG4gICAqIFRoZSBzeXN0ZW0gYnVpbGRlciBjb25maWd1cmF0aW9uIG9mIHRoZSBhcHBsaWNhdGlvbi5cbiAgICogQHR5cGUge2FueX1cbiAgICovXG4gIFNZU1RFTV9CVUlMREVSX0NPTkZJRzogYW55ID0ge1xuICAgIGRlZmF1bHRKU0V4dGVuc2lvbnM6IHRydWUsXG4gICAgYmFzZTogdGhpcy5QUk9KRUNUX1JPT1QsXG4gICAgcGFja2FnZUNvbmZpZ1BhdGhzOiBbXG4gICAgICBqb2luKCdub2RlX21vZHVsZXMnLCAnKicsICdwYWNrYWdlLmpzb24nKSxcbiAgICAgIGpvaW4oJ25vZGVfbW9kdWxlcycsICdAYW5ndWxhcicsICcqJywgJ3BhY2thZ2UuanNvbicpXG4gICAgICAvLyBmb3Igb3RoZXIgbW9kdWxlcyBsaWtlIEBuZ3gtdHJhbnNsYXRlIHRoZSBwYWNrYWdlLmpzb24gcGF0aCBuZWVkcyB0byB1cGRhdGVkIGhlcmVcbiAgICAgIC8vIG90aGVyd2lzZSBucG0gcnVuIGJ1aWxkLnByb2Qgd291bGQgZmFpbFxuICAgICAgLy8gam9pbignbm9kZV9tb2R1bGVzJywgJ0BuZ3gtdHJhbnNsYXRlJywgJyonLCAncGFja2FnZS5qc29uJylcbiAgICBdLFxuICAgIHBhdGhzOiB7XG4gICAgICAvLyBOb3RlIHRoYXQgZm9yIG11bHRpcGxlIGFwcHMgdGhpcyBjb25maWd1cmF0aW9uIG5lZWQgdG8gYmUgdXBkYXRlZFxuICAgICAgLy8gWW91IHdpbGwgaGF2ZSB0byBpbmNsdWRlIGVudHJpZXMgZm9yIGVhY2ggaW5kaXZpZHVhbCBhcHBsaWNhdGlvbiBpblxuICAgICAgLy8gYHNyYy9jbGllbnRgLlxuICAgICAgW2pvaW4odGhpcy5UTVBfRElSLCAnKicpXTogYCR7dGhpcy5UTVBfRElSfS8qYCxcbiAgICAgIC8vIFtqb2luKHRoaXMuVE1QX0RJUiwgdGhpcy5CT09UU1RSQVBfRElSLCAnKicpXTogYCR7dGhpcy5UTVBfRElSfS8ke3RoaXMuQk9PVFNUUkFQX0RJUn0vKmAsXG4gICAgICAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci9hbmltYXRpb25zJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyL2J1bmRsZXMvcGxhdGZvcm0tYnJvd3Nlci1hbmltYXRpb25zLnVtZC5qcycsXG4gICAgICAnQGFuZ3VsYXIvYW5pbWF0aW9ucy9icm93c2VyJzogJ25vZGVfbW9kdWxlcy9AYW5ndWxhci9hbmltYXRpb25zL2J1bmRsZXMvYW5pbWF0aW9ucy1icm93c2VyLnVtZC5qcycsXG4gICAgICAnZGlzdC90bXAvbm9kZV9tb2R1bGVzLyonOiAnZGlzdC90bXAvbm9kZV9tb2R1bGVzLyonLFxuICAgICAgJ25vZGVfbW9kdWxlcy8qJzogJ25vZGVfbW9kdWxlcy8qJyxcbiAgICAgICcqJzogJ25vZGVfbW9kdWxlcy8qJ1xuICAgIH0sXG4gICAgcGFja2FnZXM6IHtcbiAgICAgICdAYW5ndWxhci9hbmltYXRpb25zJzoge1xuICAgICAgICBtYWluOiAnYnVuZGxlcy9hbmltYXRpb25zLnVtZC5qcycsXG4gICAgICAgIGRlZmF1bHRFeHRlbnNpb246ICdqcydcbiAgICAgIH0sXG4gICAgICAnQGFuZ3VsYXIvY29tbW9uJzoge1xuICAgICAgICBtYWluOiAnYnVuZGxlcy9jb21tb24udW1kLmpzJyxcbiAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgfSxcbiAgICAgICdAYW5ndWxhci9jb21waWxlcic6IHtcbiAgICAgICAgbWFpbjogJ2J1bmRsZXMvY29tcGlsZXIudW1kLmpzJyxcbiAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgfSxcbiAgICAgICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnOiB7XG4gICAgICAgIG1haW46ICdidW5kbGVzL2NvcmUtdGVzdGluZy51bWQuanMnLFxuICAgICAgICBkZWZhdWx0RXh0ZW5zaW9uOiAnanMnXG4gICAgICB9LFxuICAgICAgJ0Bhbmd1bGFyL2NvcmUnOiB7XG4gICAgICAgIG1haW46ICdidW5kbGVzL2NvcmUudW1kLmpzJyxcbiAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgfSxcbiAgICAgICdAYW5ndWxhci9mb3Jtcyc6IHtcbiAgICAgICAgbWFpbjogJ2J1bmRsZXMvZm9ybXMudW1kLmpzJyxcbiAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgfSxcbiAgICAgICdAYW5ndWxhci9odHRwJzoge1xuICAgICAgICBtYWluOiAnYnVuZGxlcy9odHRwLnVtZC5qcycsXG4gICAgICAgIGRlZmF1bHRFeHRlbnNpb246ICdqcydcbiAgICAgIH0sXG4gICAgICAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic6IHtcbiAgICAgICAgbWFpbjogJ2J1bmRsZXMvcGxhdGZvcm0tYnJvd3Nlci51bWQuanMnLFxuICAgICAgICBkZWZhdWx0RXh0ZW5zaW9uOiAnanMnXG4gICAgICB9LFxuICAgICAgJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXItZHluYW1pYyc6IHtcbiAgICAgICAgbWFpbjogJ2J1bmRsZXMvcGxhdGZvcm0tYnJvd3Nlci1keW5hbWljLnVtZC5qcycsXG4gICAgICAgIGRlZmF1bHRFeHRlbnNpb246ICdqcydcbiAgICAgIH0sXG4gICAgICAnQGFuZ3VsYXIvcm91dGVyJzoge1xuICAgICAgICBtYWluOiAnYnVuZGxlcy9yb3V0ZXIudW1kLmpzJyxcbiAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgfSxcbiAgICAgICdAYW5ndWxhci9zZXJ2aWNlLXdvcmtlcic6IHtcbiAgICAgICAgbWFpbjogJ2J1bmRsZXMvc2VydmljZS13b3JrZXIudW1kLmpzJyxcbiAgICAgICAgZGVmYXVsdEV4dGVuc2lvbjogJ2pzJ1xuICAgICAgfSxcbiAgICAgICdyeGpzJzoge1xuICAgICAgICBtYWluOiAnUnguanMnLFxuICAgICAgICBkZWZhdWx0RXh0ZW5zaW9uOiAnanMnXG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBUaGUgQXV0b3ByZWZpeGVyIGNvbmZpZ3VyYXRpb24gZm9yIHRoZSBhcHBsaWNhdGlvbi5cbiAgICogQHR5cGUge0FycmF5fVxuICAgKi9cbiAgQlJPV1NFUl9MSVNUID0gW1xuICAgICdpZSA+PSAxMCcsXG4gICAgJ2llX21vYiA+PSAxMCcsXG4gICAgJ2ZmID49IDMwJyxcbiAgICAnY2hyb21lID49IDM0JyxcbiAgICAnc2FmYXJpID49IDcnLFxuICAgICdvcGVyYSA+PSAyMycsXG4gICAgJ2lvcyA+PSA3JyxcbiAgICAnYW5kcm9pZCA+PSA0LjQnLFxuICAgICdiYiA+PSAxMCdcbiAgXTtcblxuICAvKipcbiAgICogV2hpdGUgbGlzdCBmb3IgQ1NTIGNvbG9yIGd1YXJkXG4gICAqIEB0eXBlIHtbc3RyaW5nLCBzdHJpbmddW119XG4gICAqL1xuICBDT0xPUl9HVUFSRF9XSElURV9MSVNUOiBbc3RyaW5nLCBzdHJpbmddW10gPSBbXG4gIF07XG5cbiAgLyoqXG4gICogQnJvd3Nlci1zeW5jIG1pZGRsZXdhcmUgY29uZmlndXJhdGlvbnMgYXJyYXkuXG4gICogQHR5cGUge0FycmF5fVxuICAqL1xuICBQUk9YWV9NSURETEVXQVJFOiBhbnlbXSA9IFtdO1xuXG4gIC8qKlxuICAgKiBDb25maWd1cmF0aW9ucyBmb3IgTlBNIG1vZHVsZSBjb25maWd1cmF0aW9ucy4gQWRkIHRvIG9yIG92ZXJyaWRlIGluIHByb2plY3QuY29uZmlnLnRzLlxuICAgKiBAdHlwZSB7YW55fVxuICAgKi9cbiAgUExVR0lOX0NPTkZJR1M6IGFueSA9IHt9O1xuXG4gIC8qKlxuICAgKiBHZW5lcmF0ZXMgdGhlIHF1ZXJ5IHN0cmluZyB3aGljaCBzaG91bGQgYmUgYXBwZW5kZWQgdG8gdGhlIGVuZCBvZiB0aGUgVVJMcyBpbiBkZXYgbW9kZS5cbiAgICovXG4gIFFVRVJZX1NUUklOR19HRU5FUkFUT1IgPSAoKSA9PiB7XG4gICAgcmV0dXJuIERhdGUubm93KCkudG9TdHJpbmcoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBhcnJheSBvZiBpbmplY3RhYmxlIGRlcGVuZGVuY2llcyAodGhlIGxpc3Qgb2YgbG9jYWwgZmlsZXMgdG8gYmUgaW5qZWN0ZWQgaW4gdGhlIGBpbmRleC5odG1sYCkuXG4gICAqIEByZXR1cm4ge0luamVjdGFibGVEZXBlbmRlbmN5W119XG4gICAqL1xuICBwcml2YXRlIGdldCBfQVBQX0FTU0VUUygpOiBJbmplY3RhYmxlRGVwZW5kZW5jeVtdIHtcbiAgICByZXR1cm4gW1xuICAgICAgeyBzcmM6IGAke3RoaXMuQ1NTX1NSQ30vJHt0aGlzLkNTU19CVU5ETEVfTkFNRX0uJHt0aGlzLmdldEluamVjdGFibGVTdHlsZUV4dGVuc2lvbigpfWAsIGluamVjdDogdHJ1ZSwgdmVuZG9yOiBmYWxzZSB9LFxuICAgICAgLi4udGhpcy5BUFBfQVNTRVRTLFxuICAgIF07XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyB0aGUgY29uZmlndXJhdGlvbiBvYmplY3QgZm9yIE5QTSBtb2R1bGUgY29uZmlndXJhdGlvbnMuXG4gICAqL1xuICBwcml2YXRlIGdldCBfUExVR0lOX0NPTkZJR1MoKTogYW55IHtcbiAgICAvKipcbiAgICAgKiBUaGUgQnJvd3NlclN5bmMgY29uZmlndXJhdGlvbiBvZiB0aGUgYXBwbGljYXRpb24uXG4gICAgICogVGhlIGRlZmF1bHQgb3BlbiBiZWhhdmlvciBpcyB0byBvcGVuIHRoZSBicm93c2VyLiBUbyBwcmV2ZW50IHRoZSBicm93c2VyIGZyb20gb3BlbmluZyB1c2UgdGhlIGAtLWJgICBmbGFnIHdoZW5cbiAgICAgKiBydW5uaW5nIGBucG0gc3RhcnRgICh0ZXN0ZWQgd2l0aCBzZXJ2ZS5kZXYpLlxuICAgICAqIEV4YW1wbGU6IGBucG0gc3RhcnQgLS0gLS1iYFxuICAgICAqIEByZXR1cm4ge2FueX1cbiAgICAgKi9cbiAgICBsZXQgZGVmYXVsdHMgPSB7XG4gICAgICAnYnJvd3Nlci1zeW5jJzoge1xuICAgICAgICBtaWRkbGV3YXJlOiBbcmVxdWlyZSgnY29ubmVjdC1oaXN0b3J5LWFwaS1mYWxsYmFjaycpKHtcbiAgICAgICAgICBpbmRleDogYCR7dGhpcy5BUFBfQkFTRX1pbmRleC5odG1sYFxuICAgICAgICB9KSwgLi4udGhpcy5QUk9YWV9NSURETEVXQVJFXSxcbiAgICAgICAgcG9ydDogdGhpcy5QT1JULFxuICAgICAgICBzdGFydFBhdGg6IHRoaXMuQVBQX0JBU0UsXG4gICAgICAgIG9wZW46IGFyZ3ZbJ2InXSA/IGZhbHNlIDogdHJ1ZSxcbiAgICAgICAgaW5qZWN0Q2hhbmdlczogZmFsc2UsXG4gICAgICAgIHNlcnZlcjoge1xuICAgICAgICAgIGJhc2VEaXI6IGAke3RoaXMuRElTVF9ESVJ9L2VtcHR5L2AsXG4gICAgICAgICAgcm91dGVzOiB7XG4gICAgICAgICAgICBbYCR7dGhpcy5BUFBfQkFTRX0ke3RoaXMuQVBQX1NSQ31gXTogdGhpcy5BUFBfU1JDLFxuICAgICAgICAgICAgW2Ake3RoaXMuQVBQX0JBU0V9JHt0aGlzLkFQUF9ERVNUfWBdOiB0aGlzLkFQUF9ERVNULFxuICAgICAgICAgICAgW2Ake3RoaXMuQVBQX0JBU0V9bm9kZV9tb2R1bGVzYF06ICdub2RlX21vZHVsZXMnLFxuICAgICAgICAgICAgW2Ake3RoaXMuQVBQX0JBU0UucmVwbGFjZSgvXFwvJC8sICcnKX1gXTogdGhpcy5BUFBfREVTVFxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgLy8gTm90ZTogeW91IGNhbiBjdXN0b21pemUgdGhlIGxvY2F0aW9uIG9mIHRoZSBmaWxlXG4gICAgICAnZW52aXJvbm1lbnQtY29uZmlnJzogam9pbih0aGlzLlBST0pFQ1RfUk9PVCwgdGhpcy5UT09MU19ESVIsICdlbnYnKSxcblxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgb3B0aW9ucyB0byBwYXNzIHRvIGd1bHAtc2FzcyAoYW5kIHRoZW4gdG8gbm9kZS1zYXNzKS5cbiAgICAgICAqIFJlZmVyZW5jZTogaHR0cHM6Ly9naXRodWIuY29tL3Nhc3Mvbm9kZS1zYXNzI29wdGlvbnNcbiAgICAgICAqIEB0eXBlIHtvYmplY3R9XG4gICAgICAgKi9cbiAgICAgICdndWxwLXNhc3MnOiB7XG4gICAgICAgIGluY2x1ZGVQYXRoczogWycuL25vZGVfbW9kdWxlcy8nXVxuICAgICAgfSxcblxuICAgICAgLyoqXG4gICAgICAgKiBUaGUgb3B0aW9ucyB0byBwYXNzIHRvIGd1bHAtY29uY2F0LWNzc1xuICAgICAgICogUmVmZXJlbmNlOiBodHRwczovL2dpdGh1Yi5jb20vbWFyaW9jYXNjaWFyby9ndWxwLWNvbmNhdC1jc3NcbiAgICAgICAqIEB0eXBlIHtvYmplY3R9XG4gICAgICAgKi9cbiAgICAgICdndWxwLWNvbmNhdC1jc3MnOiB7XG4gICAgICAgIHRhcmdldEZpbGU6IGAke3RoaXMuQ1NTX0JVTkRMRV9OQU1FfS5jc3NgLFxuICAgICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgcmViYXNlVXJsczogZmFsc2VcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICB0aGlzLm1lcmdlT2JqZWN0KGRlZmF1bHRzLCB0aGlzLlBMVUdJTl9DT05GSUdTKTtcblxuICAgIHJldHVybiBkZWZhdWx0cztcbiAgfVxuXG4gIC8qKlxuICAgKiBLYXJtYSByZXBvcnRlciBjb25maWd1cmF0aW9uXG4gICAqL1xuICBnZXRLYXJtYVJlcG9ydGVycygpOiBhbnkge1xuICAgIHJldHVybiB7XG4gICAgICBwcmVwcm9jZXNzb3JzOiB7XG4gICAgICAgICdkaXN0LyoqLyEoKnNwZWN8aW5kZXh8Ki5tb2R1bGV8Ki5yb3V0ZXMpLmpzJzogWydjb3ZlcmFnZSddXG4gICAgICB9LFxuICAgICAgcmVwb3J0ZXJzOiBbJ21vY2hhJywgJ2NvdmVyYWdlJywgJ2thcm1hLXJlbWFwLWlzdGFuYnVsJ10sXG4gICAgICBjb3ZlcmFnZVJlcG9ydGVyOiB7XG4gICAgICAgIGRpcjogdGhpcy5DT1ZFUkFHRV9ESVIgKyAnLycsXG4gICAgICAgIHJlcG9ydGVyczogW1xuICAgICAgICAgIHsgdHlwZTogJ2pzb24nLCBzdWJkaXI6ICcuJywgZmlsZTogJ2NvdmVyYWdlLWZpbmFsLmpzb24nIH0sXG4gICAgICAgICAgeyB0eXBlOiAnaHRtbCcsIHN1YmRpcjogJy4nIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHJlbWFwSXN0YW5idWxSZXBvcnRlcjoge1xuICAgICAgICByZXBvcnRzOiB7XG4gICAgICAgICAgaHRtbDogdGhpcy5DT1ZFUkFHRV9UU19ESVJcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG4gIH1cblxuICAvKipcbiAgICogUmVjdXJzaXZlbHkgbWVyZ2Ugc291cmNlIG9udG8gdGFyZ2V0LlxuICAgKiBAcGFyYW0ge2FueX0gdGFyZ2V0IFRoZSB0YXJnZXQgb2JqZWN0ICh0byByZWNlaXZlIHZhbHVlcyBmcm9tIHNvdXJjZSlcbiAgICogQHBhcmFtIHthbnl9IHNvdXJjZSBUaGUgc291cmNlIG9iamVjdCAodG8gYmUgbWVyZ2VkIG9udG8gdGFyZ2V0KVxuICAgKi9cbiAgbWVyZ2VPYmplY3QodGFyZ2V0OiBhbnksIHNvdXJjZTogYW55KSB7XG4gICAgY29uc3QgZGVlcEV4dGVuZCA9IHJlcXVpcmUoJ2RlZXAtZXh0ZW5kJyk7XG4gICAgZGVlcEV4dGVuZCh0YXJnZXQsIHNvdXJjZSk7XG4gIH1cblxuICAvKipcbiAgICogTG9jYXRlIGEgcGx1Z2luIGNvbmZpZ3VyYXRpb24gb2JqZWN0IGJ5IHBsdWdpbiBrZXkuXG4gICAqIEBwYXJhbSB7YW55fSBwbHVnaW5LZXkgVGhlIG9iamVjdCBrZXkgdG8gbG9vayB1cCBpbiBQTFVHSU5fQ09ORklHUy5cbiAgICovXG4gIGdldFBsdWdpbkNvbmZpZyhwbHVnaW5LZXk6IHN0cmluZyk6IGFueSB7XG4gICAgaWYgKHRoaXMuX1BMVUdJTl9DT05GSUdTW3BsdWdpbktleV0pIHtcbiAgICAgIHJldHVybiB0aGlzLl9QTFVHSU5fQ09ORklHU1twbHVnaW5LZXldO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGdldEluamVjdGFibGVTdHlsZUV4dGVuc2lvbigpIHtcbiAgICByZXR1cm4gdGhpcy5CVUlMRF9UWVBFID09PSBCVUlMRF9UWVBFUy5QUk9EVUNUSU9OICYmIHRoaXMuRU5BQkxFX1NDU1MgPyAnc2NzcycgOiAnY3NzJztcbiAgfVxuXG4gIGFkZFBhY2thZ2VCdW5kbGVzKHBhY2s6IEV4dGVuZFBhY2thZ2VzKSB7XG5cbiAgICBpZiAocGFjay5wYXRoKSB7XG4gICAgICB0aGlzLlNZU1RFTV9DT05GSUdfREVWLnBhdGhzW3BhY2submFtZV0gPSBwYWNrLnBhdGg7XG4gICAgICB0aGlzLlNZU1RFTV9CVUlMREVSX0NPTkZJRy5wYXRoc1twYWNrLm5hbWVdID0gcGFjay5wYXRoO1xuICAgIH1cblxuICAgIGlmIChwYWNrLnBhY2thZ2VNZXRhKSB7XG4gICAgICB0aGlzLlNZU1RFTV9DT05GSUdfREVWLnBhY2thZ2VzW3BhY2submFtZV0gPSBwYWNrLnBhY2thZ2VNZXRhO1xuICAgICAgdGhpcy5TWVNURU1fQlVJTERFUl9DT05GSUcucGFja2FnZXNbcGFjay5uYW1lXSA9IHBhY2sucGFja2FnZU1ldGE7XG4gICAgfVxuICB9XG5cbiAgYWRkUGFja2FnZXNCdW5kbGVzKHBhY2tzOiBFeHRlbmRQYWNrYWdlc1tdKSB7XG5cbiAgICBwYWNrcy5mb3JFYWNoKChwYWNrOiBFeHRlbmRQYWNrYWdlcykgPT4ge1xuICAgICAgdGhpcy5hZGRQYWNrYWdlQnVuZGxlcyhwYWNrKTtcbiAgICB9KTtcblxuICB9XG5cbi8qKlxuICogQ29udmVydCBuYW1lZCByb2xsdXAgYXJyYXkgdG8gb2JqZWN0XG4gKi9cbiAgZ2V0Um9sbHVwTmFtZWRFeHBvcnRzKCkge1xuICAgIGxldCBuYW1lZEV4cG9ydHMgPSB7fTtcbiAgICB0aGlzLlJPTExVUF9OQU1FRF9FWFBPUlRTLm1hcChuYW1lZEV4cG9ydCA9PiB7XG4gICAgICBuYW1lZEV4cG9ydHMgPSBPYmplY3QuYXNzaWduKG5hbWVkRXhwb3J0cywgbmFtZWRFeHBvcnQpO1xuICAgIH0pO1xuICAgIHJldHVybiBuYW1lZEV4cG9ydHM7XG4gIH1cbn1cblxuLyoqXG4gKiBOb3JtYWxpemVzIHRoZSBnaXZlbiBgZGVwc2AgdG8gc2tpcCBnbG9icy5cbiAqIEBwYXJhbSB7SW5qZWN0YWJsZURlcGVuZGVuY3lbXX0gZGVwcyAtIFRoZSBkZXBlbmRlbmNpZXMgdG8gYmUgbm9ybWFsaXplZC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIG5vcm1hbGl6ZURlcGVuZGVuY2llcyhkZXBzOiBJbmplY3RhYmxlRGVwZW5kZW5jeVtdKSB7XG4gIGRlcHNcbiAgICAuZmlsdGVyKChkOiBJbmplY3RhYmxlRGVwZW5kZW5jeSkgPT4gIS9cXCovLnRlc3QoZC5zcmMpKSAvLyBTa2lwIGdsb2JzXG4gICAgLmZvckVhY2goKGQ6IEluamVjdGFibGVEZXBlbmRlbmN5KSA9PiBkLnNyYyA9IHJlcXVpcmUucmVzb2x2ZShkLnNyYykpO1xuICByZXR1cm4gZGVwcztcbn1cblxuLyoqXG4gKiBSZXR1cm5zIGlmIHRoZSBnaXZlbiBkZXBlbmRlbmN5IGlzIHVzZWQgaW4gdGhlIGdpdmVuIGVudmlyb25tZW50LlxuICogQHBhcmFtICB7c3RyaW5nfSAgICAgICAgICAgICAgIGVudiAtIFRoZSBlbnZpcm9ubWVudCB0byBiZSBmaWx0ZXJlZCBmb3IuXG4gKiBAcGFyYW0gIHtJbmplY3RhYmxlRGVwZW5kZW5jeX0gZCAgIC0gVGhlIGRlcGVuZGVuY3kgdG8gY2hlY2suXG4gKiBAcmV0dXJuIHtib29sZWFufSAgICAgICAgICAgICAgICAgICAgYHRydWVgIGlmIHRoZSBkZXBlbmRlbmN5IGlzIHVzZWQgaW4gdGhpcyBlbnZpcm9ubWVudCwgYGZhbHNlYCBvdGhlcndpc2UuXG4gKi9cbmZ1bmN0aW9uIGZpbHRlckRlcGVuZGVuY3kodHlwZTogc3RyaW5nLCBkOiBJbmplY3RhYmxlRGVwZW5kZW5jeSk6IGJvb2xlYW4ge1xuICBjb25zdCB0ID0gZC5idWlsZFR5cGUgfHwgZC5lbnY7XG4gIGQuYnVpbGRUeXBlID0gdDtcbiAgaWYgKCF0KSB7XG4gICAgZC5idWlsZFR5cGUgPSBPYmplY3Qua2V5cyhCVUlMRF9UWVBFUykubWFwKGsgPT4gQlVJTERfVFlQRVNba10pO1xuICB9XG4gIGlmICghKGQuYnVpbGRUeXBlIGluc3RhbmNlb2YgQXJyYXkpKSB7XG4gICAgKDxhbnk+ZCkuZW52ID0gW2QuYnVpbGRUeXBlXTtcbiAgfVxuICByZXR1cm4gZC5idWlsZFR5cGUuaW5kZXhPZih0eXBlKSA+PSAwO1xufVxuXG4vKipcbiAqIFJldHVybnMgdGhlIGFwcGxpY2F0aW9ucyB2ZXJzaW9uIGFzIGRlZmluZWQgaW4gdGhlIGBwYWNrYWdlLmpzb25gLlxuICogQHJldHVybiB7bnVtYmVyfSBUaGUgYXBwbGljYXRpb25zIHZlcnNpb24uXG4gKi9cbmZ1bmN0aW9uIGFwcFZlcnNpb24oKTogbnVtYmVyIHwgc3RyaW5nIHtcbiAgdmFyIHBrZyA9IHJlcXVpcmUoJy4uLy4uL3BhY2thZ2UuanNvbicpO1xuICByZXR1cm4gcGtnLnZlcnNpb247XG59XG5cbi8qKlxuICogUmV0dXJucyB0aGUgYXBwbGljYXRpb24gYnVpbGQgdHlwZS5cbiAqL1xuZnVuY3Rpb24gZ2V0QnVpbGRUeXBlKCkge1xuICBsZXQgdHlwZSA9IChhcmd2WydidWlsZC10eXBlJ10gfHwgYXJndlsnZW52J10gfHwgJycpLnRvTG93ZXJDYXNlKCk7XG4gIGxldCBiYXNlOiBzdHJpbmdbXSA9IGFyZ3ZbJ18nXTtcbiAgbGV0IHByb2RLZXl3b3JkID0gISFiYXNlLmZpbHRlcihvID0+IG8uaW5kZXhPZihCVUlMRF9UWVBFUy5QUk9EVUNUSU9OKSA+PSAwKS5wb3AoKTtcbiAgaWYgKChiYXNlICYmIHByb2RLZXl3b3JkKSB8fCB0eXBlID09PSBCVUlMRF9UWVBFUy5QUk9EVUNUSU9OKSB7XG4gICAgcmV0dXJuIEJVSUxEX1RZUEVTLlBST0RVQ1RJT047XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIEJVSUxEX1RZUEVTLkRFVkVMT1BNRU5UO1xuICB9XG59XG4iXX0=