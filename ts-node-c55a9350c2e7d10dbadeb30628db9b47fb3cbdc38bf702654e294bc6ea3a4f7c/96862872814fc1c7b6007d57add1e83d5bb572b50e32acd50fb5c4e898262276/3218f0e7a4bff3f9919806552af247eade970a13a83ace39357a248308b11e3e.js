"use strict";
var Builder = require('systemjs-builder');
module.exports = function (done) {
    var options = {
        normalize: true,
        runtime: false,
        sourceMaps: true,
        sourceMapContents: true,
        minify: true,
        mangle: false
    };
    var builder = new Builder('./');
    builder.config({
        paths: {
            'n:*': 'node_modules/*',
            'rxjs/*': 'node_modules/rxjs/*.js',
        },
        map: {
            'rxjs': 'n:rxjs',
        },
        packages: {
            'rxjs': { main: 'Rx.js', defaultExtension: 'js' },
        }
    });
    builder.bundle('rxjs', 'node_modules/.tmp/Rx.min.js', options)
        .then(function () { return done(); })
        .catch(function (error) { return done(error); });
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvcGMtMDkvRGVza3RvcC93d3cuc2VuaW9yc3BvbmdlLmNvbS11c2VyLWNvbW11bml0eS1iOTVmZjIzNTRkNWU3NTkzNGI2N2MzMThhMTY4ZDg4MDM1NTA3NGFjL3Rvb2xzL3Rhc2tzL3NlZWQvYnVpbGQuYnVuZGxlLnJ4anMudHMiLCJzb3VyY2VzIjpbIi9ob21lL3BjLTA5L0Rlc2t0b3Avd3d3LnNlbmlvcnNwb25nZS5jb20tdXNlci1jb21tdW5pdHktYjk1ZmYyMzU0ZDVlNzU5MzRiNjdjMzE4YTE2OGQ4ODAzNTUwNzRhYy90b29scy90YXNrcy9zZWVkL2J1aWxkLmJ1bmRsZS5yeGpzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFJQSxJQUFNLE9BQU8sR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQztBQUU1QyxpQkFBUyxVQUFDLElBQVM7SUFDakIsSUFBTSxPQUFPLEdBQUc7UUFDZCxTQUFTLEVBQUUsSUFBSTtRQUNmLE9BQU8sRUFBRSxLQUFLO1FBQ2QsVUFBVSxFQUFFLElBQUk7UUFDaEIsaUJBQWlCLEVBQUUsSUFBSTtRQUN2QixNQUFNLEVBQUUsSUFBSTtRQUNaLE1BQU0sRUFBRSxLQUFLO0tBQ2QsQ0FBQztJQUNGLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2hDLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFDYixLQUFLLEVBQUU7WUFDTCxLQUFLLEVBQUUsZ0JBQWdCO1lBQ3ZCLFFBQVEsRUFBRSx3QkFBd0I7U0FDbkM7UUFDRCxHQUFHLEVBQUU7WUFDSCxNQUFNLEVBQUUsUUFBUTtTQUNqQjtRQUNELFFBQVEsRUFBRTtZQUNSLE1BQU0sRUFBRSxFQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsSUFBSSxFQUFDO1NBQ2hEO0tBQ0YsQ0FBQyxDQUFDO0lBQ0gsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsNkJBQTZCLEVBQUUsT0FBTyxDQUFDO1NBQzNELElBQUksQ0FBQyxjQUFNLE9BQUEsSUFBSSxFQUFFLEVBQU4sQ0FBTSxDQUFDO1NBQ2xCLEtBQUssQ0FBQyxVQUFDLEtBQVMsSUFBSyxPQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBWCxDQUFXLENBQUMsQ0FBQztBQUN2QyxDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFRlbXBvcmFyeSBmaXguIFNlZSBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9hbmd1bGFyL2lzc3Vlcy85MzU5XG4gKi9cblxuY29uc3QgQnVpbGRlciA9IHJlcXVpcmUoJ3N5c3RlbWpzLWJ1aWxkZXInKTtcblxuZXhwb3J0ID0gKGRvbmU6IGFueSkgPT4ge1xuICBjb25zdCBvcHRpb25zID0ge1xuICAgIG5vcm1hbGl6ZTogdHJ1ZSxcbiAgICBydW50aW1lOiBmYWxzZSxcbiAgICBzb3VyY2VNYXBzOiB0cnVlLFxuICAgIHNvdXJjZU1hcENvbnRlbnRzOiB0cnVlLFxuICAgIG1pbmlmeTogdHJ1ZSxcbiAgICBtYW5nbGU6IGZhbHNlXG4gIH07XG4gIHZhciBidWlsZGVyID0gbmV3IEJ1aWxkZXIoJy4vJyk7XG4gIGJ1aWxkZXIuY29uZmlnKHtcbiAgICBwYXRoczoge1xuICAgICAgJ246Kic6ICdub2RlX21vZHVsZXMvKicsXG4gICAgICAncnhqcy8qJzogJ25vZGVfbW9kdWxlcy9yeGpzLyouanMnLFxuICAgIH0sXG4gICAgbWFwOiB7XG4gICAgICAncnhqcyc6ICduOnJ4anMnLFxuICAgIH0sXG4gICAgcGFja2FnZXM6IHtcbiAgICAgICdyeGpzJzoge21haW46ICdSeC5qcycsIGRlZmF1bHRFeHRlbnNpb246ICdqcyd9LFxuICAgIH1cbiAgfSk7XG4gIGJ1aWxkZXIuYnVuZGxlKCdyeGpzJywgJ25vZGVfbW9kdWxlcy8udG1wL1J4Lm1pbi5qcycsIG9wdGlvbnMpXG4gICAgLnRoZW4oKCkgPT4gZG9uZSgpKVxuICAgIC5jYXRjaCgoZXJyb3I6YW55KSA9PiBkb25lKGVycm9yKSk7XG59O1xuIl19