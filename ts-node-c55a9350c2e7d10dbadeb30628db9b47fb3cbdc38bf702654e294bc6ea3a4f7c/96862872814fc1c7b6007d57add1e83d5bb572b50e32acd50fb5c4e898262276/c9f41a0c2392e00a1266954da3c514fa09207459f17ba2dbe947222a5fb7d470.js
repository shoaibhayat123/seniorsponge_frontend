"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var server_1 = require("../seed/server");
var ProjectWebServer = (function (_super) {
    __extends(ProjectWebServer, _super);
    function ProjectWebServer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ProjectWebServer.prototype.startServer = function () {
        _super.prototype.startServer.call(this);
    };
    return ProjectWebServer;
}(server_1.SeedWebServer));
exports.ProjectWebServer = ProjectWebServer;
var app = express();
var webServer = new ProjectWebServer(app);
exports.default = webServer;
function serveSPA() { webServer.serveSPA(); }
exports.serveSPA = serveSPA;
function notifyLiveReload(e) { webServer.notifyLiveReload(e); }
exports.notifyLiveReload = notifyLiveReload;
function serveDocs() { webServer.serveDocs(); }
exports.serveDocs = serveDocs;
function serveCoverage() { webServer.serveCoverage(); }
exports.serveCoverage = serveCoverage;
function serveProd() { webServer.serveProd(); }
exports.serveProd = serveProd;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvcGMtMDkvRGVza3RvcC93d3cuc2VuaW9yc3BvbmdlLmNvbS11c2VyLWNvbW11bml0eS1iOTVmZjIzNTRkNWU3NTkzNGI2N2MzMThhMTY4ZDg4MDM1NTA3NGFjL3Rvb2xzL3V0aWxzL3Byb2plY3Qvc2VydmVyLnRzIiwic291cmNlcyI6WyIvaG9tZS9wYy0wOS9EZXNrdG9wL3d3dy5zZW5pb3JzcG9uZ2UuY29tLXVzZXItY29tbXVuaXR5LWI5NWZmMjM1NGQ1ZTc1OTM0YjY3YzMxOGExNjhkODgwMzU1MDc0YWMvdG9vbHMvdXRpbHMvcHJvamVjdC9zZXJ2ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsaUNBQW1DO0FBRW5DLHlDQUErQztBQUcvQztJQUFzQyxvQ0FBYTtJQUFuRDs7SUFZQSxDQUFDO0lBTFEsc0NBQVcsR0FBbEI7UUFDRSxpQkFBTSxXQUFXLFdBQUUsQ0FBQztJQUd0QixDQUFDO0lBQ0gsdUJBQUM7QUFBRCxDQUFDLEFBWkQsQ0FBc0Msc0JBQWEsR0FZbEQ7QUFaWSw0Q0FBZ0I7QUFjN0IsSUFBSSxHQUFHLEdBQWdCLE9BQU8sRUFBRSxDQUFDO0FBQ2pDLElBQUksU0FBUyxHQUFHLElBQUksZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDMUMsa0JBQWUsU0FBUyxDQUFDO0FBQ3pCLHNCQUE2QixTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQXBELDRCQUFvRDtBQUNwRCwwQkFBaUMsQ0FBTSxJQUFJLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFBM0UsNENBQTJFO0FBQzNFLHVCQUE4QixTQUFTLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQXRELDhCQUFzRDtBQUN0RCwyQkFBa0MsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUMsQ0FBQztBQUE5RCxzQ0FBOEQ7QUFDOUQsdUJBQThCLFNBQVMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7QUFBdEQsOEJBQXNEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgZXhwcmVzcyBmcm9tICdleHByZXNzJztcbmltcG9ydCB7IEFwcGxpY2F0aW9uIH0gZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQgeyBTZWVkV2ViU2VydmVyIH0gZnJvbSAnLi4vc2VlZC9zZXJ2ZXInO1xuXG5cbmV4cG9ydCBjbGFzcyBQcm9qZWN0V2ViU2VydmVyIGV4dGVuZHMgU2VlZFdlYlNlcnZlciB7XG4gIC8vIE92ZXJyaWRlIGFuZCBtb2RpZnkgdGhlIHNlcnZlciBjb2RlIGluIHRoaXMgY2xhc3MuXG5cbiAgLyoqXG4gICAqIFRoaXMgbWV0aG9kIGlzIGNhbGxlZCBvbmNlIHJlZ2FyZGxlc3Mgb2YgaG93IHRoZSBzZXJ2ZXIgaXMgc3RhcnRlZCwgYW5kIHRoaXNcbiAgICogaXMgdGhlIHN0YXJ0aW5nIHBvaW50IG9mIHRoZSBwcm9qZWN0J3MgY3VzdG9tIHNlcnZlciBjb2RlLlxuICAgKi9cbiAgcHVibGljIHN0YXJ0U2VydmVyKCkge1xuICAgIHN1cGVyLnN0YXJ0U2VydmVyKCk7XG5cbiAgICAvLyBBZGQgcHJvamVjdCBzZXJ2ZXIgc3RhcnR1cCBjb2RlIGhlcmUuXG4gIH1cbn1cblxubGV0IGFwcDogQXBwbGljYXRpb24gPSBleHByZXNzKCk7XG5sZXQgd2ViU2VydmVyID0gbmV3IFByb2plY3RXZWJTZXJ2ZXIoYXBwKTtcbmV4cG9ydCBkZWZhdWx0IHdlYlNlcnZlcjtcbmV4cG9ydCBmdW5jdGlvbiBzZXJ2ZVNQQSgpIHsgd2ViU2VydmVyLnNlcnZlU1BBKCk7IH1cbmV4cG9ydCBmdW5jdGlvbiBub3RpZnlMaXZlUmVsb2FkKGU6IGFueSkgeyB3ZWJTZXJ2ZXIubm90aWZ5TGl2ZVJlbG9hZChlKTsgfVxuZXhwb3J0IGZ1bmN0aW9uIHNlcnZlRG9jcygpIHsgd2ViU2VydmVyLnNlcnZlRG9jcygpOyB9XG5leHBvcnQgZnVuY3Rpb24gc2VydmVDb3ZlcmFnZSgpIHsgd2ViU2VydmVyLnNlcnZlQ292ZXJhZ2UoKTsgfVxuZXhwb3J0IGZ1bmN0aW9uIHNlcnZlUHJvZCgpIHsgd2ViU2VydmVyLnNlcnZlUHJvZCgpOyB9XG4iXX0=